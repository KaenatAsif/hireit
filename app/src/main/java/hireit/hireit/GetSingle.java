package hireit.hireit;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by faheem on 16/08/2017.
 */

public class GetSingle extends android.os.AsyncTask<Void, Void, Object> {
    //ProgressDialog loading;
    public AsyncResponse delegate = null;//Call back interface
    String a;
    Context Contextt_;

    public GetSingle(Context c, AsyncResponse asyncResponse, String a) {
        this.Contextt_ = c;
        delegate = asyncResponse;//Assigning call back interfacethrough constructor
        this.a = a;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
      /* MySingleton.getInstance().showLoadingPopup(Contextt_, "Please Wait");
        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config(Contextt_.getResources().getString(R.string.batchkey))); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//batch////////////////////////////////////////////////////////////////////////////*/

    }

    @Override
    protected void onPostExecute(Object s) {
        super.onPostExecute(s);
        Cache.dismissLoadingPopup();
        try {
            delegate.processFinish(s);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Object doInBackground(Void... params) {
        WcfHandler wcf = new WcfHandler(Contextt_);
        String[] parts = a.split("__");

        if (parts[1].equals("1")) {
            String s = wcf.GetJsonResult1(parts[0]);
            return s;
        } else if (parts[1].equals("PHP")) {
            String s = wcf.phpCall(parts[0]);
            return s;
        } else {
            JSONArray s = wcf.GetJsonResult(parts[0]);
            return s;
        }
    }
}