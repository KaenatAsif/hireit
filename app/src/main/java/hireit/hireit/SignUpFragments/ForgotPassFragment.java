package hireit.hireit.SignUpFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.FirebaseException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import cn.pedant.SweetAlert.SweetAlertDialog;
import hireit.hireit.Cache;
import hireit.hireit.Global;
import hireit.hireit.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ForgotPassFragment extends Fragment {
    Button submit;
    EditText phone;
    String numberText;
    String code =null;
    LinearLayout back;

    String number1;
    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;

    public ForgotPassFragment() {
        // Required empty public constructor
    }
    ArrayList<String> list;
    Spinner country;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_forgot_pass, container, false);

        phone= (EditText) v.findViewById(R.id.phoneNumber);
        submit= (Button) v.findViewById(R.id.submt);
        back= (LinearLayout) v.findViewById(R.id.backlayout);
        country= (Spinner) v.findViewById(R.id.country);
        numberText= phone.getText().toString();

        list = new ArrayList<String>();
        list.add("+44");
        list.add("+92");


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        country.setAdapter(adapter);


        country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (country.getSelectedItem().toString().equals("+92")){
                    phone.setHint("3XX XXXXXXX");

                }
                else {

                    phone.setHint("7XXX XXXXXX");

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                numberText= country.getSelectedItem().toString()+phone.getText().toString();
                Global.setNumberCode(numberText);

                if (numberText.startsWith("+")){

                    number1= numberText.replace("+","");

                }
                else{
                    number1=numberText;
                }
                if (numberText.length()>0){
                    sendVarificationCode();
                    Global.setNumber(number1);
                    Global.setForgotPassword(true);
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left);
                    ft.replace(R.id.container, new CodeFragment());
                    ft.addToBackStack(null);
                    ft.commit();


                }
                else {
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Something went Wrong")
                            .setContentText("Number cannot be empty")
                            .show();
                }


            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.setCustomAnimations(R.anim.enter_from_left,R.anim.exit_to_right);
                ft.replace(R.id.container, new PasswordFragment());
                ft.addToBackStack(null);
                ft.commit();

            }
        });
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            }

            @Override
            public void onVerificationFailed(FirebaseException e) {

            }

            @Override
            public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);
                code = s;


                Toast.makeText(getActivity(), "code Sent", Toast.LENGTH_SHORT).show();
                Toast.makeText(getActivity(), "Code is: "+code+"", Toast.LENGTH_SHORT).show();
                Cache prefs= new Cache(getActivity());
                Cache.setKeyCodeResendToken(forceResendingToken);
                prefs.setKeyConfirmationCode(code);
            }
        };




        return v;
    }
    private void sendVarificationCode()
    {

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                numberText,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                getActivity(),               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks

    }



}
