package hireit.hireit.SignUpFragments;


import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;
import hireit.hireit.Global;
import hireit.hireit.R;


/**
 * A fragment that launches other parts of the demo application.
 */
public class PersonalFragment extends Fragment {
    private static final String KEY_MOVIE_TITLE = "key_title";

    EditText firstName;
    EditText lastName;
    Spinner gender;
    TextView dateofBirth;
    LinearLayout next;
    int mYear;
    int mMonth;
    int mDay;

    ArrayList<String> list;



    public static PersonalFragment newInstance(String movieTitle) {

        ArrayList genderArray = new ArrayList(Arrays.asList("Male","Female"));

        PersonalFragment per = new PersonalFragment();
        Bundle args = new Bundle();

        args.putString(KEY_MOVIE_TITLE, movieTitle);
        try {
            per.setArguments(args);
        } catch (IllegalStateException e) {
        }

        return per;
    }

LinearLayout navigation;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_personal, container,
                false);
//        Profile.getCurrentProfile();


        return v;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        firstName= (EditText) view.findViewById(R.id.firstNAme);
        lastName= (EditText) view.findViewById(R.id.lastName);
        gender= (Spinner) view.findViewById(R.id.gender);
        dateofBirth= (TextView) view.findViewById(R.id.dateofBirth);
        next= (LinearLayout) view.findViewById(R.id.next);


       /* ArrayAdapter<String> adapterC = new ArrayAdapter<String>(getActivity(), simple_spinner_item, list);
        adapterC.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        gender.setAdapter(adapterC);
        */

        ArrayList <String> list;

        list = new ArrayList<String>();
        list.add("Male");
        list.add("Female");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        gender.setAdapter(adapter);
        gender.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);




        dateofBirth.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // To show current date in the datepicker
                Calendar mcurrentDate = Calendar.getInstance();
                mYear = mcurrentDate.get(Calendar.YEAR);
                mMonth = mcurrentDate.get(Calendar.MONTH);
                mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        Calendar myCalendar = Calendar.getInstance();
                        myCalendar.set(Calendar.YEAR, selectedyear);
                        myCalendar.set(Calendar.MONTH, selectedmonth);
                        myCalendar.set(Calendar.DAY_OF_MONTH, selectedday);
                        String myFormat = "dd-MMM-yyyy"; //Change as you need
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.ENGLISH);
                        dateofBirth.setText(sdf.format(myCalendar.getTime()));

                        mDay = selectedday;
                        mMonth = selectedmonth;
                        mYear = selectedyear;
                    }
                }, mYear, mMonth, mDay);
                //mDatePicker.setTitle("Select date");
                mDatePicker.show();
            }
        });


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                 String st_firstName= firstName.getText().toString();
                 String st_lastName= lastName.getText().toString();
                 String st_gender= gender.getSelectedItem().toString();
                 String st_DOB= dateofBirth.getText().toString();


                if (st_firstName.length()>0&&st_lastName.length()>0&&st_gender.length()>0&&st_DOB.length()>0) {

                    Global.setFirst_Name(st_firstName);
                    Global.setLast_Name(st_lastName);
                    Global.setGender(st_gender);
                    Global.setDob(st_DOB);

                    SignUpActivity.showConfirmPasswordFragment();
                }
                else {
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Please complete\nthe missing field")
                            .show();
                }

            }
        });

    }

        private class SampleAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return getCount() ;
        }

        @Override
        public ListData getItem(int position) {
            return getItem(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = View.inflate(getActivity(), R.layout.nav_header, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            ListData item = getItem(position);

           /* TextDrawable drawable = mDrawableBuilder.build(String.valueOf(item.Name.charAt(0)), mColorGenerator.getColor(item.Name));
            holder.imageView.setImageDrawable(drawable);
            holder.view.setBackgroundColor(Color.TRANSPARENT);
            holder.textView.setText(item.Name);*/



            return convertView;
        }
    }

    private static class ViewHolder {

        private View view;
        // Name,[ConsultationType],[ConsultationPaymentMethods],[ConsultationCharges]," +
        // " [InsuranceProducts],[Complaint],[Diagnosis],[Prescription],[Tests],[Remarks]

        private TextView tvDate;
        private TextView tvDoctor;
        private TextView tvPrescription;
        private TextView tvTest;
        private TextView tvRemarks;




        private ViewHolder(View view) {
            this.view = view;


        }
    }

    private static class ListData {

        private String ID;
        private String Date;
        private String Doctor;
        private String Prescription;
        private String Test;
        private String Remarks;

        public ListData( String ID, String D, String Doc, String pres, String test,String rem) {
            this.ID = ID;
            this.Date = D;
            this.Doctor = Doc;
            this.Prescription=pres;
            this.Test= test;
            this.Remarks=rem;
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        //item.setIcon(null);
    }
}