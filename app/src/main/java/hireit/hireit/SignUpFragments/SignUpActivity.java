package hireit.hireit.SignUpFragments;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.RelativeLayout;

import hireit.hireit.R;
import hireit.hireit.SignUpFragments.navigation.FragmentNavigationManager;

public class SignUpActivity extends AppCompatActivity {

    TabLayout tabLayout;
    RelativeLayout container;
    SignUpFragment sp1;
    PasswordFragment pass;
    PersonalFragment pers;
    static SignUpActivity SignUp = new SignUpActivity();
    private static FragmentNavigationManager mNavigationManager;


static final String[] Fragments={"Confirm Code"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);



        container = (RelativeLayout) findViewById(R.id.container);


selectFirstItemAsDefault();

        sp1 = new SignUpFragment();
        pass= new PasswordFragment();
    pers=new PersonalFragment();


   /* if (getIntent().getStringExtra("Fragments").equals(Fragments[0])){

        showCodeFragment();
    }*/
        mNavigationManager = FragmentNavigationManager.obtain(this);
        selectFirstItemAsDefault();

    }

    public static void showCodeFragment() {

            if (mNavigationManager != null) {
                String firstActionMovie = "Confirm Code";
                mNavigationManager.showFragmentCode(firstActionMovie);
                //        getSupportActionBar().setTitle(firstActionMovie);
            }
    }

    public static void showPersonalFragment() {

        if (mNavigationManager != null) {
            String firstActionMovie = "Personal Fragment";
            mNavigationManager.showPersonalFragment(firstActionMovie);
            //        getSupportActionBar().setTitle(firstActionMovie);
        }
    }

    public static void selectFirstItemAsDefault() {
        if (mNavigationManager != null) {
            String firstActionMovie = "Sign up";
            mNavigationManager.showSignupFragment(firstActionMovie);
            //        getSupportActionBar().setTitle(firstActionMovie);
        }
    }

    public static void showPasswordFragment() {
        if (mNavigationManager != null) {
            String firstActionMovie = "Sign up";
            mNavigationManager.showPasswordFragment(firstActionMovie);
            //        getSupportActionBar().setTitle(firstActionMovie);
        }
    }

    public static void showConfirmPasswordFragment() {
        if (mNavigationManager != null) {
            String firstActionMovie = "Confirm Password";
            mNavigationManager.showConfirmPasswordFragment(firstActionMovie);
            //        getSupportActionBar().setTitle(firstActionMovie);
        }
    }








   /* public void nextPage(View view) {

    }

    public void LoginbyNumber(View view) {
    }

    public void passwordBack(View view) {

    }

    public void personalBack(View view) {

    }

    public void personalNext(View view) {
        Intent carlist= new Intent(SignUpActivity.this, MainActivity.class);
        startActivity(carlist);
    }

    public void drivingBack(View view) {

    }

*/


/*public void getKey(){
    try {
        PackageInfo info = getPackageManager().getPackageInfo(
                "com.HireIt",
                PackageManager.GET_SIGNATURES);
        for (Signature signature : info.signatures) {
            MessageDigest md = MessageDigest.getInstance("SHA");
            md.update(signature.toByteArray());
            Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
        }
    } catch (PackageManager.NameNotFoundException e) {

    } catch (NoSuchAlgorithmException e) {

    }

}*/
}
