package hireit.hireit.SignUpFragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import hireit.hireit.AsyncResponse;
import hireit.hireit.Cache;
import hireit.hireit.GetProfile;
import hireit.hireit.GetSingle;
import hireit.hireit.Global;
import hireit.hireit.Models.HireitCustomerProfile;
import hireit.hireit.Models.HireitLoginResponse;
import hireit.hireit.R;
import hireit.hireit.drawer.MainActivity;



/**
 * A fragment that launches other parts of the demo application.
 */
public class ConfirmPasswordFragment extends Fragment {


    private static final String KEY_MOVIE_TITLE = "key_title";
    public static ConfirmPasswordFragment newInstance(String movieTitle) {
        ConfirmPasswordFragment con = new ConfirmPasswordFragment();
        Bundle args = new Bundle();

        args.putString(KEY_MOVIE_TITLE, movieTitle);
        try {
            con.setArguments(args);
        } catch (IllegalStateException e) {
        }

        return con;
    }
    TextView privacyLink;
    TextView termsLink;
    CheckBox privacyCheck;
    CheckBox termsCheck;
    WebView web;
    TextView passwordText;
    TextView confirmPassword;
    RelativeLayout card;
    Button backView;
    String passwordString;
    View root;
    LinearLayout back;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_confirm_password, container,
                false);

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

      final EditText  password = (EditText) view.findViewById(R.id.password);
       LinearLayout register= (LinearLayout) view.findViewById(R.id.registerLayout);
        card= (RelativeLayout) view.findViewById(R.id.card);
        back= (LinearLayout) view.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.setCustomAnimations(R.anim.enter_from_left,R.anim.exit_to_right);
                ft.replace(R.id.container, new PersonalFragment());
                ft.addToBackStack(null);
                ft.commit();
            }
        });


        confirmPassword= (TextView) view.findViewById(R.id.confirm);

         privacyLink= (TextView) view.findViewById(R.id.privacyLink);
         termsLink= (TextView) view.findViewById(R.id.termsLink);
         privacyCheck= (CheckBox) view.findViewById(R.id.privacyCheck);
         termsCheck= (CheckBox) view.findViewById(R.id.termsCheck);
         web = (WebView) view.findViewById(R.id.web);
        backView = (Button) view.findViewById(R.id.cancel);



        backView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                web.setVisibility(View.GONE);
                backView.setVisibility(View.GONE);
            }

        });

        privacyLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                web.setVisibility(View.VISIBLE);
                backView.setVisibility(View.VISIBLE);
                web.loadUrl("http://www.hire-it.co/privacy-notice/");
                web.setBackgroundResource(R.drawable.pugnotification_ic_placeholder);
                web.setBackgroundColor(Color.TRANSPARENT);
                web.getSettings().setJavaScriptEnabled(true);


                /*Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.hire-it.co/privacy-notice/"));
                startActivity(browserIntent);
*/
            }
        });
        termsLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                web.setVisibility(View.VISIBLE);
                backView.setVisibility(View.VISIBLE);
                web.loadUrl("http://www.hire-it.co/terms-conditions/");
                web.setBackgroundResource(R.drawable.pugnotification_ic_placeholder);
                web.setBackgroundColor(Color.TRANSPARENT);
                web.getSettings().setJavaScriptEnabled(true);

            }
        });


        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                 passwordString= password.getText().toString();
                JSONObject jsonObj = null;


                String firstName= Global.getFirst_Name();
                String lastName= Global.getLast_Name();
                String date= Global.getDob();
                String gender= Global.getGender();
                String number= Global.getNumber();
                String pic="%22%22";

                if (password.getText().toString().length()>5&&confirmPassword.getText().toString().length()>5) {
                    if (password.getText().toString().equals(confirmPassword.getText().toString())) {
                        if (termsCheck.isChecked() && privacyCheck.isChecked()) {
                            String function = "RegisterNewUser?uname=" + number + "@hire-it.co&password=" + passwordString + "&firstname=" + firstName + "&lastname=" + lastName + "&gender=" + gender + "&DOB=" + date + "&phoneno=" + number;
                            GetProfile asyncTask = new GetProfile(getActivity(), new AsyncResponse() {

                                @Override
                                public void processFinish(Object result) {

                                    String ans = result + "";
                                    JSONObject jsonObj = null;
                                    JSONObject profile = null;

                                    try {
                                        jsonObj = new JSONObject(ans);
                                        profile = jsonObj.getJSONObject("CustomerProfile");

                                        Global.customerProfile = new HireitCustomerProfile(profile);
                                        String result1 = Global.customerProfile.getResultMessage();

                                        if (result1.equals("Already Register")) {
                                            Toast.makeText(getActivity(), result1, Toast.LENGTH_LONG).show();


                                            SignUpActivity.showPasswordFragment();
                                        } else if (result1.equals("Successfully register")) {

                                            String firstName = Global.customerProfile.getFirstName();
                                            String lastName = Global.customerProfile.getLastName();
                                            String gender = Global.customerProfile.getGender();
                                            String dob = Global.customerProfile.getDateOfBirth();
                                            String number = Global.customerProfile.getMobileNumber();
                                            String refferalCode = Global.customerProfile.getRefferalCode();

                                            System.out.println("confirmPass: "+refferalCode);


                                            Global.setFirst_Name(firstName);
                                            Global.setLast_Name(lastName);
                                            Global.setGender(gender);
                                            Global.setDob(dob);
                                            Global.setNumber(number);
                                            Global.setRefferalCode(refferalCode);

                                            SharedPreferences sharedpref = getActivity().getSharedPreferences("_LoginInfo_", Context.MODE_PRIVATE);
                                            SharedPreferences.Editor editor = sharedpref.edit();
                                            editor.putString("FirstName", firstName);
                                            editor.putString("LastName", lastName);
                                            editor.putString("Gender", gender);
                                            editor.putString("DOB", dob);
                                            editor.putString("Password", passwordString);
                                            editor.putString("Number", number);
                                            editor.putString("RefferalCode", refferalCode);
                                            editor.apply();

                                            Toast.makeText(getActivity(), result1, Toast.LENGTH_LONG).show();
                                            Cache prefs = new Cache(getActivity());
                                            prefs.setKeyLogin(true);
                                            Global.setSigningUp(true);

                                            String function = "SaveDeviceToken?email="+number+"@hire-it.co&devicetoken="+Global.getDeviceToken()+"&os=android";
                                            GetSingle asyncTask = new GetSingle(getActivity(), new AsyncResponse() {

                                                @Override
                                                public void processFinish(Object result) {

                                                    String ans = result + "";
                                                    JSONObject jsonObj = null;

                                                    try {
                                                        jsonObj = new JSONObject(ans);

                                                        Global.saveToken = new HireitLoginResponse(jsonObj);
                                                        result = Global.saveToken.getResult().toString();

                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }

                                                }
                                            }, function + "__" + "1");
                                            asyncTask.execute();

                                            Intent gotoMain = new Intent(getActivity(), MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(gotoMain);


                                        } else {
                                            Toast.makeText(getActivity(), result1, Toast.LENGTH_LONG).show();

                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, function + "__" + "1");
                            asyncTask.execute();
                        } else {
                            new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Something Went Wrong")
                                    .setContentText("Please Check the Terms and Conditions\nPrivacy Plicy")
                                    .show();
                        }
                    } else {
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Something Went Wrong")
                                .setContentText("Passwords do not match")
                                .show();
                    }
                }
                else {
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Something Went Wrong")
                            .setContentText("Password must conain 6 characters")
                            .show();

                }


            }



            // sendVarificationCode();

                /*Intent sendCode= new Intent(getActivity(),SignUpActivity.class).putExtra("Fragments","Confirm Code");
                startActivity(sendCode);

*/
//                new SweetAlertDialog(getActivity(),SweetAlertDialog.PROGRESS_TYPE)
//                        .setTitleText("Please Wait")
//                        .show();
//







        });

    }


    private class SampleAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return getCount() ;
        }

        @Override
        public ListData getItem(int position) {
            return getItem(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = View.inflate(getActivity(), R.layout.nav_header, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            ListData item = getItem(position);

           /* TextDrawable drawable = mDrawableBuilder.build(String.valueOf(item.Name.charAt(0)), mColorGenerator.getColor(item.Name));
            holder.imageView.setImageDrawable(drawable);
            holder.view.setBackgroundColor(Color.TRANSPARENT);
            holder.textView.setText(item.Name);*/



            return convertView;
        }
    }

    private static class ViewHolder {

        private View view;
        // Name,[ConsultationType],[ConsultationPaymentMethods],[ConsultationCharges]," +
        // " [InsuranceProducts],[Complaint],[Diagnosis],[Prescription],[Tests],[Remarks]

        private TextView tvDate;
        private TextView tvDoctor;
        private TextView tvPrescription;
        private TextView tvTest;
        private TextView tvRemarks;




        private ViewHolder(View view) {
            this.view = view;


        }
    }

    private static class ListData {

        private String ID;
        private String Date;
        private String Doctor;
        private String Prescription;
        private String Test;
        private String Remarks;

        public ListData( String ID, String D, String Doc, String pres, String test,String rem) {
            this.ID = ID;
            this.Date = D;
            this.Doctor = Doc;
            this.Prescription=pres;
            this.Test= test;
            this.Remarks=rem;
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        //item.setIcon(null);
    }
}