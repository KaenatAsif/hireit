package hireit.hireit.SignUpFragments.navigation;

import android.annotation.SuppressLint;
import android.support.v4.BuildConfig;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import hireit.hireit.R;
import hireit.hireit.SignUpFragments.CodeFragment;
import hireit.hireit.SignUpFragments.ConfirmPasswordFragment;
import hireit.hireit.SignUpFragments.PasswordFragment;
import hireit.hireit.SignUpFragments.PersonalFragment;
import hireit.hireit.SignUpFragments.SignUpActivity;
import hireit.hireit.SignUpFragments.SignUpFragment;


/**
 * @author msahakyan
 */

public class FragmentNavigationManager implements NavigationManager {

    private static FragmentNavigationManager sInstance;

    private FragmentManager mFragmentManager;
    SignUpActivity mActivity;

    public static FragmentNavigationManager obtain(SignUpActivity activity) {
        if (sInstance == null) {
            sInstance = new FragmentNavigationManager();
        }
        sInstance.configure(activity);
        return sInstance;
    }

    private void configure(SignUpActivity activity) {
        mActivity = activity;
        mFragmentManager = mActivity.getSupportFragmentManager();
    }



    @Override
    public void showFragmentCode(String title) {
        showFragment(CodeFragment.newInstance(title) , false);
    }
    @Override
    public void showSignupFragment(String title) {
        showFragment(SignUpFragment.newInstance(title) , false);


    }
    @Override
    public void showPersonalFragment(String title) {
        showFragment(PersonalFragment.newInstance(title) , false);


    }
    @Override
    public void showPasswordFragment(String title) {
        showFragment(PasswordFragment.newInstance(title) , false);


    }
    @Override
    public void showConfirmPasswordFragment(String title) {
        showFragment(ConfirmPasswordFragment.newInstance(title) , false);


    }



    private void showFragment(Fragment fragment, boolean allowStateLoss) {
        FragmentManager fm = mFragmentManager;

        @SuppressLint("CommitTransaction")
        FragmentTransaction ft = fm.beginTransaction()
            .replace(R.id.container, fragment);

        ft.addToBackStack(null);

        if (allowStateLoss || !BuildConfig.DEBUG) {
            ft.commitAllowingStateLoss();
        } else {
            ft.commit();
        }

        fm.executePendingTransactions();
    }
}
