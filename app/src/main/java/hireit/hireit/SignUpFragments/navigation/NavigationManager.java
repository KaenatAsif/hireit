package hireit.hireit.SignUpFragments.navigation;

/**
 * @author msahakyan
 */

public interface NavigationManager {




    void showFragmentCode(String title);

    void showSignupFragment(String title);

    void showPersonalFragment(String title);

    void showPasswordFragment(String title);

    void showConfirmPasswordFragment(String title);



}
