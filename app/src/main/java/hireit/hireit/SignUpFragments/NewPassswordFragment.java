package hireit.hireit.SignUpFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import hireit.hireit.AsyncResponse;
import hireit.hireit.GetProfile;
import hireit.hireit.Global;
import hireit.hireit.Models.HireitLoginResponse;
import hireit.hireit.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewPassswordFragment extends Fragment {


    public NewPassswordFragment() {
        // Required empty public constructor
    }

    EditText password;
    EditText  confirmPassword;
    Button resetPass;
    String passwordText;
    String confirmPassText;
    LinearLayout back;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_new_passsword, container, false);

        confirmPassword= (EditText) v.findViewById(R.id.confirm);
        password= (EditText) v.findViewById(R.id.password);
        resetPass= (Button) v.findViewById(R.id.reset);
        back= (LinearLayout) v.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.setCustomAnimations(R.anim.enter_from_left,R.anim.exit_to_right);
                ft.replace(R.id.container, new PasswordFragment());
                ft.addToBackStack(null);
                ft.commit();

            }
        });


        resetPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passwordText= password.getText().toString();
                confirmPassText= confirmPassword.getText().toString();
                if (passwordText.length()>5&&confirmPassText.length()>5) {

                    if (confirmPassText.equals(passwordText)) {
                        String function = "ForgotPassword?email="+Global.getNumber()+"@hire-it.co&password="+confirmPassText;
                        GetProfile asyncTask = new GetProfile(getActivity(), new AsyncResponse() {

                            @Override
                            public void processFinish(Object result) {

                                String ans = result + "";
                                JSONObject jsonObj = null;
                                try {

                                    jsonObj = new JSONObject(ans);
                                    Global.newPassword = new HireitLoginResponse(jsonObj);
                                    String result1 = Global.newPassword.getResult();

                                    if (result1.equals("Success")) {

                                        new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                                                .setTitleText("Password Changed")
                                                .setContentText("Please Login")
                                                .show();

                                        Global.setForgotPassword(false);

                                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                                        ft.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left);
                                        ft.replace(R.id.container, new SignUpFragment());
                                        ft.addToBackStack(null);
                                        ft.commit();

                                    } else {

                                        Toast.makeText(getActivity(), "Password not set", Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, function + "__" + "1");
                        asyncTask.execute();

                    } else {
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Something Went Wrong")
                                .setContentText("Password must conain 6 characters")
                                .show();
                    }

                }else {
                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Something Went Wrong")
                        .setContentText("Password must conain 6 characters")
                        .show();

            }



        }
        });
        return v;
    }

}
