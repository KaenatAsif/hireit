package hireit.hireit.SignUpFragments;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import hireit.hireit.AsyncResponse;
import hireit.hireit.Cache;
import hireit.hireit.GetProfile;
import hireit.hireit.GetSingle;
import hireit.hireit.Global;
import hireit.hireit.Models.HireitCustomerProfile;
import hireit.hireit.Models.HireitLoginResponse;
import hireit.hireit.R;
import hireit.hireit.drawer.MainActivity;


/**
 * A fragment that launches other parts of the demo application.
 */
public class PasswordFragment extends Fragment {

    EditText password;
    LinearLayout next;

    private static final String KEY_MOVIE_TITLE = "key_title";
    public static PasswordFragment newInstance(String movieTitle) {
        PasswordFragment pas = new PasswordFragment();
        Bundle args = new Bundle();

        args.putString(KEY_MOVIE_TITLE, movieTitle);
        try {
            pas.setArguments(args);
        } catch (IllegalStateException e) {
        }

        return pas;
    }
LinearLayout back;


TextView forgtPass;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_password, container,
                false);

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        LinearLayout next= (LinearLayout) view.findViewById(R.id.next);
         back= (LinearLayout) view.findViewById(R.id.back);
        password= (EditText) view.findViewById(R.id.password);
        forgtPass= (TextView) view.findViewById(R.id.forgotPAss);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.setCustomAnimations(R.anim.enter_from_left,R.anim.exit_to_right);
                ft.replace(R.id.container, new SignUpFragment());
                ft.addToBackStack(null);
                ft.commit();

            }
        });



        forgtPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left);
                ft.replace(R.id.container, new ForgotPassFragment());
                ft.addToBackStack(null);
                ft.commit();


            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                JSONObject jsonObj = null;

                final String passwordString= password.getText().toString();

                final String number1= Global.getNumber();
                if (passwordString.length()>5) {
                    Cache prefs = new Cache(getActivity());
                    prefs.setPassword(passwordString);

                    String function = "Login?uname=" + number1 + "@hire-it.co&password=" + passwordString;
                    GetProfile asyncTask = new GetProfile(getActivity(), new AsyncResponse() {

                        @Override
                        public void processFinish(Object result) {

                            String ans = result + "";
                            JSONObject jsonObj = null;
                            JSONObject profile = null;
                            try {
                                jsonObj = new JSONObject(ans);
                                profile = jsonObj.getJSONObject("CustomerProfile");

                                Global.customerProfile = new HireitCustomerProfile(profile);
                                String result1 = Global.customerProfile.getResultMessage();


                                if (result1.equals("Successfully Login")) {
                                    Toast.makeText(getActivity(), result1, Toast.LENGTH_LONG).show();
                                    Global.setKEY_LOGIN(true);


                                    //Getting data from server api
                                    String st_firstName = Global.customerProfile.getFirstName();
                                    String st_lastName = Global.customerProfile.getLastName();
                                    String st_gender = Global.customerProfile.getGender();
                                    String st_DOB = Global.customerProfile.getDateOfBirth();
                                    String number = Global.customerProfile.getMobileNumber();
                                    String refferalCode = Global.customerProfile.getRefferalCode();
                                    String walletAmount = Global.customerProfile.getWalletAmount();

                                    //Setting data in Cache class

                                    Global.setFirst_Name(st_firstName);
                                    Global.setLast_Name(st_lastName);
                                    Global.setGender(st_gender);
                                    Global.setDob(st_DOB);
                                    Global.setNumber(number);
                                    Global.setRefferalCode(refferalCode);
                                    Global.setWalletAmount(walletAmount);

                                    SharedPreferences sharedpref = getActivity().getSharedPreferences("_LoginInfo_", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedpref.edit();
                                    editor.putString("FirstName", st_firstName);
                                    editor.putString("LastName", st_lastName);
                                    editor.putString("Gender", st_gender);
                                    editor.putString("DOB", st_DOB);
                                    editor.putString("Password", passwordString);
                                    editor.putString("Number", number);
                                    editor.putString("RefferalCode", refferalCode);
                                    editor.putString("WalletAmount", walletAmount);
                                    editor.apply();

                                    String function = "SaveDeviceToken?email="+number+"@hire-it.co&devicetoken="+Global.getDeviceToken()+"&os=android";
                                    GetSingle asyncTask = new GetSingle(getActivity(), new AsyncResponse() {

                                        @Override
                                        public void processFinish(Object result) {

                                            String ans = result + "";
                                            JSONObject jsonObj = null;

                                            try {
                                                jsonObj = new JSONObject(ans);

                                                Global.saveToken = new HireitLoginResponse(jsonObj);
                                                result = Global.saveToken.getResult().toString();

                                            } catch (JSONException e) {

                                                e.printStackTrace();
                                            }

                                        }
                                    }, function + "__" + "1");
                                    asyncTask.execute();

                                    Intent goToCar = new Intent(getActivity(), MainActivity.class);
                                    goToCar.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    goToCar.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    goToCar.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

                                    startActivity(goToCar);
                                    getActivity().finish();




                                } else {

                                    Toast.makeText(getActivity(), "Login Failed", Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, function + "__" + "1");
                    asyncTask.execute();
                }
                else {
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Something Went Wrong")
                            .setContentText("Password must conain 6 characters")
                            .show();
                }

            }



            // sendVarificationCode();

                /*Intent sendCode= new Intent(getActivity(),SignUpActivity.class).putExtra("Fragments","Confirm Code");
                startActivity(sendCode);

*/
//                new SweetAlertDialog(getActivity(),SweetAlertDialog.PROGRESS_TYPE)
//                        .setTitleText("Please Wait")
//                        .show();
//







        });

    }

    private class SampleAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return getCount() ;
        }

        @Override
        public ListData getItem(int position) {
            return getItem(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = View.inflate(getActivity(), R.layout.nav_header, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            ListData item = getItem(position);

           /* TextDrawable drawable = mDrawableBuilder.build(String.valueOf(item.Name.charAt(0)), mColorGenerator.getColor(item.Name));
            holder.imageView.setImageDrawable(drawable);
            holder.view.setBackgroundColor(Color.TRANSPARENT);
            holder.textView.setText(item.Name);*/



            return convertView;
        }
    }

    private static class ViewHolder {

        private View view;
        // Name,[ConsultationType],[ConsultationPaymentMethods],[ConsultationCharges]," +
        // " [InsuranceProducts],[Complaint],[Diagnosis],[Prescription],[Tests],[Remarks]

        private TextView tvDate;
        private TextView tvDoctor;
        private TextView tvPrescription;
        private TextView tvTest;
        private TextView tvRemarks;




        private ViewHolder(View view) {
            this.view = view;


        }
    }

    private static class ListData {

        private String ID;
        private String Date;
        private String Doctor;
        private String Prescription;
        private String Test;
        private String Remarks;

        public ListData( String ID, String D, String Doc, String pres, String test,String rem) {
            this.ID = ID;
            this.Date = D;
            this.Doctor = Doc;
            this.Prescription=pres;
            this.Test= test;
            this.Remarks=rem;
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        //item.setIcon(null);
    }

   /* public void saveInfo(String number, String password) {
        SharedPreferences sharedpref = getActivity().getSharedPreferences("_LoginInfo_", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpref.edit();
        editor.putString("Number", number);
        editor.putString("Password", password);
        editor.apply();
//        Intent n=new Intent(_Splash.this,MainPage.class).putExtra("",userName);
    }
*/


}