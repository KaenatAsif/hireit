package hireit.hireit.SignUpFragments;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;
import hireit.hireit.AsyncResponse;
import hireit.hireit.Cache;
import hireit.hireit.GetProfile;
import hireit.hireit.Global;
import hireit.hireit.Models.HireitLoginResponse;
import hireit.hireit.R;


/**
 * A fragment that launches other parts of the demo application.
 */
public class SignUpFragment extends Fragment {
    private static final String EMAIL = "email";
    private static final String USER_POSTS = "user_posts";
    private static final String public_profile = "public_profile";

    private CallbackManager mCallbackManager;
     LinearLayout mLoginButton;
    TextView text;
Pattern Number_Pattern;
    Matcher matcher;


    private static final String TAG = "PhoneAuthActivity";

    private static final String KEY_VERIFY_IN_PROGRESS = "key_verify_in_progress";

    private static final int STATE_INITIALIZED = 1;
    private static final int STATE_CODE_SENT = 2;
    private static final int STATE_VERIFY_FAILED = 3;
    private static final int STATE_VERIFY_SUCCESS = 4;
    private static final int STATE_SIGNIN_FAILED = 5;
    private static final int STATE_SIGNIN_SUCCESS = 6;
    private static final String KEY_MOVIE_TITLE = "key_title";
    public SignUpFragment(){

    }

    public static SignUpFragment newInstance(String movieTitle) {

        SignUpFragment sign = new SignUpFragment();
        Bundle args = new Bundle();
        args.putString(KEY_MOVIE_TITLE, movieTitle);
        try {
            sign.setArguments(args);
        } catch (IllegalStateException e) {
        }

        return sign;
    }

    // [START declare_auth]

    EditText PhoneNumber;
    ImageView submit;
    // [END declare_auth]
    ArrayList<String> list;

    private boolean mVerificationInProgress = false;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private FirebaseAuth mAuth;
    String code =null;
    String phoneNo;
    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    SharedPreferences sp;

    String number1;
    String number;
   Spinner country;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_signup1, container,
                false);



        PhoneNumber= (EditText) v.findViewById(R.id.phone);
        submit= (ImageView) v.findViewById(R.id.submit);
        country= (Spinner) v.findViewById(R.id.country);
        final SharedPreferences prefs = getActivity().getSharedPreferences(
                getActivity().getPackageName(), Context.MODE_PRIVATE);




        list = new ArrayList<String>();
        list.add("+44");
        list.add("+92");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        country.setAdapter(adapter);

        Number_Pattern=Pattern.compile("^07[0-9'@s]{9,9}$");
        matcher=Number_Pattern.matcher(PhoneNumber.getText().toString());

        // mAuth = FirebaseAuth.getInstance();
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            }

            @Override
            public void onVerificationFailed(FirebaseException e) {

                Toast.makeText(getActivity(), "Number not verified. Try again.", Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);
                code = s;


                Toast.makeText(getActivity(), "code Sent", Toast.LENGTH_SHORT).show();
               // Toast.makeText(getActivity(), "Code is: "+code+"", Toast.LENGTH_SHORT).show();
                Cache prefs= new Cache(getActivity());
                Cache.setKeyCodeResendToken(forceResendingToken);
                prefs.setKeyConfirmationCode(code);
            }
        };

        country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (country.getSelectedItem().toString().equals("+92")){
                    PhoneNumber.setHint("3XX XXXXXXX");

                }
                else {

                    PhoneNumber.setHint("7XXX XXXXXX");

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int index= country.getSelectedItemPosition()-1;




                number = country.getSelectedItem().toString()+PhoneNumber.getText().toString();
                Global.setNumberCode(number);

                if (number.startsWith("+")){

                    number1= number.replace("+","");

                }
                else {
                    number1 = number;
                }



                ConnectivityManager manager = (ConnectivityManager)
                        getActivity().getSystemService(getActivity().CONNECTIVITY_SERVICE);
/*
 * 3G confirm
 */
                Boolean is3g = manager.getNetworkInfo(
                        ConnectivityManager.TYPE_MOBILE).isConnectedOrConnecting();
                Boolean isWifi = manager.getNetworkInfo(
                        ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting();
                if (!is3g&&!isWifi) {

                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("No Internet")
                            .setContentText("Please turn on Wifi or mobile data ")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })
                            .show();



                }
                else {

                    if (number.length() > 0/*matcher.matches()*/) {
                        Cache prefs = new Cache(getActivity());
                        prefs.setNumber(number1);

                        Global.setNumber(number1);
                        String function = "CheckRegistration?uname=" + number1 + "@hire-it.co";
                        GetProfile asyncTask = new GetProfile(getActivity(), new AsyncResponse() {

                            @Override
                            public void processFinish(Object result) {

                                String ans = result + "";
                                JSONObject jsonObj = null;

                                try {
                                    jsonObj = new JSONObject(ans);

                                    Global.loginObj = new HireitLoginResponse(jsonObj);
                                    result = Global.loginObj.getResult().toString();
                                    if (result.equals("True")) {
                                        //  Toast.makeText(getActivity(), "Success", Toast.LENGTH_LONG).show();

                                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                                        ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
                                        ft.replace(R.id.container, new PasswordFragment());
                                        ft.addToBackStack(null);
                                        ft.commit();
                                    } else {
                                        Cache.showLoadingPopup(getActivity(), "Sending Code");
                                        sendVarificationCode();
                                        Cache.dismissLoadingPopup();
                                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                                        ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
                                        ft.replace(R.id.container, new CodeFragment());
                                        ft.addToBackStack(null);
                                        ft.commit();


                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, function + "__" + "1");
                        asyncTask.execute();
                    } else {
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Something Went Wrong")
                                .setContentText("Not a valid Number")
                                .show();
                    }
                }

            }



               // sendVarificationCode();

                /*Intent sendCode= new Intent(getActivity(),SignUpActivity.class).putExtra("Fragments","Confirm Code");
                startActivity(sendCode);

*/
//                new SweetAlertDialog(getActivity(),SweetAlertDialog.PROGRESS_TYPE)
//                        .setTitleText("Please Wait")
//                        .show();
//







        });


        return v;
    }

    private void sendVarificationCode()
    {


        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                number,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                getActivity(),               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks

    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {


    }

    private class SampleAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return getCount() ;
        }

        @Override
        public ListData getItem(int position) {
            return getItem(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = View.inflate(getActivity(), R.layout.nav_header, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            ListData item = getItem(position);

           /* TextDrawable drawable = mDrawableBuilder.build(String.valueOf(item.Name.charAt(0)), mColorGenerator.getColor(item.Name));
            holder.imageView.setImageDrawable(drawable);
            holder.view.setBackgroundColor(Color.TRANSPARENT);
            holder.textView.setText(item.Name);*/



            return convertView;
        }
    }

    private static class ViewHolder {

        private View view;
        // Name,[ConsultationType],[ConsultationPaymentMethods],[ConsultationCharges]," +
        // " [InsuranceProducts],[Complaint],[Diagnosis],[Prescription],[Tests],[Remarks]

        private TextView tvDate;
        private TextView tvDoctor;
        private TextView tvPrescription;
        private TextView tvTest;
        private TextView tvRemarks;

        private ViewHolder(View view) {
            this.view = view;


        }
    }

    private static class ListData {

        private String ID;
        private String Date;
        private String Doctor;
        private String Prescription;
        private String Test;
        private String Remarks;

        public ListData( String ID, String D, String Doc, String pres, String test,String rem) {
            this.ID = ID;
            this.Date = D;
            this.Doctor = Doc;
            this.Prescription=pres;
            this.Test= test;
            this.Remarks=rem;
        }
    }




    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        //item.setIcon(null);
    }
}