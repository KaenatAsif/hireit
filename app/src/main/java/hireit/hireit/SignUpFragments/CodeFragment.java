package hireit.hireit.SignUpFragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

import cn.pedant.SweetAlert.SweetAlertDialog;
import hireit.hireit.Cache;
import hireit.hireit.Global;
import hireit.hireit.R;


/**
 * A fragment that launches other parts of the demo application.
 */
public class CodeFragment extends Fragment {
    SweetAlertDialog pDialog;
    String code = null;
    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private static final String KEY_MOVIE_TITLE = "key_title";
    private View view;

    public CodeFragment() {

    }

    Button verify;
    Button resend;

    private FirebaseAuth mAuth;
    EditText getCodeText;

    public static CodeFragment newInstance(String movieTitle) {
        CodeFragment code = new CodeFragment();
        Bundle args = new Bundle();
        args.putString(KEY_MOVIE_TITLE, movieTitle);
        try {
            code.setArguments(args);
        } catch (IllegalStateException e) {
        }

        return code;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_code, container,
                false);
         final Cache prefs= new Cache(getActivity());
        mAuth = FirebaseAuth.getInstance();
        getCodeText = (EditText) v.findViewById(R.id.getCodeText);

        resend = (Button) v.findViewById(R.id.resend);

        verify = (Button) v.findViewById(R.id.verify);

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

             //   signInWithPhoneAuthCredential(phoneAuthCredential);


            }

            @Override
            public void onVerificationFailed(FirebaseException e) {


               Toast.makeText(getActivity(), "Number not verified. Try again.", Toast.LENGTH_SHORT).show();
             //   Toast.makeText(getActivity(), e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                FragmentTransaction ft = getFragmentManager().beginTransaction();

                ft.replace(R.id.container, new SignUpFragment());
                ft.setCustomAnimations(R.anim.enter_from_left,R.anim.exit_to_right);
                ft.addToBackStack(null);
                ft.commit();

            }

            @Override
            public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);
                code = s;

                Toast.makeText(getActivity(), "code Sent", Toast.LENGTH_SHORT).show();
             //   Toast.makeText(getActivity(), "Code is: "+code+"", Toast.LENGTH_SHORT).show();
                Cache prefs= new Cache(getActivity());
                Cache.setKeyCodeResendToken(forceResendingToken);
                prefs.setKeyConfirmationCode(code);
            }

        };

        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               Cache.showLoadingPopup(getActivity(),"Resending Code");

                Toast.makeText(getActivity(), "code Sent", Toast.LENGTH_SHORT).show();


                resendVerificationCode(Global.getNumberCode(),Cache.getKeyCodeResendToken());




            }
        });

        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Cache.showLoadingPopup(getActivity(),"Verifying Code");

                String code = getCodeText.getText().toString();
                if (code.length()>0) {
                    PhoneAuthCredential credential = PhoneAuthProvider.getCredential(Cache.getKeyConfirmationCode(), code);
                    signInWithPhoneAuthCredential(credential);
                }
                else{
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Something Went Wrong")
                            .setContentText("Code Cannot be empty")
                            .show();

                }

                Cache.dismissLoadingPopup();
            }
        });

        return v;
    }


    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {

                            // Sign in success, update UI with the signed-in user's information
                            //Log.d(TAG, "signInWithCredential:success");

                            FirebaseUser user = task.getResult().getUser();
                          //  Toast.makeText(getActivity(), "Task Completed", Toast.LENGTH_SHORT).show();
                            new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Code Verified")
                                    .setContentText("Enter your details")
                                    .show();

                            if (Global.isForgotPassword()){
                                FragmentTransaction ft = getFragmentManager().beginTransaction();
                                ft.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left);
                                ft.replace(R.id.container, new NewPassswordFragment());
                                ft.addToBackStack(null);
                                ft.commit();


                            }


                            else {
                                FragmentTransaction ft = getFragmentManager().beginTransaction();
                                ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
                                ft.replace(R.id.container, new PersonalFragment());
                                ft.addToBackStack(null);
                                ft.commit();

                            }
                        } else {
                            // Sign in failed, display a message and update the UI
                            Toast.makeText(getActivity(), "Incorrect Code", Toast.LENGTH_SHORT).show();
                            //Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                              //  Toast.makeText(getActivity(), task.getException().toString(), Toast.LENGTH_SHORT).show();


                            }
                        }
                    }
                });
    }

    // [START resend_verification]
    public void resendVerificationCode(String phoneNumber,
                                       PhoneAuthProvider.ForceResendingToken token) {

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                getActivity(),           //a reference to an activity if this method is in a custom service
                mCallbacks,token);
        // resending
        // [END start_phone_auth]
        Cache.dismissLoadingPopup();

    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

    }

}


