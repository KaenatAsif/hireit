package hireit.hireit;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import com.google.firebase.auth.PhoneAuthProvider;

import java.util.Map;
import java.util.Set;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by WebDoc Kaenat on 6/13/2018.
 */

public class Cache implements SharedPreferences {
    public static SweetAlertDialog pDialog = null;

    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    Context ctx;



    public  boolean KEY_LOGIN=false;
    public   String KEY_ID="id";
    public   String KEY_NAME="name";
    public   String KEY_PASSWORD="password";
    public   String KEY_EMAIL="email";
    public   String KEY_Phone="phone";
    public   String KEY_GENDER="gender";
    public static String KEY_CONFIRMATION_CODE="code";


    public static  PhoneAuthProvider.ForceResendingToken KEY_CODE_RESEND_TOKEN=null;


    public static String first_Name;
    public static String last_Name;
    public  static String gender;
    public  static String dob;
    public static String password;
    public static String number;



    public Cache(Context cont)
    {
        this.ctx=cont;
        prefs=ctx.getSharedPreferences("hireit", Context.MODE_PRIVATE);
        editor=prefs.edit();
    }


    public void setFirst_Name(String firstName) {
        editor.putString(first_Name, firstName);
        editor.commit();
    }

    public String getFirst_Name() {
        return(prefs.getString(first_Name, null));
    }

    public String getLast_Name() {
        return(prefs.getString(last_Name, null));
    }

    public void setLast_Name(String lastName) {
        editor.putString(last_Name, lastName);
        editor.commit();
    }

    public String getGender() {
        return(prefs.getString(gender, null));
    }

    public void setGender(String Gender) {
        editor.putString(gender, Gender);
        editor.commit();
    }

    public String getDob() {
        return(prefs.getString(dob, null));
    }

    public void setDob(String Dob) {
        editor.putString(dob, Dob);
        editor.commit();
    }

    public String getPaswword() {
        return(prefs.getString(password, null));
    }

    public void setPassword(String Password) {
        editor.putString(password, Password);
        editor.commit();
    }

    public boolean isKeyLogin() {
        return prefs.getBoolean(String.valueOf(KEY_LOGIN),false);
    }

    public void setKeyLogin(boolean keyLogin) {
        editor.putBoolean(String.valueOf(KEY_LOGIN), keyLogin);
        editor.commit();
    }

    public String getNumber() {
        return(prefs.getString(number, null));
    }

    public void setNumber(String Number) {
        editor.putString(number, Number);
        editor.commit();
    }

    public void setKEY_Phone(String kEY_phone) {
        editor.putString(KEY_Phone, kEY_phone);
        editor.commit();
    }

    public String getKEY_Phone() {
        return(prefs.getString(KEY_Phone, null));
    }

    public void setKeyGender(String keyGender) {
        editor.putString(KEY_GENDER, keyGender);
        editor.commit();
    }


    @Override
    public Map<String, ?> getAll() {
        return null;
    }

    @Nullable
    @Override
    public String getString(String s, @Nullable String s1) {
        return null;
    }

    @Nullable
    @Override
    public Set<String> getStringSet(String s, @Nullable Set<String> set) {
        return null;
    }

    @Override
    public int getInt(String s, int i) {
        return 0;
    }

    @Override
    public long getLong(String s, long l) {
        return 0;
    }

    @Override
    public float getFloat(String s, float v) {
        return 0;
    }

    @Override
    public boolean getBoolean(String s, boolean b) {
        return false;
    }

    @Override
    public boolean contains(String s) {
        return false;
    }

    @Override
    public Editor edit() {
        return null;
    }

    @Override
    public void registerOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {

    }

    @Override
    public void unregisterOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {

    }


    public static String getKeyConfirmationCode() {
        return KEY_CONFIRMATION_CODE;
    }

    public static void setKeyConfirmationCode(String keyConfirmationCode) {
        KEY_CONFIRMATION_CODE = keyConfirmationCode;
    }

    public static PhoneAuthProvider.ForceResendingToken getKeyCodeResendToken() {
        return KEY_CODE_RESEND_TOKEN;
    }

    public static void setKeyCodeResendToken(PhoneAuthProvider.ForceResendingToken keyCodeResendToken) {
        KEY_CODE_RESEND_TOKEN = keyCodeResendToken;
    }

    public static void showLoadingPopup(Context con, String a) {
        pDialog = new SweetAlertDialog(con, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(R.color.yellowBackground);
        pDialog.setTitleText(a);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    public static void showSuccessPopup(Context con, String b){
        pDialog = new SweetAlertDialog(con, SweetAlertDialog.SUCCESS_TYPE);
        pDialog.getProgressHelper().setBarColor(R.color.yellowBackground);
        pDialog.setTitleText(b);
        pDialog.show();

    }

    public static void dismissLoadingPopup() {
        if (pDialog != null)
            try {
                pDialog.dismiss();
            } catch (IllegalArgumentException e) {
                return;
            }
    }
}
