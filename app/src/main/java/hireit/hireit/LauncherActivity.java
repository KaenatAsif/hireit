package hireit.hireit;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.google.firebase.iid.FirebaseInstanceId;

import hireit.hireit.SignUpFragments.SignUpActivity;
import hireit.hireit.drawer.MainActivity;

public class LauncherActivity extends AppCompatActivity {
    ImageView logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);




        logo = (ImageView) findViewById(R.id.logo);
        Animation animBlink = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.blink_animation);

        String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        String a= FirebaseInstanceId.getInstance().getToken();
        Global.setDeviceToken(a);

        // set animation listener
        logo.startAnimation(animBlink);
        animBlink.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {



            }

            @Override
            public void onAnimationEnd(Animation animation) {



                    checkInfo();

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        // load the animation








/*
        Cache pre= new Cache(this);
        pre.setNumber(null);
        pre.setPassword(null);
        pre.setFirst_Name(null);*/




    }

    private void checkInfo() {



        // start the animation


        SharedPreferences sharedpref = getSharedPreferences("_LoginInfo_", Context.MODE_PRIVATE);
        String password = sharedpref.getString("Password", "0");
        String number  = sharedpref.getString("Number", "0");

        if (number=="0"|| password=="0"){

            Intent a= new Intent(this, SignUpActivity.class);
            startActivity(a);
            finish();
        }
        else{
            Intent i= new Intent(this, MainActivity.class);

            startActivity(i);
            finish();
        }


            }
/*

        } else {
            if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED)
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{android.Manifest.permission.RECORD_AUDIO, android.Manifest.permission.CAMERA, android.Manifest.permission.READ_PHONE_STATE},
                        1);
*/


        }



