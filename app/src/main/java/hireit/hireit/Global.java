package hireit.hireit;

import android.content.Context;

import com.google.firebase.auth.PhoneAuthProvider;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import hireit.hireit.Models.BookedCarsList;
import hireit.hireit.Models.ClaimsHistory;
import hireit.hireit.Models.HireitCarPicture;
import hireit.hireit.Models.HireitCarPriceContract;
import hireit.hireit.Models.HireitCarsNew;
import hireit.hireit.Models.HireitCarsResponse;
import hireit.hireit.Models.HireitCarsResult;
import hireit.hireit.Models.HireitCustomerLedgerDetialsResult;
import hireit.hireit.Models.HireitCustomerProfile;
import hireit.hireit.Models.HireitDocument;
import hireit.hireit.Models.HireitGetDocumentsResponse;
import hireit.hireit.Models.HireitGetLedgerResponse;
import hireit.hireit.Models.HireitLedgerDetailResponse;
import hireit.hireit.Models.HireitLoginResponse;
import hireit.hireit.Models.HireitUserProfileResponse;

/**
 * Created by WebDoc Kaenat on 6/13/2018.
 */

public class Global {
    public static SweetAlertDialog pDialog = null;

    Context ctx;

    public   String KEY_ID="id";

    static int NotificationId=0;

    public static int getNotificationId() {
        return NotificationId;
    }

    public static void setNotificationId(int notificationId) {
        NotificationId = notificationId;
    }

    public   String KEY_NAME="name";
    public   String KEY_PASSWORD="password";
    public   String KEY_EMAIL="email";
    public   String KEY_Phone="phone";
    public   String KEY_GENDER="gender";
    public static String KEY_CONFIRMATION_CODE="code";
    public static boolean booking=false;

    public static boolean signingUp=false;

    public static boolean isSigningUp() {
        return signingUp;
    }

    public static void setSigningUp(boolean signingUp) {
        Global.signingUp = signingUp;
    }

    public static boolean isBooking() {
        return booking;
    }

    public static void setBooking(boolean booking) {
        Global.booking = booking;
    }

    public static  HireitLoginResponse saveToken;

    public static  HireitLoginResponse submitDoc;

    public static String deviceToken;

    public static String getDeviceToken() {
        return deviceToken;
    }

    public static void setDeviceToken(String deviceToken) {
        Global.deviceToken = deviceToken;
    }

    public static boolean forgotPassword=false;

    public static boolean isForgotPassword() {
        return forgotPassword;
    }

    public static void setForgotPassword(boolean forgotPassword) {
        Global.forgotPassword = forgotPassword;
    }

    public static boolean isLeeds=false;
    public static boolean isKirklees=false;
    public static boolean isBradFord=false;
    public static boolean isWakefield=false;
    public static boolean raiseClaim=false;

    public static boolean isRaiseClaim() {
        return raiseClaim;
    }

    public static void setRaiseClaim(boolean raiseClaim) {
        Global.raiseClaim = raiseClaim;
    }

    public static String Weeks;

    public static String carId;
    public static String claimNo;

    public static String getClaimNo() {
        return claimNo;
    }

    public static void setClaimNo(String claimNo) {
        Global.claimNo = claimNo;
    }

    public static String getCarId() {
        return carId;
    }

    public static void setCarId(String carId) {
        Global.carId = carId;
    }

    public static String getWeeks() {
        return Weeks;
    }

    public static void setWeeks(String weeks) {
        Weeks = weeks;
    }

    public static boolean UploadPhotos=false;


    public static HireitLoginResponse checkDocuments;
    public static HireitLoginResponse confirmBooking;
    public static HireitLoginResponse claims;
    public static HireitLoginResponse newPassword;
    public static HireitGetLedgerResponse ledgerResponse;

    public static HireitGetDocumentsResponse documentsResponse;

    public static ClaimsHistory claimsHistoryResult;
    public static HireitDocument document;

    public static boolean isUploadPhotos() {
        return UploadPhotos;
    }

    public static void setUploadPhotos(boolean goUploadPhotos) {
        Global.UploadPhotos = goUploadPhotos;
    }

    public static boolean isLeeds() {
        return isLeeds;
    }

    public static void setLeeds(boolean leeds) {
        isLeeds = leeds;
    }

    public static boolean isKirklees() {
        return isKirklees;
    }

    public static void setKirklees(boolean kirklees) {
        isKirklees = kirklees;
    }

    public static boolean isBradFord() {
        return isBradFord;
    }

    public static void setBradFord(boolean bradFord) {
        isBradFord = bradFord;
    }

    public static boolean isWakefield() {
        return isWakefield;
    }

    public static void setWakefield(boolean wakefield) {
        isWakefield = wakefield;
    }

    public static  PhoneAuthProvider.ForceResendingToken KEY_CODE_RESEND_TOKEN=null;

    public static HireitLoginResponse loginObj;

    public static HireitUserProfileResponse updateProfile;


    public static HireitLoginResponse checkRefferal;

    public static BookedCarsList bookedCarsList;

    public static HireitCarsResponse carsResponse;

    public static HireitCarsResult carsResult;
    public static HireitCarsResult bookedCars;
    public static HireitCarsNew carsNew;
    public static HireitCarPriceContract carPriceContract;
    public static HireitCarPicture carPicture;

    public static HireitUserProfileResponse profileResponse;

    public static HireitCustomerProfile customerProfile;

    public static HireitLedgerDetailResponse ledgerDetailResponse;

    public static HireitCustomerLedgerDetialsResult detialsResult;

    public static String first_Name;
    public static String last_Name;
    public  static String gender;
    public  static String dob;
    public static String password;
    public static String number;

    public static String numberCode;

    public static String getNumberCode() {
        return numberCode;
    }

    public static void setNumberCode(String numberCode) {
        Global.numberCode = numberCode;
    }

    public static String refferalCode;
    public static String walletAmount;
    public  static boolean KEY_LOGIN=false;
    public static int arrayIndex;

    public static String itemCarId;

    public static String getItemCarId() {
        return itemCarId;
    }

    public static void setItemCarId(String itemCarId) {
        Global.itemCarId = itemCarId;
    }

    public static int getArrayIndex() {
        return arrayIndex;
    }

    public static void setArrayIndex(int arrayIndex) {
        Global.arrayIndex = arrayIndex;
    }

    public static ArrayList<HireitCarsNew> LeedsArray= new ArrayList<HireitCarsNew>();
    public static ArrayList<HireitCarsNew> KirkleesArray=new ArrayList<HireitCarsNew>();
    public static ArrayList<HireitCarsNew> WakefieldArray= new ArrayList<HireitCarsNew>();
    public static ArrayList<HireitCarsNew> BradfordArray=new ArrayList<HireitCarsNew>();

    public static ArrayList<HireitCarsResult> mainCarArray;

    public static ArrayList<HireitCarsResult> getMainCarArray() {
        return mainCarArray;
    }

    public static void setMainCarArray(ArrayList<HireitCarsResult> mainCarArray) {
        Global.mainCarArray = mainCarArray;
    }

    public static ArrayList<HireitCarsNew> getLeedsArray() {
        return LeedsArray;
    }

    public static void setLeedsArray(ArrayList<HireitCarsNew> leedsArray) {
        LeedsArray = leedsArray;
    }

    public static ArrayList<HireitCarsNew> getKirkleesArray() {
        return KirkleesArray;
    }

    public static void setKirkleesArray(ArrayList<HireitCarsNew> kirkleesArray) {
        KirkleesArray = kirkleesArray;
    }

    public static ArrayList<HireitCarsNew> getWakefieldArray() {
        return WakefieldArray;
    }

    public static void setWakefieldArray(ArrayList<HireitCarsNew> wakefieldArray) {
        WakefieldArray = wakefieldArray;
    }

    public static ArrayList<HireitCarsNew> getBradfordArray() {
        return BradfordArray;
    }

    public static void setBradfordArray(ArrayList<HireitCarsNew> bradfordArray) {
        BradfordArray = bradfordArray;
    }

    public static String picLink;

    public static String getPicLink() {
        return picLink;
    }

    public static void setPicLink(String picLink) {
        Global.picLink = picLink;
    }

    public static void setpDialog(SweetAlertDialog pDialog) {
        Global.pDialog = pDialog;
    }

    public static void setLoginObj(HireitLoginResponse loginObj) {
        Global.loginObj = loginObj;
    }

    public static void setProfileResponse(HireitUserProfileResponse profileResponse) {
        Global.profileResponse = profileResponse;
    }

    public static void setCustomerProfile(HireitCustomerProfile customerProfile) {
        Global.customerProfile = customerProfile;
    }

    public static void setKEY_LOGIN(boolean KEY_LOGIN) {
        Global.KEY_LOGIN = KEY_LOGIN;
    }

    public void setKEY_ID(String KEY_ID) {
        this.KEY_ID = KEY_ID;
    }

    public static void setFirst_Name(String first_Name) {
        Global.first_Name = first_Name;
    }

    public static void setLast_Name(String last_Name) {
        Global.last_Name = last_Name;
    }

    public static void setGender(String gender) {
        Global.gender = gender;
    }

    public static void setDob(String dob) {
        Global.dob = dob;
    }

    public static void setPassword(String password) {
        Global.password = password;
    }

    public static void setNumber(String number) {
        Global.number = number;
    }



    public static String getRefferalCode() {
        return refferalCode;
    }

    public static void setRefferalCode(String refferalCode) {
        Global.refferalCode = refferalCode;
    }

    public static String getWalletAmount() {
        return walletAmount;
    }

    public static void setWalletAmount(String walletAmount) {
        Global.walletAmount = walletAmount;
    }

    public boolean isKEY_LOGIN() {
        return KEY_LOGIN;
    }

    public String getKEY_ID() {
        return KEY_ID;
    }

    public static HireitLoginResponse getLoginObj() {
        return loginObj;
    }

    public static HireitUserProfileResponse getProfileResponse() {
        return profileResponse;
    }

    public static HireitCustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public static String getFirst_Name() {
        return first_Name;
    }

    public static String getLast_Name() {
        return last_Name;
    }

    public static String getGender() {
        return gender;
    }

    public static String getDob() {
        return dob;
    }

    public static String getPassword() {
        return password;
    }

    public static String getNumber() {
        return number;
    }

    public static String getKeyConfirmationCode() {
        return KEY_CONFIRMATION_CODE;
    }

    public static void setKeyConfirmationCode(String keyConfirmationCode) {
        KEY_CONFIRMATION_CODE = keyConfirmationCode;
    }

    public static PhoneAuthProvider.ForceResendingToken getKeyCodeResendToken() {
        return KEY_CODE_RESEND_TOKEN;
    }

    public static void setKeyCodeResendToken(PhoneAuthProvider.ForceResendingToken keyCodeResendToken) {
        KEY_CODE_RESEND_TOKEN = keyCodeResendToken;
    }

    public static void showLoadingPopup(Context con, String a) {
        pDialog = new SweetAlertDialog(con, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(R.color.yellowBackground);
        pDialog.setTitleText(a);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    public static void showSuccessPopup(Context con, String b){
        pDialog = new SweetAlertDialog(con, SweetAlertDialog.SUCCESS_TYPE);
        pDialog.getProgressHelper().setBarColor(R.color.yellowBackground);
        pDialog.setTitleText(b);
        pDialog.show();

    }

    public static void dismissLoadingPopup() {
        if (pDialog != null)
            try {
                pDialog.dismiss();
            } catch (IllegalArgumentException e) {
                return;
            }
    }
}
