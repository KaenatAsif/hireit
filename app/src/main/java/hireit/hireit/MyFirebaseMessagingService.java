package hireit.hireit;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.util.Log;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import hireit.hireit.SignUpFragments.MyjobService;
import hireit.hireit.drawer.MainActivity;

import static android.content.ContentValues.TAG;


/**
 * Created by WebDoc Kaenat on 7/30/2018.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

public int count=0;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // ...

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());


        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
           // scheduleJob();
            //Intent stopIntent = new Intent(getApplicationContext(), MainActivity.class);

            Map<String, String> params = remoteMessage.getData();
            JSONObject object = new JSONObject(params);
            //Log.e("JSON_OBJECT", object.toString());
            //String id= remoteMessage.getMessageId();


            try {
                /*PugNotification.with(getApplicationContext())
                        .load()
                        .smallIcon(R.drawable.hireit_logo)
                        .autoCancel(true)
                        .largeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.hireit_logo))
                        .title("Hire-It")
                        .message(object.getString("message").toString())
                        .bigTextStyle(object.getString("message").toString())
                .flags(Notification.DEFAULT_ALL)
                        .simple()
                        .build();*/

                // prepare intent which is triggered if the
// notification is selected

                Intent intent = new Intent(this, MainActivity.class);
// use System.currentTimeMillis() to have a unique ID for the pending intent
                PendingIntent pIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent,PendingIntent.FLAG_ONE_SHOT);

// build notification
// the addAction re-use the same intent to keep the example short
                Notification n  = new Notification.Builder(this)
                        .setContentTitle("Hire-It")
                        .setContentText(object.getString("message").toString())
                        .setSmallIcon(R.drawable.hireit_logo)
                        .setContentIntent(pIntent)
                        .setStyle(new Notification.BigTextStyle()
                                .bigText(object.getString("message").toString()))
                        .setAutoCancel(true)
                        .build();

                //int id=Global.getNotificationId();
              //  Global.setNotificationId(id+1);
               int id=Integer.parseInt(object.getString("id").toString());

                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                notificationManager.notify(id,n);


            } catch (JSONException e) {
                e.printStackTrace();
            }


            new AudioPlayer(getApplicationContext()).playMsg();

        }

        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());


        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }



    private void scheduleJob() {
        // [START dispatch_job]
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        Job myJob = dispatcher.newJobBuilder()
                .setService(MyjobService.class)
                .setTag("my-job-tag")
                .build();
        dispatcher.schedule(myJob);
        // [END dispatch_job]
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }
}
