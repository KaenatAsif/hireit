package hireit.hireit;

import android.content.Context;


import org.json.JSONException;

import java.io.IOException;

/**
 * Created by faheem on 24/08/2017.
 */

public class GetNetwork extends android.os.AsyncTask<Void, Void, Object> {
    //ProgressDialog loading;
    public AsyncResponse delegate = null;//Call back interface
    String a;
    Context Contextt_;

    public GetNetwork(Context c, AsyncResponse asyncResponse, String a) {
        this.Contextt_ = c;
        delegate = asyncResponse;//Assigning call back interfacethrough constructor
        this.a = a;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Cache.showLoadingPopup(Contextt_,"Please Wait");



    }

    @Override
    protected void onPostExecute(Object s) {
        super.onPostExecute(s);
        Cache.dismissLoadingPopup();
        try {
            delegate.processFinish(s);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Object doInBackground(Void... params) {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 172.217.9.142");
            int exitValue = ipProcess.waitFor();
            if (exitValue == 0){
                Cache.dismissLoadingPopup();
                return true;}
            else {
                Cache.dismissLoadingPopup();
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return false;
    }
}