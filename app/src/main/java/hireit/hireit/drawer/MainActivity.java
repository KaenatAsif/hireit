package hireit.hireit.drawer;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.ColorRes;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import hireit.hireit.AsyncResponse;
import hireit.hireit.Cache;
import hireit.hireit.GetProfile;
import hireit.hireit.GetSingle;
import hireit.hireit.Global;
import hireit.hireit.Models.HireitCarsNew;
import hireit.hireit.Models.HireitCarsResult;
import hireit.hireit.Models.HireitLoginResponse;
import hireit.hireit.Models.HireitUserProfileResponse;
import hireit.hireit.R;
import hireit.hireit.SignUpFragments.SignUpActivity;
import hireit.hireit.drawer.DrawerFragments.BookedCars;
import hireit.hireit.drawer.DrawerFragments.Claims;
import hireit.hireit.drawer.DrawerFragments.Documents;
import hireit.hireit.drawer.DrawerFragments.MainCarList;
import hireit.hireit.drawer.DrawerFragments.ProfileFragment;
import hireit.hireit.drawer.DrawerFragments.WalletFragment;


public class MainActivity extends AppCompatActivity {
    private DrawerLayout mDrawer;
    private ActionBarDrawerToggle mToggle;
    private Toolbar mToolBar;
    NavigationView nvDrawer;
    Cache preferences;
    JSONArray jarr;
    ImageView refresh;

    ArrayList<HireitCarsNew> LeedsArray;
    ArrayList<HireitCarsNew> BradfordArray;
    ArrayList<HireitCarsNew> KirkleesArray;
    ArrayList<HireitCarsNew> WakefieldArray;

    private String carsName;
    private String carsCity;

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        setDefault();
    }

    ColorStateList colorStateList2;

    int[][] states;
    int[] colors;

    String firstName;
    String lastName;
    String gender;
    String dob;
    String refferalCode;
    String walletAmount;
    String number;
    TextView wallet;
    TextView navUsername;

    String[] date;
    String dob1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        preferences = new Cache(getApplicationContext());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        ConnectivityManager manager = (ConnectivityManager)
                getSystemService(MainActivity.CONNECTIVITY_SERVICE);

        Boolean is3g = manager.getNetworkInfo(
                ConnectivityManager.TYPE_MOBILE).isConnectedOrConnecting();
        Boolean isWifi = manager.getNetworkInfo(
                ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting();

        if (!is3g && !isWifi) {

            new SweetAlertDialog(MainActivity.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("No Internet")
                    .setContentText("Please turn on Wifi or mobile data ")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                            sweetAlertDialog.dismissWithAnimation();

                        }
                    })
                    .show();


        }


        LeedsArray = new ArrayList<>();
        WakefieldArray = new ArrayList<>();
        KirkleesArray = new ArrayList<>();
        BradfordArray = new ArrayList<>();





        if (Global.isUploadPhotos()) {
            Fragment fragment = new Documents();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, fragment).addToBackStack(null).commit();


        } else if (Global.isSigningUp()) {
            Fragment fragment = new ProfileFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, fragment).addToBackStack(null).commit();


        } else {
            String function1 = "cars";
            GetProfile asyncTask1 = new GetProfile(MainActivity.this, new AsyncResponse() {
                @Override
                public void processFinish(Object result) {

                    String ans = result + "";
                    JSONObject jsonObj = null;
                    JSONArray caresult = null;
                    JSONObject details;
                    JSONObject details1;

                    try {
                        jsonObj = new JSONObject(ans);

                        caresult = jsonObj.getJSONArray("CarsResult");

                        if (caresult.length()>0) {

                            for (int i = 0; i < caresult.length(); i++) {

                                details = caresult.getJSONObject(i);

                                details1 = details.getJSONObject("carsNew");
                                Global.carsResult = new HireitCarsResult(details);

                                Global.carsNew = new HireitCarsNew(details1);

                                carsName = Global.carsResult.getCarsNew().getCarName();
                                carsCity = Global.carsResult.getCarsNew().getCarCity();


                                if (carsCity.equals("Leeds")) {
                                    LeedsArray.add(Global.carsNew);
                                    Global.setLeedsArray(LeedsArray);
                                }
                                if (carsCity.equals("BradFord")) {
                                    BradfordArray.add(Global.carsNew);
                                    Global.setBradfordArray(BradfordArray);
                                }
                                if (carsCity.equals("Kirklees")) {
                                    KirkleesArray.add(Global.carsNew);
                                    Global.setKirkleesArray(KirkleesArray);
                                }
                                if (carsCity.equals("Wakefield")) {
                                    WakefieldArray.add(Global.carsNew);
                                    Global.setWakefieldArray(WakefieldArray);

                                }

                                setDefault();

                            }
                        }

                        else{
                                setDefault();
                            }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, function1 + "__" + "1");
            asyncTask1.execute();

        }


        mToolBar = (Toolbar) findViewById(R.id.tool);

        setSupportActionBar(mToolBar);
        mToolBar.setTitle("Hire-It");

        /*topToolBar.setSubtitle("");
        topToolBar.setLogoDescription("");*/

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setHomeButtonEnabled(true);
        //setSupportActionBar(mToolBar);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(MainActivity.this, mDrawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        mDrawer.addDrawerListener(mToggle);
        mToggle.syncState();
        mToolBar.setTitle("Hire-It");

        nvDrawer = (NavigationView) findViewById(R.id.nav_view);
        View navHeaderView;
        navHeaderView = nvDrawer.getHeaderView(0);
         wallet = (TextView) navHeaderView.findViewById(R.id.walletAmount);
        refresh = (ImageView)  navHeaderView.findViewById(R.id.refresh);


         navUsername = (TextView) navHeaderView.findViewById(R.id.tvUserName_HeaderView);
        TextView walletText= (TextView)navHeaderView.findViewById(R.id.wallet);
        walletText.setText("Wallet");

        SharedPreferences sharedpref = getSharedPreferences("_LoginInfo_", Context.MODE_PRIVATE);
         firstName = sharedpref.getString("FirstName", "0");
         lastName = sharedpref.getString("LastName", "0");
         number = sharedpref.getString("Number", "0");
         gender = sharedpref.getString("Gender", "0");
          dob1 = sharedpref.getString("DOB", "0");
         walletAmount = sharedpref.getString("WalletAmount", "0");
         refferalCode = sharedpref.getString("RefferalCode", "0");

        date= dob1.split(" ");
        dob=date[0];

        navUsername.setText(firstName + " " + lastName);
        wallet.setText("£"+walletAmount);
        Global.setFirst_Name(firstName);
        Global.setLast_Name(lastName);
        Global.setNumber(number);
        Global.setGender(gender);
        Global.setDob(dob);
        Global.setWalletAmount(walletAmount);
        Global.setRefferalCode(refferalCode);
        setupDrawerContent(nvDrawer);


        String function = "SaveDeviceToken?email=" + number + "@hire-it.co&devicetoken=" + Global.getDeviceToken() + "&os=android";
        GetSingle asyncTask = new GetSingle(MainActivity.this, new AsyncResponse() {

            @Override
            public void processFinish(Object result) {

                String ans = result + "";
                JSONObject jsonObj = null;

                try {
                    jsonObj = new JSONObject(ans);

                    Global.saveToken = new HireitLoginResponse(jsonObj);
                    result = Global.saveToken.getResult().toString();

                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }
        }, function + "__" + "1");
        asyncTask.execute();


        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Animation startRotateAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.revolove);
                refresh.startAnimation(startRotateAnimation);

                        String function = "UpdateProfile?email=" + Global.getNumber() + "@hire-it.co&firstname=" + Global.getFirst_Name() + "&lastname=" + Global.getLast_Name() + "&gender=" + Global.getGender() + "&DOB=" + dob + "&phoneno=" + Global.getNumber();
                        GetSingle asyncTask = new GetSingle(MainActivity.this, new AsyncResponse() {
                            @Override
                            public void processFinish(Object result) {

                                String ans = result + "";
                                JSONObject jsonObj = null;

                                try {
                                    jsonObj = new JSONObject(ans);
                                    Global.updateProfile = new HireitUserProfileResponse(jsonObj);
                                    String result1 = Global.updateProfile.getCustomerProfile().getResultMessage().toString();
                                    if (result1.equals("Successfully Updated")) {

                                        String walletAmount= Global.updateProfile.getCustomerProfile().getWalletAmount();
                                        String firstName= Global.updateProfile.getCustomerProfile().getFirstName();
                                        String lastName= Global.updateProfile.getCustomerProfile().getLastName();



                                        wallet.setText("£"+walletAmount);
                                        navUsername.setText(firstName+" "+lastName);


                                        Global.setWalletAmount(Global.updateProfile.getCustomerProfile().getWalletAmount());
                                        Global.setFirst_Name(Global.updateProfile.getCustomerProfile().getFirstName());
                                        Global.setLast_Name(Global.updateProfile.getCustomerProfile().getLastName());

                                        SharedPreferences sharedpref = getSharedPreferences("_LoginInfo_", Context.MODE_PRIVATE);
                                        SharedPreferences.Editor editor = sharedpref.edit();
                                        editor.putString("FirstName", Global.updateProfile.getCustomerProfile().getFirstName());
                                        editor.putString("LastName", Global.updateProfile.getCustomerProfile().getLastName());
                                        editor.putString("WalletAmount", Global.updateProfile.getCustomerProfile().getWalletAmount());

                                        editor.apply();



                                        /*Intent i= new Intent(MainActivity.this,MainActivity.class);
                                        startActivity(i);
*/
                                    } else {

                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, function + "__" + "1");
                        asyncTask.execute();










            }
        });



        states = new int[][]{
                new int[]{-android.R.attr.state_enabled}, // disabled
                new int[]{android.R.attr.state_enabled}, // enabled
                new int[]{-android.R.attr.state_checked}, // unchecked
                new int[]{android.R.attr.state_pressed}  // pressed

        };

        colors = new int[]{
                Color.YELLOW,
                Color.BLACK,
                Color.YELLOW,
                Color.YELLOW,
        };

        colorStateList2 = new ColorStateList(states, colors);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        getMenuInflater().inflate(R.menu.menu_main1, menu);
        MenuItem call = menu.findItem(R.id.call);

        call.setIcon(R.drawable.phone);
        call.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {



                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:"+"08000418722"));
                    startActivity(callIntent);



            return true;
            }
        });

        return super.onCreateOptionsMenu(menu);



    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void setupDrawerContent(final NavigationView navigationView) {
        navigationView.setItemIconTintList(colorStateList2);
/*
        navigationView.getMenu().getItem(1).setTitle("     Profile"+"                 £"+Global.getWalletAmount());
*/
       for (int b=0;b<navigationView.getMenu().size();b++){
        //   setTextColorForMenuItem(navigationView.getMenu().getItem(b),R.color.black);
        navigationView.setItemIconTintList(colorStateList2);
       }

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        selectDrawerItem(menuItem);
                        navigationView.setItemTextColor(colorStateList2);

                        return true;
                    }
                });
    }


    public void setDefault(){
        FragmentTransaction fm = getSupportFragmentManager().beginTransaction();
        fm.replace(R.id.container, new MainCarList());
        fm.commit();

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void selectDrawerItem(MenuItem menuItem) {


        Fragment fragment = null;
        Class fragmentClass = MainCarList.class;
        boolean flag = false;
        boolean flag1= false;
        switch (menuItem.getItemId()) {
            case R.id.action_home:
                fragmentClass = MainCarList.class;
                setTextColorForMenuItem(menuItem,R.color.yellowBackground);


                break;
            case R.id.action_myProfile:
                fragmentClass = ProfileFragment.class;
                setTextColorForMenuItem(menuItem, R.color.yellowBackground);
                break;
            case R.id.action_documents:
                fragmentClass = Documents.class;
                setTextColorForMenuItem(menuItem, R.color.yellowBackground);
                break;
            case R.id.action_logout:
                flag = true;
                setTextColorForMenuItem(menuItem, R.color.yellowBackground);
                break;
            case R.id.wallet:
                fragmentClass = WalletFragment.class;
                setTextColorForMenuItem(menuItem, R.color.yellowBackground);
                break;
            case R.id.action_Claims:
                fragmentClass = Claims.class;
                setTextColorForMenuItem(menuItem, R.color.yellowBackground);
                break;
            case R.id.action_share:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "Hire-It app\nAndroid:https://play.google.com/store/apps/details?id=hireit.hireit\niOS: https://itunes.apple.com/us/app/hire-it/id1423013131?ls=1&mt=8");
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, "Share via"));
                setTextColorForMenuItem(menuItem, R.color.yellowBackground);
                break;
            case R.id.action_BookedCars:
                fragmentClass = BookedCars.class;
                setTextColorForMenuItem(menuItem, R.color.yellowBackground);
                break;
            case R.id.action_aboutUs:
                setTextColorForMenuItem(menuItem, R.color.yellowBackground);
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("" +"http://www.hire-it.co/#standards"));
                startActivity(browserIntent);

                break;
            default:
                Toast.makeText(getApplicationContext(),"in Deffault",Toast.LENGTH_LONG).show();
                fragmentClass= MainCarList.class;
                break;

        }

        if (flag) {
            new SweetAlertDialog(this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                    .setCustomImage(R.drawable.hireit_logo)
                    .setTitleText("Are you sure?")
                    .setContentText("You want to logout?")
                    .setConfirmText("Yes")
                    .setCancelText("No")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();

                            Global.setKEY_LOGIN(false);
                            Global.setNumber("");
                            Global.setKeyConfirmationCode("");
                            Global.setKeyCodeResendToken(null);
                            Global.setFirst_Name("");
                            Global.setLast_Name("");
                            Global.setPassword("");
                            Global.setGender("");
                            Global.setDob("");
                            Global.setRefferalCode("");
                            Global.setWalletAmount("");
                            Global.setRefferalCode("");
                            SharedPreferences sharedpref = getSharedPreferences("_LoginInfo_", Context.MODE_PRIVATE);
                            sharedpref.edit().clear().commit();

                            Intent intent = new Intent(MainActivity.this, SignUpActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    })
                    .show();


        }

        else {

            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, fragment).addToBackStack("tag").commit();
            menuItem.setChecked(true);
            setTitle(menuItem.getTitle());
            mDrawer.closeDrawers();
            setTextColorForMenuItem(menuItem,R.color.black);
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mToggle.onOptionsItemSelected(item)) {
            return true;

        }
        return super.onOptionsItemSelected(item);
    }
    private void setTextColorForMenuItem(MenuItem menuItem, @ColorRes int color) {
        SpannableString spanString = new SpannableString(menuItem.getTitle().toString());
        spanString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, color)), 0, spanString.length(), 0);
        menuItem.setTitle(spanString);
    }

    private void setTextForMenuItem(MenuItem menuItem, String title) {
        SpannableString spanString = new SpannableString(menuItem.getTitle().toString());
        menuItem.setTitle(spanString);
    }
    private void resetAllMenuItemsTextColor(NavigationView navigationView) {
        for (int i = 0; i < navigationView.getMenu().size(); i++)
            setTextColorForMenuItem(navigationView.getMenu().getItem(i), R.color.black);
    }
}