package hireit.hireit.drawer.DrawerFragments;

import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import hireit.hireit.AsyncResponse;
import hireit.hireit.GetProfile;
import hireit.hireit.Global;
import hireit.hireit.Models.BookedCarsList;
import hireit.hireit.Models.HireitCarsNew;
import hireit.hireit.Models.HireitLoginResponse;
import hireit.hireit.R;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class Claims extends Fragment {

    private static final String KEY_MOVIE_TITLE = "key_title";
    private String picLink;

    private Uri picUrl;

    private String carName;
    private String carReg;
    private String remaining;
    private String carId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA, android.Manifest.permission.READ_PHONE_STATE, android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_claims, container, false);
    }

    ArrayList<ListData> main;
    JSONArray jarr;
    SampleAdapter sa;
    Button raise;
    Button viewClaim;
    View backScreen;
    LinearLayout open;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        main = new ArrayList<>();
        sa = new SampleAdapter();

        final ListView lv = view.findViewById(R.id.listClaims);
         open =(LinearLayout) view.findViewById(R.id.open);
        lv.setAdapter(sa);
        raise= (Button) view.findViewById(R.id.raise);
        viewClaim= (Button) view.findViewById(R.id.view);
        backScreen= (View) view.findViewById(R.id.backScreen);



        viewClaim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Global.setRaiseClaim(false);

                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.container, new claimHistory());
                ft.addToBackStack(null);
                ft.commit();

            }
        });

        backScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                open.setVisibility(View.GONE);
                backScreen.setVisibility(View.GONE);
            }
        });

        raise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String function="NewClaims?email="+Global.getNumber()+"@hire-it.co&carid="+Global.getItemCarId();
                GetProfile asyncTask= new GetProfile(getActivity(), new AsyncResponse() {
                    @Override
                    public void processFinish(Object result) {

                        String ans = result + "";
                        JSONObject jsonObj = null;

                        try {
                            jsonObj = new JSONObject(ans);


                            Global.claims = new HireitLoginResponse(jsonObj);
                            String result1 = Global.claims.getResult();

                            open.setVisibility(View.GONE);
                            backScreen.setVisibility(View.GONE);

                                if (result1.equals("User not Registerd")||result1.equals("Failed to raise claim")) {
                                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText(result1.toString())
                                            .show();

                                }
                                else {
                                    new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                                            .setTitleText("Claim Raised")
                                            .setContentText("claim no. is"+result1.toString())
                                            .show();
                                    Global.setClaimNo(result1.toString());
                                    Global.setRaiseClaim(true);
                                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                                    ft.replace(R.id.container, new claimHistory());
                                    ft.addToBackStack(null);
                                    ft.commit();

                                }
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }


                    }
                },function+"__"+"1");
                asyncTask.execute();



            }
        });




        String function="CarsList?email="+Global.getNumber()+"@hire-it.co";
        GetProfile asyncTask= new GetProfile(getActivity(), new AsyncResponse() {
            @Override
            public void processFinish(Object result) {

                String ans = result + "";

              try {
                  JsonParser parser = new JsonParser();
                  JsonElement mJson =  parser.parse(ans);
                  Gson gson = new Gson();
                  BookedCarsList object = gson.fromJson(mJson, BookedCarsList.class);

                  Global.bookedCarsList = object;
                  String result1= Global.bookedCarsList.getCarsListResult().getResult();
                  if (Global.bookedCarsList.getCarsListResult().getCarClaims().size()>0) {
                      if (result1.equals("Success")) {
                          for (int i = 0; i < Global.bookedCarsList.getCarsListResult().getCarClaims().size(); i++) {
                              carName = Global.bookedCarsList.getCarsListResult().getCarClaims().get(i).getCarName();
                              carReg = Global.bookedCarsList.getCarsListResult().getCarClaims().get(i).getRegistrationNumber();
                              carId=Global.bookedCarsList.getCarsListResult().getCarClaims().get(i).getCarid();
                              sa.addEntry(new ListData(carName,carReg,carId));

                          }

                      }
                  }




              }
              catch (Exception e){
                  e.printStackTrace();
              }


            }
        },function+"__"+"1");
        asyncTask.execute();


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String ItemcarId= String.valueOf(sa.getItem(i).carId);
                Global.setItemCarId(ItemcarId);

                backScreen.setVisibility(View.VISIBLE);
                open.setVisibility(View.VISIBLE);


            }
        });



    }

    private static class ViewHolder {

        private View view;

        private TextView carName;
        private TextView carReg;

        private ViewHolder(View view) {
            this.view = view;
            carName= view.findViewById(R.id.carName);
            carReg = view.findViewById(R.id.carReg);



        }

    }



    private class SampleAdapter extends BaseAdapter {
        SampleAdapter() {
            main = new ArrayList<>();
            // LeedsArray= new ArrayList<>();
        }


        public void addEntry(ListData l) {
            main.add(l);
            notifyDataSetChanged();
        }
        public void addEntryLeeds(HireitCarsNew l) {
            //  LeedsArray.add(l);
            notifyDataSetChanged();
        }

        public void clear() {
            main.clear();
            notifyDataSetChanged();
        }



        @Override
        public int getCount() {
            return main.size();
        }

        @Override
        public ListData getItem(int position) {
            return main.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = View.inflate(getActivity(), R.layout.lisitem_claims, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            ListData item = getItem(position);
            DisplayMetrics dm = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);

            int width = dm.widthPixels;
            int height = dm.heightPixels;

            holder.carName.setText(item.carName);
            holder.carReg.setText(item.carReg);




         /*    Bitmap bt= getBitmapFromURL(picLink);
            holder.carImg.setImageBitmap(getBitmapFromURL(bt.toString()));
*/


            return convertView;

        }
    }



    private class ListData {
        private String carName;
        private String carReg;
        private String carId;
        //["Sehat Sahara Plus", "2", "2", "10", "16", "7\/19\/2017 12:00:00 AM", "7\/19\/2018 12:00:00 AM"]
        public ListData(String carName,String carReg,String carId) {
            this.carName = carName;
            this.carReg = carReg;
            this.carId = carId;


        }
    }



}

