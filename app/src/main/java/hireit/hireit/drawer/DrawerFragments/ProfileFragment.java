package hireit.hireit.drawer.DrawerFragments;


import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;
import hireit.hireit.AsyncResponse;
import hireit.hireit.GetProfile;
import hireit.hireit.Global;
import hireit.hireit.Models.HireitUserProfileResponse;
import hireit.hireit.R;


/**
 * A fragment that launches other parts of the demo application.
 */
public class ProfileFragment extends Fragment {

    private static final String KEY_MOVIE_TITLE = "key_title";
    public ProfileFragment(){

    }

    public static ProfileFragment newInstance(String movieTitle) {
        ProfileFragment prof = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(KEY_MOVIE_TITLE, movieTitle);
        try {
            prof.setArguments(args);
        } catch (IllegalStateException e) {
        }
        return prof;
    }

    EditText firstName;
    EditText lastName;
    Spinner gender;
    EditText dob;
    EditText reff;
    int mYear;
    int mMonth;
    int mDay;
    int minYear;

    Button edit;
    Button save;

    String firstNametext;
    String lastNametext;
    String gendertext;
    String walletAmount;
    String refferalCode;
    String numbertext;
    String dobtext;

LinearLayout navigation;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile, container,
                false);

        return v;
    }
    DatePickerDialog mDatePicker;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        firstName= (EditText) view.findViewById(R.id.firstName);
        lastName= (EditText) view.findViewById(R.id.lastName);
        gender= (Spinner) view.findViewById(R.id.gender);
        dob= (EditText) view.findViewById(R.id.dob);
        reff= (EditText) view.findViewById(R.id.refferalCode);

        navigation= (LinearLayout) view.findViewById(R.id.navigation);
        ArrayList<String> list;

        list = new ArrayList<String>();
        list.add("Male");
        list.add("Female");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        SharedPreferences sharedprefe = getActivity().getSharedPreferences("_LoginInfo_", Context.MODE_PRIVATE);
        firstNametext = sharedprefe.getString("FirstName", "0");
        lastNametext = sharedprefe.getString("LastName", "0");
        numbertext = sharedprefe.getString("Number", "0");
        gendertext = sharedprefe.getString("Gender", "0");
        dobtext = sharedprefe.getString("DOB", "0");
        walletAmount = sharedprefe.getString("WalletAmount", "0");
        refferalCode = sharedprefe.getString("RefferalCode", "0");


        gender.setAdapter(adapter);
        firstName.setText(firstNametext);
        lastName.setText(lastNametext);

        reff.setText(refferalCode);
        gender.setEnabled(false);
        gender.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        if (Global.getGender().equals("Male")){
            gender.setSelection(0);
        }
        else
            gender.setSelection(1);
        String[] date= dobtext.split(" ");
        dob.setText(date[0]);

        edit= (Button) view.findViewById(R.id.edit);
        save= (Button) view.findViewById(R.id.save);
        edit.setVisibility(View.VISIBLE);
        navigation.setVisibility(View.GONE);

        if (Global.isSigningUp()){
            edit.setVisibility(View.GONE);
            save.setVisibility(View.GONE);
            navigation.setVisibility(View.VISIBLE);



        }


        navigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Global.setSigningUp(false);

                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.container, new MainCarList());
                ft.addToBackStack(null);
                ft.commit();

            }
        });



        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edit.setVisibility(View.GONE);
                save.setVisibility(View.VISIBLE);
                firstName.setEnabled(true);
                firstName.setFocusable(true);
                firstName.setClickable(true);
                firstName.setFocusableInTouchMode(true);
                firstName.setPressed(true);
                firstName.setBackgroundColor(getResources().getColor(R.color.white));

                lastName.setEnabled(true);
                lastName.setFocusable(true);
                lastName.setClickable(true);
                lastName.setFocusableInTouchMode(true);
                lastName.setPressed(true);
                lastName.setBackgroundColor(getResources().getColor(R.color.white));


                gender.setEnabled(true);
                gender.setClickable(true);
                gender.setBackgroundColor(getResources().getColor(R.color.white));

                dob.setEnabled(true);
                dob.setFocusable(true);
                dob.setClickable(true);
                dob.setFocusableInTouchMode(true);
                dob.setPressed(true);
                dob.setBackgroundColor(getResources().getColor(R.color.white));

                dob.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        // To show current date in the datepicker
                        Calendar mcurrentDate = Calendar.getInstance();
                        mYear = mcurrentDate.get(Calendar.YEAR);
                        mMonth = mcurrentDate.get(Calendar.MONTH);
                        mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);





                             mDatePicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

                                 public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                                    Calendar myCalendar = Calendar.getInstance();

                                     //datepicker.setMaxDate(new Date().getTime()-18);

                                     myCalendar.set(Calendar.YEAR, selectedyear);
                                    myCalendar.set(Calendar.MONTH, selectedmonth);
                                    myCalendar.set(Calendar.DAY_OF_MONTH, selectedday);
                                     String myFormat = "dd-MMM-yyyy"; //Change as you need
                                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.ENGLISH);

                                  /* if (>mYear-18){

                                       new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                               .setTitleText("Something Went Wrong")
                                               .setContentText("User must be 18 years old")
                                               .show();
                                   }*/
                                    dob.setText(sdf.format(myCalendar.getTime()));

                                    mDay = selectedday;
                                    mMonth = selectedmonth;
                                    mYear = selectedyear;



                                 }


                            }, mYear - 18, mMonth, mDay);
                            //mDatePicker.setTitle("Select date");

                            mDatePicker.show();





                    }
                });



            }
        });


        save.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

        String function="UpdateProfile?email="+Global.getNumber()+"@hire-it.co&firstname="+firstName.getText()+"&lastname="+lastName.getText()+"&gender="+gender.getSelectedItem().toString()+"&DOB="+dob.getText()+"&phoneno="+Global.getNumber();
        GetProfile asyncTask= new GetProfile(getActivity(), new AsyncResponse() {
            @Override
            public void processFinish(Object result) {

                String ans = result + "";
                JSONObject jsonObj = null;

                try {
                    jsonObj = new JSONObject(ans);
                    Global.updateProfile = new HireitUserProfileResponse(jsonObj);
                    String result1= Global.updateProfile.getCustomerProfile().getResultMessage().toString();
                    if (result1.equals("Successfully Updated")) {
                        Toast.makeText(getActivity(),result1,Toast.LENGTH_LONG).show();
                        firstName.setEnabled(false);
                        firstName.setFocusable(false);
                        firstName.setClickable(false);
                        firstName.setFocusableInTouchMode(false);
                        firstName.setPressed(false);
                        firstName.setBackgroundColor(getResources().getColor(R.color.lightBackgrond));

                        lastName.setEnabled(false);
                        lastName.setFocusable(false);
                        lastName.setClickable(false);
                        lastName.setFocusableInTouchMode(false);
                        lastName.setPressed(false);
                        lastName.setBackgroundColor(getResources().getColor(R.color.lightBackgrond));

                        gender.setEnabled(false);
                        gender.setClickable(false);
                        gender.setPressed(false);
                        gender.setBackgroundColor(getResources().getColor(R.color.lightBackgrond));

                        dob.setEnabled(false);
                        dob.setFocusable(false);
                        dob.setClickable(false);
                        dob.setFocusableInTouchMode(false);
                        dob.setPressed(false);
                        dob.setBackgroundColor(getResources().getColor(R.color.lightBackgrond));



                        save.setVisibility(View.GONE);
                        edit.setVisibility(View.VISIBLE);
                        Global.setWalletAmount(Global.updateProfile.getCustomerProfile().getWalletAmount());
                        Global.setFirst_Name(Global.updateProfile.getCustomerProfile().getFirstName());
                        Global.setLast_Name(Global.updateProfile.getCustomerProfile().getLastName());
                        Global.setGender(Global.updateProfile.getCustomerProfile().getGender());
                        Global.setDob(Global.updateProfile.getCustomerProfile().getDateOfBirth());
                        Global.setRefferalCode(Global.updateProfile.getCustomerProfile().getRefferalCode());
                        SharedPreferences sharedpref = getActivity().getSharedPreferences("_LoginInfo_", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedpref.edit();
                        editor.putString("FirstName",Global.updateProfile.getCustomerProfile().getFirstName());
                        editor.putString("LastName",Global.updateProfile.getCustomerProfile().getLastName());
                        editor.putString("Gender", Global.updateProfile.getCustomerProfile().getGender());
                        editor.putString("DOB", Global.updateProfile.getCustomerProfile().getDateOfBirth());
                        editor.putString("RefferalCode", Global.updateProfile.getCustomerProfile().getRefferalCode());
                        editor.putString("WalletAmount", Global.updateProfile.getCustomerProfile().getWalletAmount());

                        editor.apply();


                    }
                    else {
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("Profile not updated")
                                .setContentText("Please try again")
                                .show();
                    }

                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        },function+"__"+"1");
        asyncTask.execute();


    }
});
    }



}