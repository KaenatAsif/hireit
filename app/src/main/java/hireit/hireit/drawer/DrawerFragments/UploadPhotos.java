package hireit.hireit.drawer.DrawerFragments;


import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import hireit.hireit.AsyncResponse;
import hireit.hireit.GetProfile;
import hireit.hireit.Global;
import hireit.hireit.Models.HireitLoginResponse;
import hireit.hireit.R;

import static android.app.Activity.RESULT_OK;


/**
 * A fragment that launches other parts of the demo application.
 */
public class UploadPhotos extends Fragment {

    private static final String KEY_MOVIE_TITLE = "key_title";

    public UploadPhotos() {

    }

    public static UploadPhotos newInstance(String movieTitle) {
        UploadPhotos doc = new UploadPhotos();
        Bundle args = new Bundle();
        args.putString(KEY_MOVIE_TITLE, movieTitle);
        try {
            doc.setArguments(args);
        } catch (IllegalStateException e) {
        }
        return doc;
    }

    private final int IMG_REQUEST_dvlaFront = 1;
    private final int IMG_REQUEST_dvlaBack = 2;
    private final int IMG_REQUEST_badgeFront = 3;
    private final int IMG_REQUEST_badgeBack = 4;
    private final int IMG_REQUEST_bill = 5;


    private static final String uploadURL = "http://www.hire-it.co/Documents/upload.php";
    String DIR = "";
    Bitmap bitmapDvlaFront = null;
    Bitmap bitmapDvlaBack = null;
    Bitmap bitmapBadgeFront = null;
    Bitmap bitmapBadgeBack = null;
    Bitmap bitmapBill = null;
    int width, height;
    private RequestQueue requestQueue;
    private StringRequest stringRequest;
    int COUNT = 0;
    int PICTURE_COUNT = 0;
    Global preferences;
    Boolean flag = true;
    Button dvlaFront;
    ImageView dvlaFrontpic;
    Button dvlaBack;
    ImageView dvlaBackPic;
    Button badgeFront;
    ImageView badgeFrontPic;
    Button badgeBack;
    ImageView badgeBackPic;
    Button submitDoc;
    CheckBox checkAccept;
    TextView contract;


    Button gallery;
    Button camera;
    LinearLayout open;
    int count = 0;

    boolean isDvlaFront = false;
    boolean isDvlaBack = false;
    boolean isBadgeFront = false;
    boolean isBadgeBack = false;
    boolean isBill = false;

    boolean isGallery = false;
    boolean isCamera = false;
    LinearLayout sc;
    Button bill;
    ImageView billPic;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    View viewBack;

    String imageFilePath;
    Uri imageUri;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.activity_upload_photos, container, false);

        preferences = new Global();


        Global.setUploadPhotos(false);
        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA, android.Manifest.permission.READ_PHONE_STATE, android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    1);
        }


        gallery = (Button) view.findViewById(R.id.gallery);
        camera = (Button) view.findViewById(R.id.camera);
        open = (LinearLayout) view.findViewById(R.id.open);
        viewBack = (View) view.findViewById(R.id.viewBack);

        sc = (LinearLayout) view.findViewById(R.id.sc);

        dvlaFrontpic = (ImageView) view.findViewById(R.id.dvlaFrontPic);
        dvlaBackPic = (ImageView) view.findViewById(R.id.dvlaBackPic);
        badgeFrontPic = (ImageView) view.findViewById(R.id.badgeFrontPic);
        badgeBackPic = (ImageView) view.findViewById(R.id.badgeBackPic);
        billPic = (ImageView) view.findViewById(R.id.billPic);

        dvlaFront = (Button) view.findViewById(R.id.dvlaFront);
        dvlaBack = (Button) view.findViewById(R.id.dvlaBack);
        badgeFront = (Button) view.findViewById(R.id.badgeFront);
        bill = (Button) view.findViewById(R.id.bill);
        badgeBack = (Button) view.findViewById(R.id.badgeBack);

        submitDoc = (Button) view.findViewById(R.id.submitDoc);
        checkAccept = (CheckBox) view.findViewById(R.id.checkAccept);

        contract = (TextView) view.findViewById(R.id.contract);
        contract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://hire-it.co/wp-content/uploads/contract/contract-hire-it.pdf"));
                startActivity(browserIntent);

            }
        });

        viewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewBack.setVisibility(View.GONE);
                open.setVisibility(View.GONE);
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                open.setVisibility(View.GONE);
                viewBack.setVisibility(View.GONE);
                sc.setVisibility(View.VISIBLE);
                if (isDvlaFront) {
                    isGallery = true;
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    photoPickerIntent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(photoPickerIntent, IMG_REQUEST_dvlaFront);

                } else if (isDvlaBack) {
                    isGallery = true;
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    photoPickerIntent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(photoPickerIntent, IMG_REQUEST_dvlaBack);

                } else if (isBadgeFront) {
                    isGallery = true;
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    photoPickerIntent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(photoPickerIntent, IMG_REQUEST_badgeFront);

                } else if (isBadgeBack) {
                    isGallery = true;
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    photoPickerIntent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(photoPickerIntent, IMG_REQUEST_badgeBack);

                } else if (isBill) {
                    isGallery = true;
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    photoPickerIntent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(photoPickerIntent, IMG_REQUEST_bill);

                }

            }
        });

        camera.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View view) {
                isGallery = false;
                if (isDvlaFront) {
                    Toast.makeText(getActivity(), "Camera", Toast.LENGTH_SHORT).show();
                    open.setVisibility(View.GONE);
                    viewBack.setVisibility(View.GONE);
                    sc.setVisibility(View.VISIBLE);

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                        // fileTemp = ImageUtils.getOutputMediaFile();
                        ContentValues values = new ContentValues(1);
                        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
                        imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

                        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

                        startActivityForResult(intent, IMG_REQUEST_dvlaFront);
                    }
                } else if (isDvlaBack) {
                    Toast.makeText(getActivity(), "Camera", Toast.LENGTH_SHORT).show();
                    open.setVisibility(View.GONE);
                    viewBack.setVisibility(View.GONE);
                    sc.setVisibility(View.VISIBLE);
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                        // fileTemp = ImageUtils.getOutputMediaFile();
                        ContentValues values = new ContentValues(1);
                        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
                        imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

                        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

                        startActivityForResult(intent, IMG_REQUEST_dvlaBack);
                    }

                } else if (isBadgeFront) {
                    Toast.makeText(getActivity(), "Camera", Toast.LENGTH_SHORT).show();
                    open.setVisibility(View.GONE);
                    viewBack.setVisibility(View.GONE);
                    sc.setVisibility(View.VISIBLE);
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                        // fileTemp = ImageUtils.getOutputMediaFile();
                        ContentValues values = new ContentValues(1);
                        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
                        imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

                        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

                        startActivityForResult(intent, IMG_REQUEST_badgeFront);
                    }

                } else if (isBadgeBack) {
                    Toast.makeText(getActivity(), "Camera", Toast.LENGTH_SHORT).show();
                    open.setVisibility(View.GONE);
                    viewBack.setVisibility(View.GONE);
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                        // fileTemp = ImageUtils.getOutputMediaFile();
                        ContentValues values = new ContentValues(1);
                        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
                        imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

                        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

                        startActivityForResult(intent, IMG_REQUEST_badgeBack);
                    }

                } else if (isBill) {
                    Toast.makeText(getActivity(), "Camera", Toast.LENGTH_SHORT).show();
                    open.setVisibility(View.GONE);
                    viewBack.setVisibility(View.GONE);
                    sc.setVisibility(View.VISIBLE);
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                        // fileTemp = ImageUtils.getOutputMediaFile();
                        ContentValues values = new ContentValues(1);
                        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
                        imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

                        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

                        startActivityForResult(intent, IMG_REQUEST_bill);
                    }

                }


            }

        });


        dvlaFront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  sc.setVisibility(View.GONE);

                viewBack.setVisibility(View.VISIBLE);
                open.setVisibility(View.VISIBLE);

                isDvlaFront = true;
                isDvlaBack = false;
                isBadgeFront = false;
                isBill = false;
                isBadgeBack = false;

            }
        });

        dvlaBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewBack.setVisibility(View.VISIBLE);
                open.setVisibility(View.VISIBLE);

                isDvlaFront = false;
                isDvlaBack = true;
                isBadgeFront = false;
                isBill = false;
                isBadgeBack = false;
            }
        });

        badgeFront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewBack.setVisibility(View.VISIBLE);
                open.setVisibility(View.VISIBLE);

                isDvlaFront = false;
                isDvlaBack = false;
                isBadgeFront = true;
                isBill = false;
                isBadgeBack = false;


            }
        });
        badgeBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewBack.setVisibility(View.VISIBLE);
                open.setVisibility(View.VISIBLE);

                isDvlaFront = false;
                isDvlaBack = false;
                isBadgeFront = false;
                isBill = false;
                isBadgeBack = true;


            }
        });

        bill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                viewBack.setVisibility(View.VISIBLE);
                open.setVisibility(View.VISIBLE);

                isDvlaFront = false;
                isDvlaBack = false;
                isBadgeFront = false;
                isBadgeBack = false;
                isBill = true;

            }
        });

        submitDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (count > 3) {

                    if (checkAccept.isChecked()) {
                        String function = "SaveDocuments?email=" + Global.getNumber() + "@hire-it.co";

                        GetProfile asyncTask = new GetProfile(getActivity(), new AsyncResponse() {

                            @Override
                            public void processFinish(Object result) throws JSONException {

                                try {
                                    String ans = result + "";
                                    JSONObject jsonObj = null;

                                    jsonObj = new JSONObject(ans);

                                    Global.submitDoc = new HireitLoginResponse(jsonObj);
                                    String result1 = Global.submitDoc.getResult().toString();

                                    if (result1.equals("Success")) {
                                        new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                                                .setTitleText("Done")
                                                .setContentText("Documents saved successfully")
                                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                        if (Global.isBooking()) {

                                                            FragmentTransaction ft = getFragmentManager().beginTransaction();
                                                            ft.replace(R.id.container, new MainCarList());
                                                            ft.addToBackStack(null);
                                                            ft.commit();
                                                        } else {
                                                            FragmentTransaction ft = getFragmentManager().beginTransaction();
                                                            ft.replace(R.id.container, new MainCarList());
                                                            ft.addToBackStack(null);
                                                            ft.commit();
                                                        }
                                                        sweetAlertDialog.dismissWithAnimation();
                                                    }
                                                })
                                                .show();


                                    } else {

                                        Toast.makeText(getActivity(), result.toString(), Toast.LENGTH_LONG).show();

                                    }
                                }
                                catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, function + "__" + "1");
                        asyncTask.execute();


                    } else {
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Something Went Wrong")
                                .setContentText("Please check the contract")
                                .show();
                    }
                } else {
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Something Went Wrong")
                            .setContentText("Please complete your document")
                            .show();
                }
            }
        });

        return view;
    }

    private String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private void uploadImageDvlaFront() {

        stringRequest = new StringRequest(Request.Method.POST, uploadURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(getActivity(), "Successfully Uploaded", Toast.LENGTH_LONG).show();
                count++;

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                count++;
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("email", Global.getNumber() + "@hire-it.co");
                hashMap.put("picname", "DVLAFront");
                String uploadImage = getStringImage(bitmapDvlaFront);
                hashMap.put("image", uploadImage);
             /*   bitmapDvlaFront.recycle();
                bitmapDvlaFront = null;*/
                System.gc();
                return hashMap;
            }
        };
        //Toast.makeText(getActivity(), "Picture Link = "+pictureLink, Toast.LENGTH_SHORT).show();
        requestQueue = new Volley().newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    private void uploadImageDvlaBack() {
        stringRequest = new StringRequest(Request.Method.POST, uploadURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(getActivity(), "Successfully Uploaded", Toast.LENGTH_LONG).show();
                count++;

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                count++;
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("email", Global.getNumber() + "@hire-it.co");
                hashMap.put("picname", "DVLABack");
                String uploadImage = getStringImage(bitmapDvlaBack);
                hashMap.put("image", uploadImage);
               /* bitmapDvlaBack.recycle();
                bitmapDvlaBack = null;*/
                System.gc();
                return hashMap;
            }
        };
        //Toast.makeText(getActivity(), "Picture Link = "+pictureLink, Toast.LENGTH_SHORT).show();
        requestQueue = new Volley().newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    private void uploadImageBadgeFront() {
        stringRequest = new StringRequest(Request.Method.POST, uploadURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(getActivity(), "Successfully Uploaded", Toast.LENGTH_LONG).show();
                count++;

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                count++;
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("email", Global.getNumber() + "@hire-it.co");
                hashMap.put("picname", "TaxiBadgeNumberFront");
                String uploadImage = getStringImage(bitmapBadgeFront);
                hashMap.put("image", uploadImage);
             /*  bitmapBadgeFront.recycle();
                bitmapBadgeFront = null;*/
                System.gc();
                return hashMap;
            }
        };
        //Toast.makeText(getActivity(), "Picture Link = "+pictureLink, Toast.LENGTH_SHORT).show();
        requestQueue = new Volley().newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    private void uploadBill() {
        stringRequest = new StringRequest(Request.Method.POST, uploadURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(getActivity(), "Successfully Uploaded", Toast.LENGTH_LONG).show();
                count++;

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                count++;
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("email", Global.getNumber() + "@hire-it.co");
                hashMap.put("picname", "UtilityBillOrTaxiLetter");
                String uploadImage = getStringImage(bitmapBill);
                hashMap.put("image", uploadImage);
             /*  bitmapBadgeFront.recycle();
                bitmapBadgeFront = null;*/
                System.gc();
                return hashMap;
            }
        };
        //Toast.makeText(getActivity(), "Picture Link = "+pictureLink, Toast.LENGTH_SHORT).show();
        requestQueue = new Volley().newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    private void uploadBadgeBack() {
        stringRequest = new StringRequest(Request.Method.POST, uploadURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(getActivity(), "Successfully Uploaded", Toast.LENGTH_LONG).show();
                count++;

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                count++;
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("email", Global.getNumber() + "@hire-it.co");
                hashMap.put("picname", "TaxiBadgeNumberBack");
                String uploadImage = getStringImage(bitmapBadgeBack);
                hashMap.put("image", uploadImage);
             /*   bitmapBadgeBack.recycle();
                bitmapBadgeBack = null;*/
                System.gc();
                return hashMap;
            }
        };
        //Toast.makeText(getActivity(), "Picture Link = "+pictureLink, Toast.LENGTH_SHORT).show();
        requestQueue = new Volley().newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

//

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMG_REQUEST_dvlaFront && resultCode == RESULT_OK) {

            if (isGallery){

                Uri path = data.getData();
                try {
                        bitmapDvlaFront = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), path);

                    dvlaFrontpic.setImageBitmap(bitmapDvlaFront);
                    uploadImageDvlaFront();

                    Global.dismissLoadingPopup();

                } catch (IOException e) {
                    e.printStackTrace();
                }
                flag = false;
            }
            else {
                Uri selectedImage = imageUri;//Uri.fromFile(new File(imageFilePath));

                getActivity().getContentResolver().notifyChange(selectedImage, null);
                ContentResolver cr = getActivity().getContentResolver();
                try {
                    Toast.makeText(getActivity(), selectedImage.toString(),
                            Toast.LENGTH_LONG).show();
                    bitmapDvlaFront = MediaStore.Images.Media.getBitmap(cr, selectedImage);

                    dvlaFrontpic.setImageBitmap(bitmapDvlaFront);
                    uploadImageDvlaFront();

                    Global.dismissLoadingPopup();
                    flag = false;


                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

        } else if (requestCode == IMG_REQUEST_dvlaBack && resultCode == RESULT_OK ) {

                if (isGallery) {

                    Uri path = data.getData();
                    try {
                        bitmapDvlaBack = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), path);

                        dvlaBackPic.setImageBitmap(bitmapDvlaBack);
                        uploadImageDvlaBack();

                        Global.dismissLoadingPopup();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    flag = false;
                } else {
                    Uri selectedImage = imageUri;//Uri.fromFile(new File(imageFilePath));

                    getActivity().getContentResolver().notifyChange(selectedImage, null);
                    ContentResolver cr = getActivity().getContentResolver();
                    try {
                        Toast.makeText(getActivity(), selectedImage.toString(),
                                Toast.LENGTH_LONG).show();
                        bitmapDvlaBack = MediaStore.Images.Media.getBitmap(cr, selectedImage);

                        dvlaBackPic.setImageBitmap(bitmapDvlaBack);
                        uploadImageDvlaBack();

                        Global.dismissLoadingPopup();
                        flag = false;


                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            } else if (requestCode == IMG_REQUEST_badgeFront && resultCode == RESULT_OK ) {
            if (isGallery) {

                Uri path = data.getData();
                try {
                    bitmapBadgeFront = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), path);

                    badgeFrontPic.setImageBitmap(bitmapBadgeFront);
                    uploadImageBadgeFront();

                    Global.dismissLoadingPopup();

                } catch (IOException e) {
                    e.printStackTrace();
                }
                flag = false;
            } else {
                Uri selectedImage = imageUri;//Uri.fromFile(new File(imageFilePath));

                getActivity().getContentResolver().notifyChange(selectedImage, null);
                ContentResolver cr = getActivity().getContentResolver();
                try {
                    Toast.makeText(getActivity(), selectedImage.toString(),
                            Toast.LENGTH_LONG).show();
                    bitmapBadgeFront = MediaStore.Images.Media.getBitmap(cr, selectedImage);

                    badgeFrontPic.setImageBitmap(bitmapBadgeFront);
                    uploadImageBadgeFront();

                    Global.dismissLoadingPopup();
                    flag = false;


                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        } else if (requestCode == IMG_REQUEST_badgeBack && resultCode == RESULT_OK ) {
            if (isGallery) {

                Uri path = data.getData();
                try {
                    bitmapBadgeBack = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), path);

                    badgeBackPic.setImageBitmap(bitmapBadgeBack);
                    uploadBadgeBack();

                    Global.dismissLoadingPopup();

                } catch (IOException e) {
                    e.printStackTrace();
                }
                flag = false;
            } else {
                Uri selectedImage = imageUri;//Uri.fromFile(new File(imageFilePath));

                getActivity().getContentResolver().notifyChange(selectedImage, null);
                ContentResolver cr = getActivity().getContentResolver();
                try {
                    Toast.makeText(getActivity(), selectedImage.toString(),
                            Toast.LENGTH_LONG).show();
                    bitmapBadgeBack = MediaStore.Images.Media.getBitmap(cr, selectedImage);

                    badgeBackPic.setImageBitmap(bitmapBadgeBack);
                    uploadBadgeBack();

                    Global.dismissLoadingPopup();
                    flag = false;


                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }  else if (requestCode == IMG_REQUEST_bill && resultCode == RESULT_OK ) {
            if (isGallery) {

                Uri path = data.getData();
                try {
                    bitmapBill = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), path);

                    billPic.setImageBitmap(bitmapBill);
                    uploadBill();

                    Global.dismissLoadingPopup();

                } catch (IOException e) {
                    e.printStackTrace();
                }
                flag = false;
            } else {
                Uri selectedImage = imageUri;//Uri.fromFile(new File(imageFilePath));

                getActivity().getContentResolver().notifyChange(selectedImage, null);
                ContentResolver cr = getActivity().getContentResolver();
                try {
                    Toast.makeText(getActivity(), selectedImage.toString(),
                            Toast.LENGTH_LONG).show();
                    bitmapBill = MediaStore.Images.Media.getBitmap(cr, selectedImage);

                    billPic.setImageBitmap(bitmapBill);
                    uploadBill();

                    Global.dismissLoadingPopup();
                    flag = false;


                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }  else {
            Toast.makeText(getActivity(), "Something went Wrong", Toast.LENGTH_SHORT).show();

        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        String imageFilePath = image.getAbsolutePath();
        return image;
    }

    public String getRealPathFromUri(Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = 0;
            if (cursor != null) {
                column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            } else
                return contentUri.toString();
        } finally {
            if (cursor != null) {
                cursor.close();
            }

        }
    }
}




