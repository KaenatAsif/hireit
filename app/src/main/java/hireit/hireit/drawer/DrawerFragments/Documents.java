package hireit.hireit.drawer.DrawerFragments;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import hireit.hireit.AsyncResponse;
import hireit.hireit.GetProfile;
import hireit.hireit.Global;
import hireit.hireit.Models.HireitGetDocumentsResponse;
import hireit.hireit.R;

import static android.app.Activity.RESULT_OK;


public class Documents extends Fragment {

    View _v_;

    ListView lv;
    JSONArray jarr;

    Boolean Network_b = false;
    private String dvlaFront;
    private String dvlaBack;
    private String badgeBack;
    private String badgeFront;
    private String bill;
    private String img;

    ImageView dvlaFrontPic;
    ImageView dvlaBackPic;
    ImageView badgeFrontPic;
    ImageView badgeBackPic;
    ImageView billPic;

    Button add;
    RelativeLayout placeHolder;
    ScrollView Documents;

    Button editDvlaFront;
    Button editDvlaBack;
    Button editBadgeFront;
    Button editBadgeBack;
    Button editBill;

    private final int IMG_REQUEST_dvlaFront = 1;
    private final int IMG_REQUEST_dvlaBack = 2;
    private final int IMG_REQUEST_badgeFront = 3;
    private final int IMG_REQUEST_badgeBack = 4;
    private final int IMG_REQUEST_bill = 5;

    private Uri imageUri;
    private static final String uploadURL = "http://www.hire-it.co/Documents/upload.php";
    String DIR = "";
    Bitmap bitmapDvlaFront = null;
    Bitmap bitmapDvlaBack = null;
    Bitmap bitmapBadgeFront = null;
    Bitmap bitmapBadgeBack = null;
    Bitmap bitmapBill=null;
    int width, height;
    private RequestQueue requestQueue;
    private StringRequest stringRequest;
    int COUNT = 0;
    int PICTURE_COUNT = 0;
    Global preferences;
    Boolean flag = true;
    View vi;

    Button submitDoc;
    CheckBox checkAccept;
    TextView contract;

    Button gallery;
    Button camera;
    LinearLayout open;

    boolean isDvlaFront= false;
    boolean isDvlaBack= false;
    boolean isBadgeFront= false;
    boolean isBadgeBack= false;
    boolean isBill= false;

    boolean isGallery= false;
    boolean isCamera= false;
    ScrollView sc;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_documents, container, false);

        Global.setUploadPhotos(false);

        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA, android.Manifest.permission.READ_PHONE_STATE, android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    1);
        }

        Documents = (ScrollView) v.findViewById(R.id.sc);
        placeHolder = (RelativeLayout) v.findViewById(R.id.placeHolder);
        gallery = (Button) v.findViewById(R.id.gallery);
        camera = (Button) v.findViewById(R.id.camera);
        open = (LinearLayout) v.findViewById(R.id.open);

        sc = (ScrollView) v.findViewById(R.id.sc);
        vi = (View) v.findViewById(R.id.vi);


        dvlaFrontPic = (ImageView) v.findViewById(R.id.dvlaFrontPic);
        dvlaBackPic = (ImageView) v.findViewById(R.id.dvlaBackPic);
        badgeFrontPic = (ImageView) v.findViewById(R.id.badgeFrontPic);
        badgeBackPic = (ImageView) v.findViewById(R.id.badgeBackPic);
        billPic = (ImageView) v.findViewById(R.id.billPic);

        editDvlaFront = (Button) v.findViewById(R.id.editDvlaFront);
        editDvlaBack = (Button) v.findViewById(R.id.editDvlaBack);
        editBadgeFront = (Button) v.findViewById(R.id.editBadgeFront);
        editBadgeBack = (Button) v.findViewById(R.id.editBadgeBack);
        editBill = (Button) v.findViewById(R.id.editBill);

        add = (Button) v.findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.container, new UploadPhotos());
                ft.addToBackStack(null);
                ft.commit();

            }
        });

        String function = "/GetDocuments?email=" + Global.getNumber() + "@hire-it.co";
        GetProfile asyncTask = new GetProfile(getActivity(), new AsyncResponse() {

            @Override
            public void processFinish(Object result) {

                String ans = result + "";
                JSONObject jsonObj = null;


                try {
                    jsonObj = new JSONObject(ans);

                    Global.documentsResponse = new HireitGetDocumentsResponse(jsonObj);
                    result = Global.documentsResponse.getDocuments().getResult();
                    if (result.equals("Success")) {
                        Toast.makeText(getActivity(), result.toString(), Toast.LENGTH_LONG).show();
                        Documents.setVisibility(View.VISIBLE);
                        placeHolder.setVisibility(View.GONE);
                        add.setVisibility(View.GONE);


                        dvlaBack = Global.documentsResponse.getDocuments().getDVLABack();
                        dvlaFront = Global.documentsResponse.getDocuments().getDVLAFront();
                        badgeBack = Global.documentsResponse.getDocuments().getTaxiBadgeBack();
                        badgeFront = Global.documentsResponse.getDocuments().getTaxiBadgeFront();
                        bill = Global.documentsResponse.getDocuments().getTaxiBadgeFront();

                        Glide.with(getActivity())
                                .load(dvlaFront)
                                .centerCrop()
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .placeholder(R.drawable.pugnotification_ic_placeholder)
                                .into(dvlaFrontPic);

                        Glide.with(getActivity())
                                .load(dvlaBack)
                                .centerCrop()
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .placeholder(R.drawable.pugnotification_ic_placeholder)
                                .into(dvlaBackPic);

                        Glide.with(getActivity())
                                .load(badgeFront)
                                .centerCrop()
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .placeholder(R.drawable.pugnotification_ic_placeholder)
                                .into(badgeFrontPic);

                        Glide.with(getActivity())
                                .load(badgeBack)
                                .centerCrop()
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .placeholder(R.drawable.pugnotification_ic_placeholder)
                                .into(badgeBackPic);

                        Glide.with(getActivity())
                                .load(bill)
                                .centerCrop()
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .placeholder(R.drawable.pugnotification_ic_placeholder)
                                .into(billPic);

                    } else {


                    //    Toast.makeText(getActivity(), result.toString(), Toast.LENGTH_LONG).show();


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, function + "__" + "1");
        asyncTask.execute();

        vi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                open.setVisibility(View.GONE);
                vi.setVisibility(View.GONE);
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                open.setVisibility(View.GONE);
                sc.setVisibility(View.VISIBLE);
                vi.setVisibility(View.GONE);
                if (isDvlaFront) {
                    isGallery=true;
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    photoPickerIntent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(photoPickerIntent, IMG_REQUEST_dvlaFront);

                }
                else if (isDvlaBack) {
                    isGallery=true;
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    photoPickerIntent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(photoPickerIntent, IMG_REQUEST_dvlaBack);

                }
                else if (isBadgeFront) {
                    isGallery=true;
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    photoPickerIntent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(photoPickerIntent, IMG_REQUEST_badgeFront);

                }
               else if (isBadgeBack) {
                    isGallery=true;
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    photoPickerIntent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(photoPickerIntent, IMG_REQUEST_badgeBack);

                }
                else if (isBadgeBack) {
                    isGallery=true;
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    photoPickerIntent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(photoPickerIntent, IMG_REQUEST_badgeBack);

                }

                else if (isBill) {
                    isGallery=true;
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    photoPickerIntent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(photoPickerIntent, IMG_REQUEST_bill);

                }

            }
        });

        camera.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View view) {
                isGallery = false;
                if (isDvlaFront) {
                    Toast.makeText(getActivity(), "Camera", Toast.LENGTH_SHORT).show();
                    open.setVisibility(View.GONE);
                    vi.setVisibility(View.GONE);
                    sc.setVisibility(View.VISIBLE);

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                        // fileTemp = ImageUtils.getOutputMediaFile();
                        ContentValues values = new ContentValues(1);
                        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
                        imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

                        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

                        startActivityForResult(intent, IMG_REQUEST_dvlaFront);
                    }
                } else if (isDvlaBack) {
                    Toast.makeText(getActivity(), "Camera", Toast.LENGTH_SHORT).show();
                    open.setVisibility(View.GONE);
                    vi.setVisibility(View.GONE);
                    sc.setVisibility(View.VISIBLE);
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                        // fileTemp = ImageUtils.getOutputMediaFile();
                        ContentValues values = new ContentValues(1);
                        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
                        imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

                        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

                        startActivityForResult(intent, IMG_REQUEST_dvlaBack);
                    }

                } else if (isBadgeFront) {
                    Toast.makeText(getActivity(), "Camera", Toast.LENGTH_SHORT).show();
                    open.setVisibility(View.GONE);
                    vi.setVisibility(View.GONE);
                    sc.setVisibility(View.VISIBLE);
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                        // fileTemp = ImageUtils.getOutputMediaFile();
                        ContentValues values = new ContentValues(1);
                        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
                        imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

                        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

                        startActivityForResult(intent, IMG_REQUEST_badgeFront);
                    }

                } else if (isBadgeBack) {
                    Toast.makeText(getActivity(), "Camera", Toast.LENGTH_SHORT).show();
                    open.setVisibility(View.GONE);
                    vi.setVisibility(View.GONE);
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                        // fileTemp = ImageUtils.getOutputMediaFile();
                        ContentValues values = new ContentValues(1);
                        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
                        imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

                        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

                        startActivityForResult(intent, IMG_REQUEST_badgeBack);
                    }

                } else if (isBill) {
                    Toast.makeText(getActivity(), "Camera", Toast.LENGTH_SHORT).show();
                    open.setVisibility(View.GONE);
                    vi.setVisibility(View.GONE);
                    sc.setVisibility(View.VISIBLE);
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                        // fileTemp = ImageUtils.getOutputMediaFile();
                        ContentValues values = new ContentValues(1);
                        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
                        imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

                        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

                        startActivityForResult(intent, IMG_REQUEST_bill);
                    }

                }


            }

        });


        editDvlaFront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                vi.setVisibility(View.VISIBLE);
                open.setVisibility(View.VISIBLE);

                isDvlaFront=true;
                isDvlaBack=false;
                isBadgeFront=false;
                isBadgeBack=false;
                isBill=false;

            }
        });

        editDvlaBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                vi.setVisibility(View.VISIBLE);
                open.setVisibility(View.VISIBLE);

                isDvlaFront=false;
                isDvlaBack=true;
                isBadgeFront=false;
                isBadgeBack=false;
                isBill=false;
            }
        });
        editBadgeFront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                vi.setVisibility(View.VISIBLE);
                open.setVisibility(View.VISIBLE);

                isDvlaFront=false;
                isDvlaBack=false;
                isBadgeFront=true;
                isBadgeBack=false;
                isBill=false;

            }
        });
        editBadgeBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vi.setVisibility(View.VISIBLE);
                open.setVisibility(View.VISIBLE);

                isDvlaFront=false;
                isDvlaBack=false;
                isBadgeFront=false;
                isBadgeBack=true;
                isBill=false;

            }
        });
        editBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vi.setVisibility(View.VISIBLE);
                open.setVisibility(View.VISIBLE);

                isDvlaFront=false;
                isDvlaBack=false;
                isBadgeFront=false;
                isBadgeBack=false;
                isBill=true;

            }
        });

        return v;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        _v_ = view;
    }

    private String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private void uploadImageDvlaFront() {

        stringRequest = new StringRequest(Request.Method.POST, uploadURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(getActivity(), "Successfully Uploaded", Toast.LENGTH_LONG).show();

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
              //  Toast.makeText(getActivity(), "Please try again", Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("email", Global.getNumber() + "@hire-it.co");
                hashMap.put("picname", "DVLAFront");
                String uploadImage = getStringImage(bitmapDvlaFront);
                hashMap.put("image", uploadImage);
             /*   bitmapDvlaFront.recycle();
                bitmapDvlaFront = null;*/
                System.gc();
                return hashMap;
            }
        };
        //Toast.makeText(getActivity(), "Picture Link = "+pictureLink, Toast.LENGTH_SHORT).show();
        requestQueue = new Volley().newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    private void uploadImageDvlaBack() {
        stringRequest = new StringRequest(Request.Method.POST, uploadURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(getActivity(), "Successfully Uploaded", Toast.LENGTH_LONG).show();

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
             //   Toast.makeText(getActivity(), "Please try again", Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("email", Global.getNumber() + "@hire-it.co");
                hashMap.put("picname", "DVLABack");
                String uploadImage = getStringImage(bitmapDvlaBack);
                hashMap.put("image", uploadImage);
               /* bitmapDvlaBack.recycle();
                bitmapDvlaBack = null;*/
                System.gc();
                return hashMap;
            }
        };
        //Toast.makeText(getActivity(), "Picture Link = "+pictureLink, Toast.LENGTH_SHORT).show();
        requestQueue = new Volley().newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    private void uploadImageBadgeFront() {
        stringRequest = new StringRequest(Request.Method.POST, uploadURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(getActivity(), "Successfully Uploaded", Toast.LENGTH_LONG).show();

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
              //  Toast.makeText(getActivity(), "Please try again", Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("email", Global.getNumber() + "@hire-it.co");
                hashMap.put("picname", "TaxiBadgeNumberFront");
                String uploadImage = getStringImage(bitmapBadgeFront);
                hashMap.put("image", uploadImage);
             /*  bitmapBadgeFront.recycle();
                bitmapBadgeFront = null;*/
                System.gc();
                return hashMap;
            }
        };
        //Toast.makeText(getActivity(), "Picture Link = "+pictureLink, Toast.LENGTH_SHORT).show();
        requestQueue = new Volley().newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    private void uploadBadgeBack() {
        stringRequest = new StringRequest(Request.Method.POST, uploadURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(getActivity(), "Successfully Uploaded", Toast.LENGTH_LONG).show();

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
             //   Toast.makeText(getActivity(), "Please try again", Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("email", Global.getNumber() + "@hire-it.co");
                hashMap.put("picname", "TaxiBadgeNumberBack");
                String uploadImage = getStringImage(bitmapBadgeBack);
                hashMap.put("image", uploadImage);
             /*   bitmapBadgeBack.recycle();
                bitmapBadgeBack = null;*/
                System.gc();
                return hashMap;
            }
        };
        //Toast.makeText(getActivity(), "Picture Link = "+pictureLink, Toast.LENGTH_SHORT).show();
        requestQueue = new Volley().newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }
    private void uploadBill() {
        stringRequest = new StringRequest(Request.Method.POST, uploadURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(getActivity(), "Successfully Uploaded", Toast.LENGTH_LONG).show();

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
             //   Toast.makeText(getActivity(), "Please try again", Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("email", Global.getNumber() + "@hire-it.co");
                hashMap.put("picname", "UtilityBillOrTaxiLetter");
                String uploadImage = getStringImage(bitmapBill);
                hashMap.put("image", uploadImage);
             /*   bitmapBadgeBack.recycle();
                bitmapBadgeBack = null;*/
                System.gc();
                return hashMap;
            }
        };
        //Toast.makeText(getActivity(), "Picture Link = "+pictureLink, Toast.LENGTH_SHORT).show();
        requestQueue = new Volley().newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMG_REQUEST_dvlaFront && resultCode == RESULT_OK) {

            if (isGallery){

                Uri path = data.getData();
                try {
                    bitmapDvlaFront = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), path);

                    dvlaFrontPic.setImageBitmap(bitmapDvlaFront);
                    uploadImageDvlaFront();

                    Global.dismissLoadingPopup();

                } catch (IOException e) {
                    e.printStackTrace();
                }
                flag = false;
            }
            else {
                Uri selectedImage = imageUri;//Uri.fromFile(new File(imageFilePath));

                getActivity().getContentResolver().notifyChange(selectedImage, null);
                ContentResolver cr = getActivity().getContentResolver();
                try {
                    Toast.makeText(getActivity(), selectedImage.toString(),
                            Toast.LENGTH_LONG).show();
                    bitmapDvlaFront = MediaStore.Images.Media.getBitmap(cr, selectedImage);

                    dvlaFrontPic.setImageBitmap(bitmapDvlaFront);
                    uploadImageDvlaFront();

                    Global.dismissLoadingPopup();
                    flag = false;


                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

        } else if (requestCode == IMG_REQUEST_dvlaBack && resultCode == RESULT_OK ) {

            if (isGallery) {

                Uri path = data.getData();
                try {
                    bitmapDvlaBack = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), path);

                    dvlaBackPic.setImageBitmap(bitmapDvlaBack);
                    uploadImageDvlaBack();

                    Global.dismissLoadingPopup();

                } catch (IOException e) {
                    e.printStackTrace();
                }
                flag = false;
            } else {
                Uri selectedImage = imageUri;//Uri.fromFile(new File(imageFilePath));

                getActivity().getContentResolver().notifyChange(selectedImage, null);
                ContentResolver cr = getActivity().getContentResolver();
                try {
                    Toast.makeText(getActivity(), selectedImage.toString(),
                            Toast.LENGTH_LONG).show();
                    bitmapDvlaBack = MediaStore.Images.Media.getBitmap(cr, selectedImage);

                    dvlaBackPic.setImageBitmap(bitmapBadgeBack);
                    uploadImageDvlaBack();

                    Global.dismissLoadingPopup();
                    flag = false;


                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        } else if (requestCode == IMG_REQUEST_badgeFront && resultCode == RESULT_OK ) {
            if (isGallery) {

                Uri path = data.getData();
                try {
                    bitmapBadgeFront = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), path);

                    badgeFrontPic.setImageBitmap(bitmapBadgeFront);
                    uploadImageBadgeFront();

                    Global.dismissLoadingPopup();

                } catch (IOException e) {
                    e.printStackTrace();
                }
                flag = false;
            } else {
                Uri selectedImage = imageUri;//Uri.fromFile(new File(imageFilePath));

                getActivity().getContentResolver().notifyChange(selectedImage, null);
                ContentResolver cr = getActivity().getContentResolver();
                try {
                    Toast.makeText(getActivity(), selectedImage.toString(),
                            Toast.LENGTH_LONG).show();
                    bitmapBadgeFront = MediaStore.Images.Media.getBitmap(cr, selectedImage);

                    badgeFrontPic.setImageBitmap(bitmapBadgeFront);
                    uploadImageBadgeFront();

                    Global.dismissLoadingPopup();
                    flag = false;


                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        } else if (requestCode == IMG_REQUEST_badgeBack && resultCode == RESULT_OK ) {
            if (isGallery) {

                Uri path = data.getData();
                try {
                    bitmapBadgeBack = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), path);

                    badgeBackPic.setImageBitmap(bitmapBadgeBack);
                    uploadBadgeBack();

                    Global.dismissLoadingPopup();

                } catch (IOException e) {
                    e.printStackTrace();
                }
                flag = false;
            } else {
                Uri selectedImage = imageUri;//Uri.fromFile(new File(imageFilePath));

                getActivity().getContentResolver().notifyChange(selectedImage, null);
                ContentResolver cr = getActivity().getContentResolver();
                try {
                    Toast.makeText(getActivity(), selectedImage.toString(),
                            Toast.LENGTH_LONG).show();
                    bitmapBadgeBack = MediaStore.Images.Media.getBitmap(cr, selectedImage);

                    badgeBackPic.setImageBitmap(bitmapBadgeBack);
                    uploadBadgeBack();

                    Global.dismissLoadingPopup();
                    flag = false;


                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }  else if (requestCode == IMG_REQUEST_bill && resultCode == RESULT_OK ) {
            if (isGallery) {

                Uri path = data.getData();
                try {
                    bitmapBill = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), path);

                    billPic.setImageBitmap(bitmapBill);
                    uploadBill();

                    Global.dismissLoadingPopup();

                } catch (IOException e) {
                    e.printStackTrace();
                }
                flag = false;
            } else {
                Uri selectedImage = imageUri;//Uri.fromFile(new File(imageFilePath));

                getActivity().getContentResolver().notifyChange(selectedImage, null);
                ContentResolver cr = getActivity().getContentResolver();
                try {
                    Toast.makeText(getActivity(), selectedImage.toString(),
                            Toast.LENGTH_LONG).show();
                    bitmapBill = MediaStore.Images.Media.getBitmap(cr, selectedImage);

                    billPic.setImageBitmap(bitmapBill);
                    uploadBill();

                    Global.dismissLoadingPopup();
                    flag = false;


                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }  else {
            Toast.makeText(getActivity(), "Something went Wrong", Toast.LENGTH_SHORT).show();

        }
    }

}
