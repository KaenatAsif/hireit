package hireit.hireit.drawer.DrawerFragments;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import belka.us.androidtoggleswitch.widgets.ToggleSwitch;
import cn.pedant.SweetAlert.SweetAlertDialog;
import hireit.hireit.AsyncResponse;
import hireit.hireit.GetProfile;
import hireit.hireit.Global;
import hireit.hireit.Models.HireitLoginResponse;
import hireit.hireit.R;
import hireit.hireit.drawer.MainActivity;

public class ConfirmBooking extends AppCompatActivity {

     String carFare;
    String reffCodde = "";
    private ViewPager mViewPager;


    //private ShadowTransformer mCardShadowTransformer;

    View v;
    View HEALTH_CARDS, LIST_VIEW;

    private static final String KEY_MOVIE_TITLE = "key_title";
    private static final int ANIM_DURATION_TOOLBAR = 750;

    ToggleSwitch toggleSwitch;
    Button bookNow;
    ListView lv;
    Toolbar toolbar;
    int height;
    private String carName;
    private String carModel;
    private String carColor;
    private String carPower;
    private String carPrice;
    private String carPic;
    private String carId;

    @Override
    public void onBackPressed() {
        Intent goToCar = new Intent(this, MainActivity.class);
        startActivity(goToCar);

    }

    TextView carsName;
    TextView model;
    TextView power;
    TextView price;
    TextView color;
EditText enter;
    Button useCode;
    private String refferalText;

    LinearLayout refferalLayout;
    CardView DiscLayout;

    TextView DiscPrice;
    TextView AfetDiscPrice;
TextView weekText;


    static CarDetailsActivity cars = new CarDetailsActivity();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_confirm_booking);

        toolbar = (Toolbar) findViewById(R.id.tool);
        refferalLayout = (LinearLayout) findViewById(R.id.referral_layout);
        DiscLayout= (CardView) findViewById(R.id.discLayout);
        DiscPrice =(TextView) findViewById(R.id.priceDisc);
        AfetDiscPrice= (TextView) findViewById(R.id.priceAfterDisc);
        useCode= (Button) findViewById(R.id.ueseCode);
        weekText= (TextView) findViewById(R.id.weekText);
        weekText.setText("You have booked car for "+Global.getWeeks()+" weeks");


        enter= (EditText) findViewById(R.id.enter);
        enter.setInputType(InputType.TYPE_NULL);
        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enter.setInputType(InputType.TYPE_CLASS_TEXT);
                enter.setFocusable(true);
            }
        });

        Resources a = getResources();
        int index = Global.getArrayIndex();
        if (Global.isLeeds()) {

            carName = Global.getLeedsArray().get(index).getCarName();
            carModel = Global.getLeedsArray().get(index).getModel();
            carColor = Global.getLeedsArray().get(index).getColor();
            carPower = Global.getLeedsArray().get(index).getEnginePower();
            carPrice = Global.getLeedsArray().get(index).getCarPriceContract().getFinalPrice();
            carFare= carPrice;
            carId = String.valueOf(Global.getLeedsArray().get(index).getId());

        } else if (Global.isKirklees()) {
            carName = Global.getKirkleesArray().get(index).getCarName();
            carModel = Global.getKirkleesArray().get(index).getModel();
            carColor = Global.getKirkleesArray().get(index).getColor();
            carPower = Global.getKirkleesArray().get(index).getEnginePower();
            carPrice = Global.getKirkleesArray().get(index).getCarPriceContract().getFinalPrice();
            carId = String.valueOf(Global.getKirkleesArray().get(index).getId());


            carFare= carPrice;
        } else if (Global.isWakefield()) {

            carName = Global.getWakefieldArray().get(index).getCarName();
            carModel = Global.getWakefieldArray().get(index).getModel();
            carColor = Global.getWakefieldArray().get(index).getColor();
            carPower = Global.getWakefieldArray().get(index).getEnginePower();
            carPrice = Global.getWakefieldArray().get(index).getCarPriceContract().getFinalPrice();
            carId = String.valueOf(Global.getWakefieldArray().get(index).getId());

            carFare= carPrice;

        } else if (Global.isBradFord()) {

            carName = Global.getBradfordArray().get(index).getCarName();
            carModel = Global.getBradfordArray().get(index).getModel();
            carColor = Global.getBradfordArray().get(index).getColor();
            carPower = Global.getBradfordArray().get(index).getEnginePower();
            carPrice = Global.getBradfordArray().get(index).getCarPriceContract().getFinalPrice();
            carId = String.valueOf(Global.getBradfordArray().get(index).getId());

            carFare= carPrice;
        }

        carsName = (TextView) findViewById(R.id.carName);
        model = (TextView) findViewById(R.id.model);
        power = (TextView) findViewById(R.id.power);
        price = (TextView) findViewById(R.id.price);
        color = (TextView) findViewById(R.id.color);


        carsName.setText(carName);
        model.setText(carModel);
        power.setText(carPower);
        price.setText("£"+carPrice);
        color.setText(carColor);

        useCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String function ="CheckReferalCode?email="+Global.getNumber()+"@hire-it.co&refferalcode="+enter.getText().toString();
                GetProfile asyncTask = new GetProfile(ConfirmBooking.this, new AsyncResponse() {

                    @Override
                    public void processFinish(Object result) throws JSONException {
                        String ans = result + "";
                        JSONObject jsonObj = null;
                        try {
                            jsonObj = new JSONObject(ans);

                            Global.checkRefferal = new HireitLoginResponse(jsonObj);
                            String result1 = Global.checkRefferal.getResult().toString();
                            if (result1.equals("Valid for using RefferalCode")) {
                                Toast.makeText(ConfirmBooking.this, result1.toString(), Toast.LENGTH_LONG).show();
                                int price = Integer.parseInt(carPrice) -10;

                                carFare = String.valueOf(price);
                                refferalText = enter.getText().toString();

                                DiscLayout.setAlpha(1);
                                DiscLayout.setVisibility(View.VISIBLE);
                                refferalLayout.setAlpha(0);
                                refferalLayout.setVisibility(View.GONE);
                                DiscPrice.setText("£"+carPrice);
                                AfetDiscPrice.setText("£"+carFare);



                            } else if(result1.equals("User Used RefferalCode once")){

                                new SweetAlertDialog(ConfirmBooking.this)
                                        .setTitleText(result1.toString())
                                        .setContentText("Please Try again")
                                        .show();
                            }
                            else
                                new SweetAlertDialog(ConfirmBooking.this)
                                        .setTitleText(result1.toString())
                                        .setContentText("Please Try again")
                                        .show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, function + "__" + "1");
                asyncTask.execute();

            }


        });

        bookNow = (Button) findViewById(R.id.bookNow);
        bookNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String function = "ConfirmHire?email="+Global.getNumber()+"@hire-it.co&carid="+carId+"&NoOfWeeks="+Global.getWeeks()+"&FareOfCar="+carFare+"&refferalcode="+refferalText;
                GetProfile asyncTask = new GetProfile(ConfirmBooking.this, new AsyncResponse() {

                    @Override
                    public void processFinish(Object result) throws JSONException {
                        String ans = result + "";
                        JSONObject jsonObj = null;
                        try {
                            jsonObj = new JSONObject(ans);

                            Global.confirmBooking = new HireitLoginResponse(jsonObj);
                            String result1 = Global.confirmBooking.getResult().toString();
                            if (result1.equals("Success")) {
                                Toast.makeText(ConfirmBooking.this, result1.toString(), Toast.LENGTH_LONG).show();
                              /*  new SweetAlertDialog(ConfirmBooking.this)
                                        .setTitleText("Success")
                                        .setContentText("You have booked the car")
                                        .show();
                                */
                              Intent i= new Intent(ConfirmBooking.this,CongratsActivity.class);
                                startActivity(i);

                            } else {
                                Toast.makeText(ConfirmBooking.this, "Not Booked", Toast.LENGTH_LONG).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, function + "__" + "1");
                asyncTask.execute();

            }


            // sendVarificationCode();

                /*Intent sendCode= new Intent(getActivity(),SignUpActivity.class).putExtra("Fragments","Confirm Code");
                startActivity(sendCode);

*/
//                new SweetAlertDialog(getActivity(),SweetAlertDialog.PROGRESS_TYPE)
//                        .setTitleText("Please Wait")
//                        .show();
//


        });


    }

}
