package hireit.hireit.drawer.DrawerFragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import hireit.hireit.AsyncResponse;
import hireit.hireit.GetProfile;
import hireit.hireit.Global;
import hireit.hireit.Models.HireitCarsNew;
import hireit.hireit.Models.HireitGetLedgerResponse;
import hireit.hireit.R;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class WalletFragment extends Fragment {

    private static final String KEY_MOVIE_TITLE = "key_title";
    private String picLink;

    private Uri picUrl;

    private String carsName;
    private String price;
    private String remaining;
    private int carId;
    private String totalAmount;
TextView total;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_wallet, container, false);
    }

    ArrayList<ListData> main;
    JSONArray jarr;
    SampleAdapter sa;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        main = new ArrayList<>();
        sa = new SampleAdapter();

        final ListView lv = view.findViewById(R.id.listWallet);

        total = view.findViewById(R.id.total);
        lv.setAdapter(sa);


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String ItemcarId= String.valueOf(sa.getItem(i).carId);
                Global.setItemCarId(ItemcarId);
                String name=sa.getItem(i).carName;

                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.container, new WalletDetailsFragment());
                ft.addToBackStack(null);
                ft.commit();

            }
        });

        String function="CustomerLedger?email="+Global.getNumber()+"@hire-it.co";
        GetProfile asyncTask= new GetProfile(getActivity(), new AsyncResponse() {
            @Override
            public void processFinish(Object result) {

                String ans = result + "";
                JSONObject jsonObj = null;
                JSONArray ledgerRespose = null;
                JSONObject details;
                JSONObject details1;

                try {
                    jsonObj = new JSONObject(ans);
                    Global.ledgerResponse = new HireitGetLedgerResponse(jsonObj);
                    if (Global.ledgerResponse.getCustomerLedgerResult().getResult().equals("Success")) {

                        totalAmount= Global.ledgerResponse.getCustomerLedgerResult().getTotalBalance();
                        total.setText(totalAmount);
                        for (int i = 0; i < Global.ledgerResponse.getCustomerLedgerResult().getCustomerLedger().size(); i++) {
                            carsName = Global.ledgerResponse.getCustomerLedgerResult().getCustomerLedger().get(i).getCarName();
                            price = Global.ledgerResponse.getCustomerLedgerResult().getCustomerLedger().get(i).getTotalPrice();
                            remaining = String.valueOf(Global.ledgerResponse.getCustomerLedgerResult().getCustomerLedger().get(i).getRemaining());
                            carId =Global.ledgerResponse.getCustomerLedgerResult().getCustomerLedger().get(i).getCarId();

                            sa.addEntry(new ListData(carsName,price,remaining,carId));
                        }

                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        },function+"__"+"1");
        asyncTask.execute();


    }

    private static class ViewHolder {

        private View view;

        private TextView Remaining;
        private TextView carName;
        private TextView carPrice;

        private ViewHolder(View view) {
            this.view = view;
            carName= view.findViewById(R.id.carName);
            carPrice = view.findViewById(R.id.price);
            Remaining=view.findViewById(R.id.remaining);



        }

    }



    private class SampleAdapter extends BaseAdapter {
        SampleAdapter() {
            main = new ArrayList<>();
           // LeedsArray= new ArrayList<>();
        }


        public void addEntry(ListData l) {
            main.add(l);
            notifyDataSetChanged();
        }
        public void addEntryLeeds(HireitCarsNew l) {
          //  LeedsArray.add(l);
            notifyDataSetChanged();
        }

        public void clear() {
            main.clear();
            notifyDataSetChanged();
        }



        @Override
        public int getCount() {
            return main.size();
        }

        @Override
        public ListData getItem(int position) {
            return main.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = View.inflate(getActivity(), R.layout.lisitem_wallet, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            ListData item = getItem(position);
            DisplayMetrics dm = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);

            int width = dm.widthPixels;
            int height = dm.heightPixels;

            holder.carName.setText(item.carName);
            holder.carPrice.setText(item.carPrice);
            holder.Remaining.setText(item.carRemaining);




         /*    Bitmap bt= getBitmapFromURL(picLink);
            holder.carImg.setImageBitmap(getBitmapFromURL(bt.toString()));
*/


            return convertView;

        }
    }



    private class ListData {
        private String carName;
        private String carPrice;
        private String carRemaining;
        private int carId;

        //["Sehat Sahara Plus", "2", "2", "10", "16", "7\/19\/2017 12:00:00 AM", "7\/19\/2018 12:00:00 AM"]
        public ListData(String carName,String carPrice,String carRemaining,int carID) {
            this.carName = carName;
            this.carPrice = carPrice;
            this.carRemaining = carRemaining;
            this.carId = carID;


        }
    }



}

