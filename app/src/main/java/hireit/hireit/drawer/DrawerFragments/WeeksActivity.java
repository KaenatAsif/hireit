package hireit.hireit.drawer.DrawerFragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import hireit.hireit.Global;
import hireit.hireit.R;

public class WeeksActivity extends AppCompatActivity {

    TextView weekNumber;
    int weeks=1;
    Button cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weeks);


        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .95), (int) (height * .6));


        weekNumber= (TextView) findViewById(R.id.weekNumber);
        cancel= (Button) findViewById(R.id.cancel);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void minus(View view) {
        if (weeks<2){
            return;
        }
        weeks-=1;
        weekNumber.setText(String.valueOf(weeks));

    }

    public void plus(View view) {
        if (weeks>11){
            return;
        }
        weeks+=1;
        weekNumber.setText(String.valueOf(weeks));


    }

    public void done(View view) {

        Global.setWeeks(weekNumber.getText().toString());

        Intent goToConfirm= new Intent(WeeksActivity.this,ConfirmBooking.class);
        startActivity(goToConfirm);

        finish();



    }
}
