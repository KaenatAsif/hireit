package hireit.hireit.drawer.DrawerFragments;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import fr.ganfra.materialspinner.MaterialSpinner;
import hireit.hireit.AsyncResponse;
import hireit.hireit.GetProfile;
import hireit.hireit.Global;
import hireit.hireit.Models.ClaimsHistory;
import hireit.hireit.Models.HireitCarsNew;
import hireit.hireit.R;

import static android.app.Activity.RESULT_OK;

public class claimHistory extends Fragment {

    private static final String KEY_MOVIE_TITLE = "key_title";
    private Uri picUrl;
    private String carsName;
    private String price;
    private String remaining;
    TextView carName;
    ArrayList<ListData> main;
    JSONArray jarr;
    SampleAdapter sa;
    MaterialSpinner claimSpinner;
    ArrayList<String> claimList;

    private String Id;
    private String Note;
    private String Paid;
    private String PaymentDate;
    private String Remaining;
    String picLink;
    Boolean flag=true;
    ImageButton gallery;
    ImageButton camera;
    private final int IMG_REQUEST_cam = 1;
    private final int IMG_REQUEST_gallery = 2;

    Bitmap bitmapImage = null;
    boolean isGallery = false;
    private RequestQueue requestQueue;
    private StringRequest request;
    TextView claimNo;
    int index;

    String status;
    String picName;
    TextView statusText;

    private static final String URL = "http://www.hire-it.co/ClaimsDocuments/upload.php";
    RelativeLayout bootomPannel;
    Uri imageUri;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_claim_history2, container, false);


        carName=(TextView) view.findViewById(R.id.carName);
        claimSpinner = (MaterialSpinner) view.findViewById(R.id.claimSpinner);
        gallery = (ImageButton) view.findViewById(R.id.gallery);
        camera = (ImageButton) view.findViewById(R.id.fabCamera);
        bootomPannel = (RelativeLayout) view.findViewById(R.id.bottomPanel);

        claimNo = (TextView) view.findViewById(R.id.claimNo);
        statusText = (TextView) view.findViewById(R.id.status);

        main = new ArrayList<>();
        sa = new SampleAdapter();
        claimList= new ArrayList<>();

        final ListView lv = view.findViewById(R.id.listclaimhistory);

        lv.setAdapter(sa);

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, claimList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        claimSpinner.setAdapter(adapter);

        requestQueue = Volley.newRequestQueue(getActivity());

        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        bootomPannel.setVisibility(View.VISIBLE);


        String function="GetClaimsHistory?email="+ Global.getNumber()+"@hire-it.co&carid="+Global.getItemCarId();
        GetProfile asyncTask= new GetProfile(getActivity(), new AsyncResponse() {
            @Override
            public void processFinish(Object result) {

                String ans = result + "";
                JSONObject jsonObj = null;

                try {
                    JsonParser parser = new JsonParser();
                    JsonElement mJson =  parser.parse(ans);
                    Gson gson = new Gson();
                    ClaimsHistory object = gson.fromJson(mJson, ClaimsHistory.class);
                    Global.claimsHistoryResult=object;
                    if (Global.isRaiseClaim()) {
                        claimSpinner.setVisibility(View.GONE);
                        claimNo.setVisibility(View.VISIBLE);
                        claimNo.setText(Global.getClaimNo());


                        int index = Global.claimsHistoryResult.getGetClaimsHistoryResult().getCustomerClaimImage().size() - 1;
                        sa.clear();

                        picName = String.valueOf(Global.claimsHistoryResult.getGetClaimsHistoryResult().getCustomerClaimImage()
                                .get(index).getPiclink().size() + 1);
                        status = Global.claimsHistoryResult.getGetClaimsHistoryResult().getCustomerClaimImage()
                                .get(index).getStatus();

                        if (status.equals("Pending")) {

                            statusText.setText("Active");
                            bootomPannel.setVisibility(View.VISIBLE);

                        } else {


                            statusText.setText("Closed");
                            bootomPannel.setVisibility(View.GONE);
                        }

                        for (int j = 0; j < Global.claimsHistoryResult.getGetClaimsHistoryResult()
                                    .getCustomerClaimImage().get(index).getPiclink().size(); j++) {
                                picLink = Global.claimsHistoryResult.getGetClaimsHistoryResult().getCustomerClaimImage()
                                        .get(index).getPiclink().get(j);
                                sa.addEntry(new ListData(picLink));
                        }
                    }
                    else {

                        claimList.clear();

                        for (int i = 0; i < Global.claimsHistoryResult.getGetClaimsHistoryResult()
                                .getCustomerClaimImage().size(); i++) {
                            claimList.add(Global.claimsHistoryResult.getGetClaimsHistoryResult()
                                    .getCustomerClaimImage().get(i).getClaimNumber());
                        }
                    }
                   /* claimList.get(0).replace("Claim no.",Global.claimsHistoryResult.getGetClaimsHistoryResult()
                            .getCustomerClaimImage().get(1).getClaimNumber().toString());

*/
                    claimSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
                        {
                            index= claimSpinner.getSelectedItemPosition()-1;
                            //int g= adapterView.getSelectedItemPosition();
                            String pos= adapterView.getItemAtPosition(index).toString();
                            Global.setClaimNo(pos);
                            sa.clear();

                            status=Global.claimsHistoryResult.getGetClaimsHistoryResult().getCustomerClaimImage()
                                    .get(index).getStatus();
                            statusText.setText(status);
                            if (!status.equals("Pending")){

                                bootomPannel.setVisibility(View.GONE);
                            }



                            picName = String.valueOf(Global.claimsHistoryResult.getGetClaimsHistoryResult().getCustomerClaimImage()
                                    .get(index).getPiclink().size()+1);




                            //   claimSpinner.setSelection(i);
                            int size=Global.claimsHistoryResult.getGetClaimsHistoryResult().getCustomerClaimImage()
                                    .get(index).getPiclink().size();

                            for (int j = 0; j < size; j++)
                            {
                                picLink = Global.claimsHistoryResult.getGetClaimsHistoryResult().getCustomerClaimImage()
                                        .get(index).getPiclink().get(j);

                                sa.addEntry(new ListData(picLink));
                            }

                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView)
                        {
                            // claimSpinner.setSelection(0);

                        }

                    });

                }
                catch (JsonParseException e)
                {
                    e.printStackTrace();
                }
            }
        },function+"__"+"1");
        asyncTask.execute();

       /* ArrayAdapter<String> adapterD = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, claimList);
        adapterD.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        claimSpinner.setAdapter(adapterD);*/



//        System.out.println("Spinneeer result 2: "+claimSpinner.getSelectedItem().toString());
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                photoPickerIntent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(photoPickerIntent, IMG_REQUEST_gallery);

            }
        });
        camera.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View view) {
                Toast.makeText(getActivity(), "Camera", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                    // fileTemp = ImageUtils.getOutputMediaFile();
                    ContentValues values = new ContentValues(1);
                    values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
                    imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

                    intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

                    startActivityForResult(intent, IMG_REQUEST_cam);
                }
            }
                });


        return view;
    }
    private String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG,50, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMG_REQUEST_gallery && resultCode == RESULT_OK ) {

            Uri path = data.getData();
            try {

                Global.showLoadingPopup(getActivity(), "Uploading Picture");

                bitmapImage = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), path);
                uploadPhoto();

                Global.dismissLoadingPopup();

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {

            Uri selectedImage = imageUri;//Uri.fromFile(new File(imageFilePath));

            getActivity().getContentResolver().notifyChange(selectedImage, null);
            ContentResolver cr = getActivity().getContentResolver();
            try {
                Toast.makeText(getActivity(), selectedImage.toString(),
                        Toast.LENGTH_LONG).show();
                bitmapImage = MediaStore.Images.Media.getBitmap(cr, selectedImage);

                uploadPhoto();

                Global.dismissLoadingPopup();
                flag = false;


            } catch (IOException e) {
                e.printStackTrace();
            }




    }
    }


    private static class ViewHolder
    {

        private View view;
        private ImageView image;
        private ProgressBar progress;
        private TextView status;

        private ViewHolder(View view)
        {
            this.view = view;
            image= view.findViewById(R.id.img);

            progress=view.findViewById(R.id.progress);
        }
    }

    private class SampleAdapter extends BaseAdapter {
        SampleAdapter() {
            main = new ArrayList<>();
            // LeedsArray= new ArrayList<>();
        }

        public void addEntry(ListData l) {
            main.add(l);
            notifyDataSetChanged();
        }
        public void addEntryLeeds(HireitCarsNew l) {
            //  LeedsArray.add(l);
            notifyDataSetChanged();
        }

        public void clear() {
            main.clear();
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return main.size();
        }

        @Override
        public ListData getItem(int position) {
            return main.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = View.inflate(getActivity(), R.layout.liatitem_claimhistory, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            ListData item = getItem(position);
            DisplayMetrics dm = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);

            int width = dm.widthPixels;
            int height = dm.heightPixels;


               // holder.image.setImageBitmap(bitmapImage);

                Glide.with(getActivity())
                        .load(item.image)
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                holder.progress.setVisibility(View.GONE);
                             //   Toast.makeText(getActivity(),"Please try again",Toast.LENGTH_LONG).show();
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {

                                holder.progress.setVisibility(View.GONE);
                                holder.image.setVisibility(View.VISIBLE);

                                return false;
                            }
                        })
                        .placeholder(R.drawable.pugnotification_ic_placeholder)
                        .into(holder.image);




            return convertView;

        }
    }

    private class ListData {

        private String image;


        public ListData(String image) {
            this.image = image;


        }
    }
    private void uploadPhoto(){
        request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                String res=response;
                final String function="AddClaimImage?email="+Global.getNumber()+"@hire-it.co&claimNo="+Global.getClaimNo()+"&imageName="+picName;

                boolean a = false;
                GetProfile async = new GetProfile(getActivity(), new AsyncResponse() {
                    @Override
                    public void processFinish(Object result) {
                        String resp= result.toString();
                        sa.addEntry(new ListData(getStringImage(bitmapImage)));

                        String function="GetClaimsHistory?email="+ Global.getNumber()+"@hire-it.co&carid="+Global.getItemCarId();
                        GetProfile asyncTask= new GetProfile(getActivity(), new AsyncResponse() {
                            @Override
                            public void processFinish(Object result) {

                                String ans = result + "";
                                JSONObject jsonObj = null;

                                try {
                                    JsonParser parser = new JsonParser();
                                    JsonElement mJson =  parser.parse(ans);
                                    Gson gson = new Gson();
                                    ClaimsHistory object = gson.fromJson(mJson, ClaimsHistory.class);
                                    Global.claimsHistoryResult=object;
                                    if (Global.isRaiseClaim()){
                                        claimSpinner.setVisibility(View.GONE);
                                        claimNo.setVisibility(View.VISIBLE);
                                        claimNo.setText(Global.getClaimNo());
                                        int index= Global.claimsHistoryResult.getGetClaimsHistoryResult().getCustomerClaimImage().size()-1;
                                        sa.clear();

                                        for (int j = 0; j < Global.claimsHistoryResult.getGetClaimsHistoryResult()
                                                .getCustomerClaimImage().get(index).getPiclink().size(); j++)
                                        {
                                            picLink = Global.claimsHistoryResult.getGetClaimsHistoryResult().getCustomerClaimImage()
                                                    .get(index).getPiclink().get(j);

                                            sa.addEntry(new ListData(picLink));

                                        }

                                    }
                                    else {

                                        claimList.clear();
                                        for (int i = 0; i < Global.claimsHistoryResult.getGetClaimsHistoryResult()
                                                .getCustomerClaimImage().size(); i++) {
                                            claimList.add(Global.claimsHistoryResult.getGetClaimsHistoryResult()
                                                    .getCustomerClaimImage().get(i).getClaimNumber());
                                        }
                                    }
                   /* claimList.get(0).replace("Claim no.",Global.claimsHistoryResult.getGetClaimsHistoryResult()
                            .getCustomerClaimImage().get(1).getClaimNumber().toString());

*/
                                    claimSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                        @Override
                                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
                                        {
                                            index= claimSpinner.getSelectedItemPosition()-1;
                                            //int g= adapterView.getSelectedItemPosition();
                                            String pos= adapterView.getItemAtPosition(index).toString();
                                            Global.setClaimNo(pos);
                                            sa.clear();

                                            //   claimSpinner.setSelection(i);
                                            int size=Global.claimsHistoryResult.getGetClaimsHistoryResult().getCustomerClaimImage()
                                                    .get(index).getPiclink().size();

                                            for (int j = 0; j < size; j++)
                                            {
                                                picLink = Global.claimsHistoryResult.getGetClaimsHistoryResult().getCustomerClaimImage()
                                                        .get(index).getPiclink().get(j);

                                                sa.addEntry(new ListData(picLink));
                                            }

                                        }
                                        @Override
                                        public void onNothingSelected(AdapterView<?> adapterView)
                                        {
                                            // claimSpinner.setSelection(0);

                                        }

                                    });




                                }
                                catch (JsonParseException e)
                                {
                                    e.printStackTrace();
                                }
                            }
                        },function+"__"+"1");
                        asyncTask.execute();



                    }
                }, function + "__1");

                async.execute();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getActivity(),"Timeout, Please try again",Toast.LENGTH_LONG).show();

                Global.dismissLoadingPopup();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<String, String>();
                hashMap.put("email", Global.getNumber() + "@hire-it.co");
                hashMap.put("claimno", Global.getClaimNo());
                hashMap.put("picname", String.valueOf(picName));
                String uploadImage = getStringImage(bitmapImage);
                hashMap.put("image", uploadImage);
             /*   bitmapDvlaFront.recycle();
                bitmapDvlaFront = null;*/
                System.gc();
                return hashMap;
            }
        };
        requestQueue = new Volley().newRequestQueue(getActivity());
        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        requestQueue.add(request);
    }

}