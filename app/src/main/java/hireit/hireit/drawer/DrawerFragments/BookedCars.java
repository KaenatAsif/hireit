package hireit.hireit.drawer.DrawerFragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import hireit.hireit.AsyncResponse;
import hireit.hireit.GetProfile;
import hireit.hireit.Global;
import hireit.hireit.Models.HireitCarsNew;
import hireit.hireit.Models.HireitCarsResult;
import hireit.hireit.R;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class BookedCars extends Fragment {

    private static final String KEY_MOVIE_TITLE = "key_title";
    private String picLink;

    private Uri picUrl;

    private String carName;
    private String carReg;
    private String remaining;
    private String carId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_booked_cars, container, false);
    }

    ArrayList<ListData> main;
    LinearLayout noCars;
    JSONArray jarr;
    SampleAdapter sa;
    Button raise;
    Button viewClaim;
    View backScreen;
    LinearLayout open;
    String name;
    String number;
    String bookedDate;
    String expiryDate;
    String rent;
    String totalAmount;
    String pic;
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        main = new ArrayList<>();
        sa = new SampleAdapter();

        final ListView lv = view.findViewById(R.id.listBookedCars);
        noCars =(LinearLayout) view.findViewById(R.id.noCars);
        lv.setAdapter(sa);





        String function="HiredCars?email="+Global.getNumber()+"@hire-it.co";
        GetProfile asyncTask = new GetProfile(getActivity(), new AsyncResponse() {
            @Override
            public void processFinish(Object result) {

                String ans = result + "";
                JSONObject jsonObj = null;
                JSONArray caresult = null;
                JSONObject details;
                JSONObject details1;

                try {
                    jsonObj = new JSONObject(ans);

                    caresult = jsonObj.getJSONArray("CarsResult");
                    if (caresult.length()>0){
                        noCars.setVisibility(View.GONE);
                    for (int i = 0; i < caresult.length(); i++) {
                        details = caresult.getJSONObject(i);

                        Global.carsResult = new HireitCarsResult(details);

                        name = Global.carsResult.getCarsNew().getCarName();
                        number = Global.carsResult.getCarsNew().getRegistrationNumber();


                        String[] bookedDateArray= Global.carsResult.getCarsNew().getBookedDate().split(" ");
                        bookedDate=bookedDateArray[0];

                        String[] expiryDateArray= Global.carsResult.getCarsNew().getExpiryDate().split(" ");
                        expiryDate=expiryDateArray[0];
                        if (Global.carsResult.getCarsNew().getCarPictureList().size()>0) {
                            pic = Global.carsResult.getCarsNew().getCarPictureList().get(0).getPictureLink().toString();
                        }
                        rent = Global.carsResult.getCarsNew().getWeeklyRent();
                        totalAmount = Global.carsResult.getCarsNew().getTotalAmount();
                        sa.addEntry(new ListData(name,number,bookedDate,expiryDate,rent,totalAmount,pic));
                    }
                        //   Toast.makeText(MainActivity.this, carsCity, Toast.LENGTH_LONG).show();
                    }
                    else {

                        noCars.setVisibility(View.VISIBLE);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, function + "__" + "1");
        asyncTask.execute();


      /*  lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String ItemcarId= String.valueOf(sa.getItem(i).carId);
                Global.setItemCarId(ItemcarId);

                backScreen.setVisibility(View.VISIBLE);
                open.setVisibility(View.VISIBLE);


            }
        });*/



    }

    private static class ViewHolder {

        private View view;

        private TextView carName;
        private TextView carNumber;
        private TextView bookedDate;
        private TextView expiryDate;
        private TextView rent;
        private TextView totalAmount;
        private ImageView pic;

        private ViewHolder(View view) {
            this.view = view;
            carName= view.findViewById(R.id.details);
            carNumber = view.findViewById(R.id.carNumber);
            bookedDate = view.findViewById(R.id.bookedDate);
            expiryDate = view.findViewById(R.id.expiryDate);
            rent = view.findViewById(R.id.weeklyRent);
            totalAmount = view.findViewById(R.id.totalAmount);
            pic = view.findViewById(R.id.carImg);

        }

    }



    private class SampleAdapter extends BaseAdapter {
        SampleAdapter() {
            main = new ArrayList<>();
            // LeedsArray= new ArrayList<>();
        }


        public void addEntry(ListData l) {
            main.add(l);
            notifyDataSetChanged();
        }
        public void addEntryLeeds(HireitCarsNew l) {
            //  LeedsArray.add(l);
            notifyDataSetChanged();
        }

        public void clear() {
            main.clear();
            notifyDataSetChanged();
        }



        @Override
        public int getCount() {
            return main.size();
        }

        @Override
        public ListData getItem(int position) {
            return main.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = View.inflate(getActivity(), R.layout.lisitem_bookedcars, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            ListData item = getItem(position);
            DisplayMetrics dm = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);

            int width = dm.widthPixels;
            int height = dm.heightPixels;

            holder.carName.setText(item.carName);
            holder.carNumber.setText(item.carNumber);
            holder.expiryDate.setText(item.expiryDate);
            holder.bookedDate.setText(item.bookedDate);
            holder.rent.setText("£"+item.weeeklyRent);
            holder.totalAmount.setText("£"+item.totalAmount);

            Glide.with(getActivity())
                    .load(item.pic)
                    .centerCrop()
                    .placeholder(R.drawable.pugnotification_ic_placeholder)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {

                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {


                            return false;
                        }
                    })
                    .into(holder.pic);

            return convertView;

        }
    }



    private class ListData {
        private String carName;
        private String carNumber;
        private String bookedDate;
        private String expiryDate;
        private String weeeklyRent;
        private String totalAmount;
        private String pic;
        //["Sehat Sahara Plus", "2", "2", "10", "16", "7\/19\/2017 12:00:00 AM", "7\/19\/2018 12:00:00 AM"]
        public ListData(String carName,String carNumber,String bookedDate,String expiryDate,String weeeklyRent,String totalAmount,String pic) {
            this.carName = carName;
            this.carNumber = carNumber;
            this.bookedDate = bookedDate;
            this.expiryDate = expiryDate;
            this.weeeklyRent = weeeklyRent;
            this.totalAmount = totalAmount;
            this.pic = pic;


        }
    }



}

