package hireit.hireit.drawer.DrawerFragments;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.ToxicBakery.viewpager.transforms.RotateUpTransformer;
import com.bumptech.glide.Glide;
import com.tmall.ultraviewpager.UltraViewPager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import belka.us.androidtoggleswitch.widgets.ToggleSwitch;
import cn.pedant.SweetAlert.SweetAlertDialog;
import hireit.hireit.AsyncResponse;
import hireit.hireit.GetProfile;
import hireit.hireit.Global;
import hireit.hireit.Models.HireitLoginResponse;
import hireit.hireit.R;
import hireit.hireit.drawer.MainActivity;

import static hireit.hireit.R.id.view;

public class CarDetailsActivity extends AppCompatActivity {

    private ViewPager mViewPager;


    //private ShadowTransformer mCardShadowTransformer;
    ArrayList<ListData> main;
    ArrayList<ListData_> main_;

    View v;
    View HEALTH_CARDS, LIST_VIEW;

    private static final String KEY_MOVIE_TITLE = "key_title";
    private static final int ANIM_DURATION_TOOLBAR = 750;

    private String[] ts = {"Pending", "All", "Expired"};

    ToggleSwitch toggleSwitch;
    Button bookNow;


    SampleAdapter sa;
    SampleAdapter_ sa_;
    ListView lv;
    Toolbar toolbar;
    int height;
    private String carName;
    private String carModel;
    private String carColor;
    private String carPower;
    private String carPrice;
    private String carPic;


    @Override
    public void onBackPressed() {
        Intent goToCar = new Intent(this, MainActivity.class);
        startActivity(goToCar);

    }

    TextView carsName;
    TextView model;
    TextView power;
    TextView price;
    TextView color;


    static CarDetailsActivity cars = new CarDetailsActivity();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_car_details);

//        mViewPager = (ViewPager) findViewById(R.id.view);


        Global.setBooking(false);
        sa = new SampleAdapter();


        UltraViewPager ultraViewPager = (UltraViewPager) findViewById(view);

//initialize UltraPagerAdapter，and add child view to UltraViewPager

        sa_ = new SampleAdapter_();
        ultraViewPager.setAdapter(sa_);


        ultraViewPager.setPageTransformer(true, new RotateUpTransformer());
        ultraViewPager.setScrollMode(UltraViewPager.ScrollMode.HORIZONTAL);
        ultraViewPager.setClipToPadding(false);

//initialize built-in indicator
        ultraViewPager.initIndicator();
//set style of indicators
        ultraViewPager.getIndicator()
                .setOrientation(UltraViewPager.Orientation.HORIZONTAL)
                .setFocusColor(Color.GREEN)
                .setNormalColor(Color.WHITE)
                .setRadius((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getResources().getDisplayMetrics()));
//set the alignment
        ultraViewPager.getIndicator().setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
//construct built-in indicator, and add it to  UltraViewPager
        ultraViewPager.getIndicator().build();

//set an infinite loop
        ultraViewPager.setInfiniteLoop(true);
//enable auto-scroll mode
        ultraViewPager.setAutoScroll(2000);


        Resources a = getResources();


            try {

                int index = Global.getArrayIndex();
                if (getIntent().getStringExtra("carsCity").equals("Leeds")) {

                    if (Global.getLeedsArray().get(Global.getArrayIndex()).getCarPictureList().size() > 0)
                        System.out.println("siize: " + Global.getLeedsArray().get(Global.getArrayIndex()).getCarPictureList().size());
                    for (int i = 0; i < Global.getLeedsArray().get(Global.getArrayIndex()).getCarPictureList().size(); i++) {
                        String p1 = Global.getLeedsArray().get(Global.getArrayIndex()).getCarPictureList().get(i).getPictureLink().toString();

                        System.out.println("heere " + i);
                        System.out.println("Liink " + Global.getLeedsArray().get(Global.getArrayIndex()).getCarPictureList().get(i).getPictureLink().toString());

                        sa_.addEntry(new ListData_(p1));
                        sa_.notifyDataSetChanged();
                        ultraViewPager.refresh();

                    }


                    Global.setLeeds(true);
                    Global.setKirklees(false);
                    Global.setBradFord(false);
                    Global.setWakefield(false);
                    carName = Global.getLeedsArray().get(index).getCarName();
                    carModel = Global.getLeedsArray().get(index).getModel();
                    carColor = Global.getLeedsArray().get(index).getColor();
                    carPower = Global.getLeedsArray().get(index).getEnginePower();
                    carPrice = Global.getLeedsArray().get(index).getCarPriceContract().getFinalPrice();

                } else if (getIntent().getStringExtra("carsCity").equals("Kirklees")) {

                    if (Global.getKirkleesArray().get(Global.getArrayIndex()).getCarPictureList().size() > 0)
                        System.out.println("siize: " + Global.getKirkleesArray().get(Global.getArrayIndex()).getCarPictureList().size());
                    for (int i = 0; i < Global.getKirkleesArray().get(Global.getArrayIndex()).getCarPictureList().size(); i++) {
                        String p1 = Global.getKirkleesArray().get(Global.getArrayIndex()).getCarPictureList().get(i).getPictureLink().toString();


                        sa_.addEntry(new ListData_(p1));
                        sa_.notifyDataSetChanged();
                        ultraViewPager.refresh();

                    }


                    Global.setKirklees(true);
                    Global.setLeeds(false);
                    Global.setBradFord(false);
                    Global.setWakefield(false);
                    carName = Global.getKirkleesArray().get(index).getCarName();
                    carModel = Global.getKirkleesArray().get(index).getModel();
                    carColor = Global.getKirkleesArray().get(index).getColor();
                    carPower = Global.getKirkleesArray().get(index).getEnginePower();
                    carPrice = Global.getKirkleesArray().get(index).getCarPriceContract().getFinalPrice();

                } else if (getIntent().getStringExtra("carsCity").equals("Wakefield")) {

                    if (Global.getWakefieldArray().get(Global.getArrayIndex()).getCarPictureList().size() > 0)
                    for (int i = 0; i < Global.getWakefieldArray().get(Global.getArrayIndex()).getCarPictureList().size(); i++) {
                        String p1 = Global.getWakefieldArray().get(Global.getArrayIndex()).getCarPictureList().get(i).getPictureLink().toString();

                        sa_.addEntry(new ListData_(p1));
                        sa_.notifyDataSetChanged();
                        ultraViewPager.refresh();

                    }


                    Global.setWakefield(true);
                    Global.setLeeds(false);
                    Global.setKirklees(false);
                    Global.setBradFord(false);

                    carName = Global.getWakefieldArray().get(index).getCarName();
                    carModel = Global.getWakefieldArray().get(index).getModel();
                    carColor = Global.getWakefieldArray().get(index).getColor();
                    carPower = Global.getWakefieldArray().get(index).getEnginePower();
                    carPrice = Global.getWakefieldArray().get(index).getCarPriceContract().getFinalPrice();

                } else if (getIntent().getStringExtra("carsCity").equals("BradFord")) {

                    if (Global.getBradfordArray().get(Global.getArrayIndex()).getCarPictureList().size() > 0)
                    for (int i = 0; i < Global.getBradfordArray().get(Global.getArrayIndex()).getCarPictureList().size(); i++) {
                        String p1 = Global.getBradfordArray().get(Global.getArrayIndex()).getCarPictureList().get(i).getPictureLink().toString();

                        sa_.addEntry(new ListData_(p1));
                        sa_.notifyDataSetChanged();
                        ultraViewPager.refresh();

                    }


                    Global.setBradFord(true);
                    Global.setLeeds(false);
                    Global.setKirklees(false);
                    Global.setWakefield(false);

                    carName = Global.getBradfordArray().get(index).getCarName();
                    carModel = Global.getBradfordArray().get(index).getModel();
                    carColor = Global.getBradfordArray().get(index).getColor();
                    carPower = Global.getBradfordArray().get(index).getEnginePower();
                    carPrice = Global.getBradfordArray().get(index).getCarPriceContract().getFinalPrice();

                }

            } catch (ArrayIndexOutOfBoundsException e) {
                // Log exception
                e.printStackTrace();
            }


        carsName = (TextView) findViewById(R.id.carName);
        model = (TextView) findViewById(R.id.model);
        power = (TextView) findViewById(R.id.power);
        price = (TextView) findViewById(R.id.price);
        color = (TextView) findViewById(R.id.color);


        carsName.setText(carName);
        model.setText(carModel);
        power.setText(carPower);
        price.setText("£"+carPrice+"/week");
        color.setText(carColor);

        sa.addEntry(new ListData(carName, carPrice, carColor, carPower, carModel));

        bookNow = (Button) findViewById(R.id.bookNow);
        bookNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String function = "CheckDocuments?email=" + Global.getNumber() + "@hire-it.co";
                GetProfile asyncTask = new GetProfile(CarDetailsActivity.this, new AsyncResponse() {

                    @Override
                    public void processFinish(Object result) throws JSONException {
                        String ans = result + "";
                        JSONObject jsonObj = null;

                        try {
                            jsonObj = new JSONObject(ans);

                            Global.checkDocuments = new HireitLoginResponse(jsonObj);
                            String result1 = Global.checkDocuments.getResult().toString();
                            if (result1.equals("Documents Present")) {
                              //  Toast.makeText(CarDetailsActivity.this, result1.toString(), Toast.LENGTH_LONG).show();
                                Intent goToWeeks= new Intent(CarDetailsActivity.this,WeeksActivity.class);
                                startActivity(goToWeeks);
                            } else {
                                new SweetAlertDialog(CarDetailsActivity.this, SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText("Documents unavailable")
                                        .setContentText("Please upload your documents")
                                        .setConfirmText("Ok")
                                        .setCancelText("Cancel")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                Global.setUploadPhotos(true);
                                                Global.setBooking(true);
                                                Intent goMain = new Intent(CarDetailsActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                startActivity(goMain);

                                            }
                                        })
                                        .show();


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, function + "__" + "1");
                asyncTask.execute();

            }


            // sendVarificationCode();

                /*Intent sendCode= new Intent(getActivity(),SignUpActivity.class).putExtra("Fragments","Confirm Code");
                startActivity(sendCode);

*/
//                new SweetAlertDialog(getActivity(),SweetAlertDialog.PROGRESS_TYPE)
//                        .setTitleText("Please Wait")
//                        .show();
//


        });


    }


    private class SampleAdapter_ extends PagerAdapter {
        SampleAdapter_() {
            main_ = new ArrayList<>();
            // LeedsArray= new ArrayList<>();
        }

        @Override
        public int getCount() {
            return main_.size();
        }


        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            return;
        }

        public void addEntry(ListData_ l) {
            main_.add(l);

        }

        public void clear() {
            main_.clear();

        }



        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {

            return view== ((View)object);


        }


        public ListData_ getItem(int position) {
            return main_.get(position);
        }


        public long getItemId(int position) {
            return position;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = LayoutInflater.from(container.getContext())
                    .inflate(R.layout.car_details_item, container, false);



            ImageView carImg= view.findViewById(R.id.pic);

            ListData_ item = getItem(position);

            DisplayMetrics dm = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(dm);

            int width = dm.widthPixels;
            int height = dm.heightPixels;
            container.addView(view);


            Glide.with(CarDetailsActivity.this)
                    .load(item.carImg)
                    .centerCrop()
                    .placeholder(R.drawable.pugnotification_ic_placeholder)
                    .into(carImg);


             return view;

         /*    Bitmap bt= getBitmapFromURL(picLink);
            holder.carImg.setImageBitmap(getBitmapFromURL(bt.toString()));
*/




        }
    }



    private class ListData_ {

        private String carImg;

        //["Sehat Sahara Plus", "2", "2", "10", "16", "7\/19\/2017 12:00:00 AM", "7\/19\/2018 12:00:00 AM"]
        public ListData_(String carImg) {
            this.carImg = carImg;




        }
    }



    private class SampleAdapter extends BaseAdapter {

        SampleAdapter() {
            main = new ArrayList<>();
        }

        public void addEntry(CarDetailsActivity.ListData l) {
            main.add(l);
            notifyDataSetChanged();
        }


        public void clear() {
            main.clear();
            notifyDataSetChanged();
        }



        @Override
        public int getCount() {
            return main.size();
        }

        @Override
        public ListData getItem(int position) {
            return main.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
               /* if (position == main.size() - 1)
                    convertView = View.inflate(getContext(), R.layout.___list_item_kitchenlast, null);
                else*/
                convertView = View.inflate(getApplicationContext(), R.layout.car_details_item, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            try {
                ListData item = getItem(position);





            } catch (IndexOutOfBoundsException e) {
            }

            return convertView;
        }
    }

    private static class ViewHolder {
        private View view;

        private TextView carsName;
        private TextView color;
        private TextView model;
        private TextView power;
        private TextView price;


        private ViewHolder(View view) {
            this.view = view;
            this.carsName= view.findViewById(R.id.carName);
            this.model=view.findViewById(R.id.model);
            this.power=view.findViewById(R.id.power);
            this.price=view.findViewById(R.id.price);


        }
    }

    private class ListData {
        private String carName;
        private String carPrice;
        private String carEnginePower;
        private String carImg;
        private String carModel;
        private String carColor;

        //["Sehat Sahara Plus", "2", "2", "10", "16", "7\/19\/2017 12:00:00 AM", "7\/19\/2018 12:00:00 AM"]
        public ListData(String carName, String carPrice, String carColor, String carEnginePower, String carModel) {
            this.carName = carName;
            this.carPrice = carPrice;
            this.carColor = carColor;
            this.carEnginePower = carEnginePower;
            this.carModel = carModel;

        }
    }
}
