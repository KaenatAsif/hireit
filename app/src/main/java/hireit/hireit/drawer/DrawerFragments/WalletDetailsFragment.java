package hireit.hireit.drawer.DrawerFragments;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import hireit.hireit.AsyncResponse;
import hireit.hireit.GetProfile;
import hireit.hireit.Global;
import hireit.hireit.Models.HireitCarsNew;
import hireit.hireit.Models.HireitLedgerDetailResponse;
import hireit.hireit.R;

public class WalletDetailsFragment extends Fragment {

    private static final String KEY_MOVIE_TITLE = "key_title";
    private String picLink;

    private Uri picUrl;

    private String carsName;
    private String price;
    private String remaining;
    TextView carName;



    private String Id;
    private String Note;
    private String Paid;
    private String PaymentDate;
    private String Remaining;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_wallet_details, container, false);
    }

    ArrayList<ListData> main;
    JSONArray jarr;
    SampleAdapter sa;
    String[] date;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        carName=(TextView) view.findViewById(R.id.carName);

        main = new ArrayList<>();
        sa = new SampleAdapter();

        final ListView lv = view.findViewById(R.id.listWalletDetails);
        lv.setAdapter(sa);


        String function="CustomerLedgerDetials?email="+Global.getNumber()+"@hire-it.co&carid="+Global.getItemCarId();
        GetProfile asyncTask= new GetProfile(getActivity(), new AsyncResponse() {
            @Override
            public void processFinish(Object result) {

                String ans = result + "";
                JSONObject jsonObj = null;

                try {
                    jsonObj = new JSONObject(ans);
                    Global.ledgerDetailResponse= new HireitLedgerDetailResponse(jsonObj);
                    String Name= Global.ledgerDetailResponse.getCustomerLedgerDetialsResult().get(0).getCarName();

                    carName.setText(Name);
                    for (int i = 0; i<Global.ledgerDetailResponse.getCustomerLedgerDetialsResult().size();i++){
                         Id=Global.ledgerDetailResponse.getCustomerLedgerDetialsResult().get(i).getId();
                        Note=Global.ledgerDetailResponse.getCustomerLedgerDetialsResult().get(i).getNote();
                        Remaining= String.valueOf(Global.ledgerDetailResponse.getCustomerLedgerDetialsResult().get(i).getRemaining());
                        Paid=Global.ledgerDetailResponse.getCustomerLedgerDetialsResult().get(i).getPaid();
                        String PaymentDate1=Global.ledgerDetailResponse.getCustomerLedgerDetialsResult().get(i).getPaymentDate();
                        date= PaymentDate1.split(" ");
                        PaymentDate=date[0];


                        sa.addEntry(new ListData(Note,Paid,Remaining,PaymentDate));

                    }

                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        },function+"__"+"1");
        asyncTask.execute();

    }

    private static class ViewHolder {

        private View view;

        private TextView Paid;
        private TextView PaymentDate;
        private TextView Remaining;
        private TextView Note;


        private ViewHolder(View view) {
            this.view = view;
            Note= view.findViewById(R.id.note);
            Paid = view.findViewById(R.id.paid);
            Remaining=view.findViewById(R.id.remaining);
            PaymentDate=view.findViewById(R.id.date);

        }
    }

    private class SampleAdapter extends BaseAdapter {
        SampleAdapter() {
            main = new ArrayList<>();
            // LeedsArray= new ArrayList<>();
        }

        public void addEntry(ListData l) {
            main.add(l);
            notifyDataSetChanged();
        }
        public void addEntryLeeds(HireitCarsNew l) {
            //  LeedsArray.add(l);
            notifyDataSetChanged();
        }

        public void clear() {
            main.clear();
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return main.size();
        }

        @Override
        public ListData getItem(int position) {
            return main.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = View.inflate(getActivity(), R.layout.listitem_wallet_details, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            ListData item = getItem(position);
            DisplayMetrics dm = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);

            int width = dm.widthPixels;
            int height = dm.heightPixels;

            holder.Note.setText(item.note);
            holder.Paid.setText(item.paid);
            holder.Remaining.setText(item.remaining);


            holder.PaymentDate.setText(item.date);

            return convertView;

        }
    }

    private class ListData {
        private String note;
        private String paid;
        private String remaining;
        private String date;

        public ListData(String note,String paid,String remaining,String date) {
            this.note = note;
            this.paid = paid;
            this.remaining = remaining;
            this.date = date;

        }
    }

}