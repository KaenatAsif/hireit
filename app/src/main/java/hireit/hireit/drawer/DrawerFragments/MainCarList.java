package hireit.hireit.drawer.DrawerFragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.baoyz.widget.PullRefreshLayout;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import hireit.hireit.AsyncResponse;
import hireit.hireit.GetSingle;
import hireit.hireit.Global;
import hireit.hireit.Models.HireitCarsNew;
import hireit.hireit.Models.HireitCarsResult;
import hireit.hireit.R;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class MainCarList extends Fragment {

    private static final String KEY_MOVIE_TITLE = "key_title";
    private String picLink;

    private Uri picUrl;
    private Date currentDate;

    private String carsName;
    private String carsCity;
    private String carsColor;
    private String carsEnginePower;
    private String carsModel ;
    private String carsStatus ;
    private String carsPrice ;
    private String carsMake;
    private String remainingDays;
    private String discountPercentage;
    private String discountAmount;
    private String finalPrice;
    private String carNumber;
    private String discountPrice;

    private Date dateTo;
    private Date dateFrom;
    LinearLayout tabLayout;
    ViewPager viewPager;
    ImageView tab_leeds;
    ImageView tab_kirklees;
    ImageView tab_bradford;
    ImageView tab_wakefield;
    ImageView carImg;
    LinearLayout discountLayout;
    LinearLayout noCars;

int button=1;
    ArrayList<ListData> main;
    JSONArray jarr;
    SampleAdapter sa;
    ArrayList<HireitCarsNew> LeedsArray;
    ArrayList<HireitCarsNew> BradfordArray;
    ArrayList<HireitCarsNew> KirkleesArray;
    ArrayList<HireitCarsNew> WakefieldArray;
    SimpleDateFormat simpleDateFormat;

    public MainCarList() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     *
     * @return A new instance of fragment _3_Fragment_1_EmptyReports.
     */
    public static MainCarList newInstance(String movieTitle) {
        MainCarList MainCarList = new MainCarList();
        Bundle args = new Bundle();
        args.putString(KEY_MOVIE_TITLE, movieTitle);
        try {
            MainCarList.setArguments(args);
        } catch (IllegalStateException e) {
        }

        return MainCarList;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_car_list, container,
                false);




        return v;
    }




    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        tabLayout = (LinearLayout) view.findViewById(R.id.tab_layout);

        LeedsArray = new ArrayList<>();
        WakefieldArray = new ArrayList<>();
        KirkleesArray = new ArrayList<>();
        BradfordArray = new ArrayList<>();
        simpleDateFormat = new SimpleDateFormat("M/dd/yyyy");


        main = new ArrayList<>();
        sa = new SampleAdapter();

        final ListView lv = view.findViewById(R.id.list);
        lv.setAdapter(sa);

        final PullRefreshLayout pullToRefresh = view.findViewById(R.id.pullToRefresh);
        pullToRefresh.setRefreshStyle(PullRefreshLayout.STYLE_CIRCLES);


        pullToRefresh.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                Global.getLeedsArray().clear();
                Global.getKirkleesArray().clear();
                Global.getWakefieldArray().clear();
                Global.getBradfordArray().clear();



                String function = "cars";
                GetSingle asyncTask = new GetSingle(getActivity(), new AsyncResponse() {
                    @Override
                    public void processFinish(Object result) {

                        String ans = result + "";
                        JSONObject jsonObj = null;
                        JSONArray caresult = null;
                        JSONObject details;
                        JSONObject details1;

                        try {
                            jsonObj = new JSONObject(ans);

                            caresult = jsonObj.getJSONArray("CarsResult");



                            for (int i = 0; i < caresult.length(); i++) {
                                details = caresult.getJSONObject(i);

                                details1 = details.getJSONObject("carsNew");
                                Global.carsResult = new HireitCarsResult(details);

                                Global.carsNew = new HireitCarsNew(details1);

                                carsName = Global.carsResult.getCarsNew().getCarName();
                                carsCity = Global.carsResult.getCarsNew().getCarCity();

                                if (carsCity.equals("Leeds"))
                                    LeedsArray.add(Global.carsNew);
                                Global.setLeedsArray(LeedsArray);

                                if (carsCity.equals("BradFord")) {
                                    BradfordArray.add(Global.carsNew);
                                    Global.setBradfordArray(BradfordArray);
                                }
                                if (carsCity.equals("Kirklees")) {
                                    KirkleesArray.add(Global.carsNew);
                                    Global.setKirkleesArray(KirkleesArray);
                                }
                                if (carsCity.equals("Wakefield")) {
                                    WakefieldArray.add(Global.carsNew);
                                    Global.setWakefieldArray(WakefieldArray);


                                }

                            }
                            performclickofbutton();

                            pullToRefresh.setRefreshing(false);

                            //sa.clear();
                      //  sa.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, function + "__" + "1");
                asyncTask.execute();

               // onViewCreated(view,savedInstanceState);


            }



        });



        tab_leeds=(ImageView) view.findViewById(R.id.tab_leeds);
        tab_kirklees=(ImageView) view.findViewById(R.id.tab_kirklees);
        tab_wakefield=(ImageView) view.findViewById(R.id.tab_wakefield);
        tab_bradford=(ImageView) view.findViewById(R.id.tab_bradford);

        noCars=(LinearLayout) view.findViewById(R.id.noCars);


        tab_leeds.setBackground(view.getResources().getDrawable(R.drawable.round_filled));
        tab_kirklees.setBackground(view.getResources().getDrawable(R.drawable.round));
        tab_bradford.setBackground(view.getResources().getDrawable(R.drawable.round));
        tab_wakefield.setBackground(view.getResources().getDrawable(R.drawable.round));







        tab_leeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                button=1;
                sa.clear();
                tab_leeds.setBackground(view.getResources().getDrawable(R.drawable.round_filled));
                tab_kirklees.setBackground(getResources().getDrawable(R.drawable.round));
                tab_bradford.setBackground(getResources().getDrawable(R.drawable.round));
                tab_wakefield.setBackground(getResources().getDrawable(R.drawable.round));
                if (Global.getLeedsArray().size()>0) {
                    noCars.setVisibility(View.GONE);
                    for (int i = 0; i < Global.getLeedsArray().size(); i++) {
                        if (Global.getLeedsArray().get(i).getCarPriceContract().getFinalPrice() != null)
                            carsPrice = Global.getLeedsArray().get(i).getCarPriceContract().getFinalPrice();
                        if (Global.getLeedsArray().get(i).getColor() != null)
                            carsColor = Global.getLeedsArray().get(i).getColor();
                        if (Global.getLeedsArray().get(i).getEnginePower() != null)
                            carsEnginePower = Global.getLeedsArray().get(i).getEnginePower();
                        if (Global.getLeedsArray().get(i).getModel() != null)
                            carsModel = Global.getLeedsArray().get(i).getModel();
                        if (Global.getLeedsArray().get(i).getCarName() != null)
                            carsName = Global.getLeedsArray().get(i).getCarName();
                        if (Global.getLeedsArray().get(i).getCarCity() != null)
                            carsCity = Global.getLeedsArray().get(i).getCarCity();
                        if (Global.getLeedsArray().get(i).getRegistrationNumber() != null)
                            carNumber = Global.getLeedsArray().get(i).getRegistrationNumber();
                        if (Global.getLeedsArray().get(i).getCarPriceContract().getAmountOfDiscunt() != null)
                            discountPrice = Global.getLeedsArray().get(i).getCarPriceContract().getAmountOfDiscunt();



                        if (Global.getLeedsArray().get(i).getCarPriceContract().getDateTo() != null)
                            try {
                                dateTo= simpleDateFormat.parse(Global.getLeedsArray().get(i).getCarPriceContract().getDateTo());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        if (Global.getLeedsArray().get(i).getCarPriceContract().getDateFrom() != null)
                            try {
                                dateFrom = simpleDateFormat.parse(Global.getLeedsArray().get(i).getCarPriceContract().getDateFrom());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        if (Global.getLeedsArray().get(i).getCarPriceContract().getDiscountPercentage() != null)
                            discountPercentage = Global.getLeedsArray().get(i).getCarPriceContract().getDiscountPercentage();
                        if (Global.getLeedsArray().get(i).getCarPriceContract().getFinalPrice() != null)
                            finalPrice = Global.getLeedsArray().get(i).getCarPriceContract().getFinalPrice();

                        if (Global.getLeedsArray().get(i).getCarPictureList().size() > 0) {

                            picLink = Global.getLeedsArray().get(i).getCarPictureList().get(0).getPictureLink();
                        }
                        sa.addEntry(new ListData(carsCity, carsName, carsPrice, carNumber,picLink, carsColor, carsEnginePower, carsModel,dateTo,dateFrom,finalPrice,discountPercentage,discountPrice));
                        sa.notifyDataSetChanged();
                    }
                }
                else{
                    noCars.setVisibility(View.VISIBLE);
                }
            }
        });

        tab_kirklees.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                button=2;
                tab_kirklees.setBackground(view.getResources().getDrawable(R.drawable.round_filled));
                tab_leeds.setBackground(getResources().getDrawable(R.drawable.round));
                tab_bradford.setBackground(getResources().getDrawable(R.drawable.round));
                tab_wakefield.setBackground(getResources().getDrawable(R.drawable.round));
                sa.clear();
                if (Global.getKirkleesArray().size()>0) {
                    noCars.setVisibility(View.GONE);

                    for (int i = 0; i < Global.getKirkleesArray().size(); i++) {

                        if (Global.getKirkleesArray().get(i).getCarPriceContract().getFinalPrice() != null)
                            carsPrice = Global.getKirkleesArray().get(i).getCarPriceContract().getFinalPrice();
                        if (Global.getKirkleesArray().get(i).getColor() != null)
                            carsColor = Global.getKirkleesArray().get(i).getColor();
                        if (Global.getKirkleesArray().get(i).getEnginePower() != null)
                            carsEnginePower = Global.getKirkleesArray().get(i).getEnginePower();
                        if (Global.getKirkleesArray().get(i).getModel() != null)
                            carsModel = Global.getKirkleesArray().get(i).getModel();
                        if (Global.getKirkleesArray().get(i).getCarName() != null)
                            carsName = Global.getKirkleesArray().get(i).getCarName();
                        if (Global.getKirkleesArray().get(i).getCarCity() != null)
                            carsCity = Global.getKirkleesArray().get(i).getCarCity();
                        if (Global.getKirkleesArray().get(i).getRegistrationNumber() != null)
                            carNumber = Global.getKirkleesArray().get(i).getRegistrationNumber();
                        if (Global.getKirkleesArray().get(i).getCarPriceContract().getAmountOfDiscunt() != null)
                            discountPrice = Global.getKirkleesArray().get(i).getCarPriceContract().getAmountOfDiscunt();



                        if (Global.getKirkleesArray().get(i).getCarPriceContract().getDateTo() != null)
                            try {
                                dateTo= simpleDateFormat.parse(Global.getKirkleesArray().get(i).getCarPriceContract().getDateTo());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        if (Global.getKirkleesArray().get(i).getCarPriceContract().getDateFrom() != null)
                            try {
                                dateFrom = simpleDateFormat.parse(Global.getKirkleesArray().get(i).getCarPriceContract().getDateFrom());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        if (Global.getKirkleesArray().get(i).getCarPriceContract().getDiscountPercentage() != null)
                            discountPercentage = Global.getKirkleesArray().get(i).getCarPriceContract().getDiscountPercentage();
                        if (Global.getKirkleesArray().get(i).getCarPriceContract().getFinalPrice() != null)
                            finalPrice = Global.getKirkleesArray().get(i).getCarPriceContract().getFinalPrice();

                        if (Global.getKirkleesArray().get(i).getCarPictureList().size() > 0) {

                            picLink = Global.getKirkleesArray().get(i).getCarPictureList().get(0).getPictureLink();
                        }
                        sa.addEntry(new ListData(carsCity, carsName, carsPrice,carNumber, picLink, carsColor, carsEnginePower, carsModel,dateTo,dateFrom,finalPrice,discountPercentage,discountPrice));
                    }

                }
                else{
                    noCars.setVisibility(View.VISIBLE);
                }
            }
        });

        tab_wakefield.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                button=3;

                tab_wakefield.setBackground(view.getResources().getDrawable(R.drawable.round_filled));
                tab_kirklees.setBackground(getResources().getDrawable(R.drawable.round));
                tab_bradford.setBackground(getResources().getDrawable(R.drawable.round));
                tab_leeds.setBackground(getResources().getDrawable(R.drawable.round));

                sa.clear();
                if (Global.getWakefieldArray().size()>0) {
                    noCars.setVisibility(View.GONE);

                    for (int i = 0; i < Global.getWakefieldArray().size(); i++) {

                        if (Global.getWakefieldArray().get(i).getCarPriceContract().getFinalPrice() != null)
                            carsPrice = Global.getWakefieldArray().get(i).getCarPriceContract().getFinalPrice();
                        if (Global.getWakefieldArray().get(i).getColor() != null)
                            carsColor = Global.getWakefieldArray().get(i).getColor();
                        if (Global.getWakefieldArray().get(i).getEnginePower() != null)
                            carsEnginePower = Global.getWakefieldArray().get(i).getEnginePower();
                        if (Global.getWakefieldArray().get(i).getModel() != null)
                            carsModel = Global.getWakefieldArray().get(i).getModel();
                        if (Global.getWakefieldArray().get(i).getCarName() != null)
                            carsName = Global.getWakefieldArray().get(i).getCarName();
                        if (Global.getWakefieldArray().get(i).getCarCity() != null)
                            carsCity = Global.getWakefieldArray().get(i).getCarCity();
                        if (Global.getWakefieldArray().get(i).getRegistrationNumber() != null)
                            carNumber = Global.getWakefieldArray().get(i).getRegistrationNumber();
                        if (Global.getWakefieldArray().get(i).getCarPriceContract().getAmountOfDiscunt() != null)
                            discountPrice = Global.getWakefieldArray().get(i).getCarPriceContract().getAmountOfDiscunt();



                        if (Global.getWakefieldArray().get(i).getCarPriceContract().getDateTo() != null)
                            try {
                                dateTo= simpleDateFormat.parse(Global.getWakefieldArray().get(i).getCarPriceContract().getDateTo());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        if (Global.getWakefieldArray().get(i).getCarPriceContract().getDateFrom() != null)
                            try {
                                dateFrom = simpleDateFormat.parse(Global.getWakefieldArray().get(i).getCarPriceContract().getDateFrom());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        if (Global.getWakefieldArray().get(i).getCarPriceContract().getDiscountPercentage() != null)
                            discountPercentage = Global.getWakefieldArray().get(i).getCarPriceContract().getDiscountPercentage();
                        if (Global.getWakefieldArray().get(i).getCarPriceContract().getFinalPrice() != null)
                            finalPrice = Global.getWakefieldArray().get(i).getCarPriceContract().getFinalPrice();

                        if (Global.getWakefieldArray().get(i).getCarPictureList().size() > 0) {

                            picLink = Global.getWakefieldArray().get(i).getCarPictureList().get(0).getPictureLink();
                        }
                        sa.addEntry(new ListData(carsCity, carsName, carsPrice,carNumber, picLink, carsColor, carsEnginePower, carsModel,dateTo,dateFrom,finalPrice,discountPercentage,discountPrice));
                        sa.notifyDataSetChanged();
                    }
                }
                else{
                    noCars.setVisibility(View.VISIBLE);
                }
            }
        });

        tab_bradford.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                button=4;
                tab_bradford.setBackground(view.getResources().getDrawable(R.drawable.round_filled));
                tab_kirklees.setBackground(getResources().getDrawable(R.drawable.round));
                tab_leeds.setBackground(getResources().getDrawable(R.drawable.round));
                tab_wakefield.setBackground(getResources().getDrawable(R.drawable.round));

                sa.clear();
                if (Global.getBradfordArray().size()>0) {
                    noCars.setVisibility(View.GONE);

                    for (int i = 0; i < Global.getBradfordArray().size(); i++) {

                        if (Global.getBradfordArray().get(i).getCarPriceContract().getFinalPrice() != null)
                            carsPrice = Global.getBradfordArray().get(i).getCarPriceContract().getFinalPrice();
                        if (Global.getBradfordArray().get(i).getColor() != null)
                            carsColor = Global.getBradfordArray().get(i).getColor();
                        if (Global.getBradfordArray().get(i).getEnginePower() != null)
                            carsEnginePower = Global.getBradfordArray().get(i).getEnginePower();
                        if (Global.getBradfordArray().get(i).getModel() != null)
                            carsModel = Global.getBradfordArray().get(i).getModel();
                        if (Global.getBradfordArray().get(i).getCarName() != null)
                            carsName = Global.getBradfordArray().get(i).getCarName();
                        if (Global.getBradfordArray().get(i).getCarCity() != null)
                            carsCity = Global.getBradfordArray().get(i).getCarCity();
                        if (Global.getBradfordArray().get(i).getRegistrationNumber() != null)
                            carNumber = Global.getBradfordArray().get(i).getRegistrationNumber();
                        if (Global.getBradfordArray().get(i).getCarPriceContract().getAmountOfDiscunt() != null)
                            discountPrice = Global.getBradfordArray().get(i).getCarPriceContract().getAmountOfDiscunt();



                        if (Global.getBradfordArray().get(i).getCarPriceContract().getDateTo() != null)
                            try {
                                dateTo= simpleDateFormat.parse(Global.getBradfordArray().get(i).getCarPriceContract().getDateTo());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        if (Global.getBradfordArray().get(i).getCarPriceContract().getDateFrom() != null)
                            try {
                                dateFrom = simpleDateFormat.parse(Global.getBradfordArray().get(i).getCarPriceContract().getDateFrom());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        if (Global.getBradfordArray().get(i).getCarPriceContract().getDiscountPercentage() != null)
                            discountPercentage = Global.getBradfordArray().get(i).getCarPriceContract().getDiscountPercentage();
                        if (Global.getBradfordArray().get(i).getCarPriceContract().getFinalPrice() != null)
                            finalPrice = Global.getBradfordArray().get(i).getCarPriceContract().getFinalPrice();

                        if (Global.getBradfordArray().get(i).getCarPictureList().size() > 0) {

                            picLink = Global.getBradfordArray().get(i).getCarPictureList().get(0).getPictureLink();
                        }
                        sa.addEntry(new ListData(carsCity, carsName, carsPrice,carNumber, picLink, carsColor, carsEnginePower, carsModel,dateTo,dateFrom,finalPrice,discountPercentage,discountPrice));
                    }
                }
                else{
                    noCars.setVisibility(View.VISIBLE);
                }
            }
        });


        tab_leeds.performClick();

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String carCity= sa.getItem(i).carCity;
                int index= i;
                Global.setArrayIndex(index);


                Intent detail = new Intent(getActivity(), CarDetailsActivity.class).putExtra("carsCity",carCity).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


                startActivity(detail);

            }
        });


    }
    public void performclickofbutton()
    {
        if(button==1)
        {
            tab_leeds.performClick();
        }
        else if(button==2)
        {
            tab_kirklees.performClick();
        }
        else if(button==3)
        {
            tab_wakefield.performClick();
        }
        else if(button==4)
        {
            tab_bradford.performClick();
        }
        else
        {
            tab_leeds.performClick();
        }
    }

    private static class ViewHolder {

        private View view;

        private TextView CarDetails;
        private ImageView carImg;
        private TextView carName;
        private TextView carPrice;
        private TextView carNumber;
        private TextView remainingDays;
        private TextView discount;
        private TextView finalPrice;

        private LinearLayout discountLayout;

        private ViewHolder(View view) {
            this.view = view;
            carName= view.findViewById(R.id.details);
            carPrice = view.findViewById(R.id.price);
            carImg=view.findViewById(R.id.carImg);
            remainingDays=view.findViewById(R.id.remainingDays);
            carNumber=view.findViewById(R.id.carNumber);
            discount=view.findViewById(R.id.discount);
            finalPrice=view.findViewById(R.id.finalPrice);
            discountLayout= view.findViewById(R.id.discountLayout);



        }

    }



    private class SampleAdapter extends BaseAdapter {
        SampleAdapter() {
            main = new ArrayList<>();
            LeedsArray= new ArrayList<>();
        }


        public void addEntry(ListData l) {
            main.add(l);
            notifyDataSetChanged();
        }
        public void addEntryLeeds(HireitCarsNew l) {
            LeedsArray.add(l);
            notifyDataSetChanged();
        }

        public void clear() {
            main.clear();
            notifyDataSetChanged();
        }



        @Override
        public int getCount() {
            return main.size();
        }

        @Override
        public ListData getItem(int position) {
            return main.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = View.inflate(getActivity(), R.layout.carlist_item, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            ListData item = getItem(position);
            DisplayMetrics dm = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);

            int width = dm.widthPixels;
            int height = dm.heightPixels;



            Date today = new Date();

            try {
                currentDate = simpleDateFormat.parse(simpleDateFormat.format(today));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            holder.discountLayout.setVisibility(View.GONE);
            int dateToNew = Integer.parseInt(printDifference(currentDate, item.dateTo));
            int dateFromNew= Integer.parseInt(printDifference(currentDate,item.dateFrom));
            int remainingDays= Integer.parseInt(printDifference(currentDate,item.dateTo));
            int carPriceInt= Integer.parseInt(item.carPrice);
            int discountPriceInt= Integer.parseInt(item.discountPrice);


            holder.carName.setText(item.carName);
            holder.carPrice.setText("£"+item.carPrice);
            holder.carNumber.setText(item.carNumber);

            Glide.with(getActivity())
                    .load(item.carImg)
                    .centerCrop()
                    .placeholder(R.drawable.pugnotification_ic_placeholder)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {

                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {


                            return false;
                        }
                    })
                    .into(holder.carImg);
         //   if daysforTo < 0 && daysforFrom > 0 || daysforTo == 0 && daysforFrom > 0

            if (dateFromNew<0&&dateToNew>0||dateFromNew==0&&dateToNew>0){
                holder.discountLayout.setVisibility(View.VISIBLE);
                holder.finalPrice.setText("£"+String.valueOf(carPriceInt-discountPriceInt));
                holder.discount.setText(item.discountPer+"%");
                holder.remainingDays.setText(String.valueOf(remainingDays));

            }

            return convertView;

        }
    }



    private class ListData {
        private String carName;
        private String carPrice;
        private String carNumber;
        private String carEnginePower;
        private String carImg;
        private String carModel;
        private String carColor;
        private String carCity;
        private String finalPrice;
        private String discountPer;
        private String discountPrice;

        private Date dateTo;
        private Date dateFrom;

        //["Sehat Sahara Plus", "2", "2", "10", "16", "7\/19\/2017 12:00:00 AM", "7\/19\/2018 12:00:00 AM"]
        public ListData(String carCity,String carName, String carPrice,String carNumber,String carImg,String carColor,String carEnginePower, String carModel,Date dateTo,Date dateFrom,String finalPrice,String discountPer,String discountPrice ) {
            this.carName = carName;
            this.carPrice = carPrice;
            this.carImg= carImg;
            this.carNumber= carNumber;
            this.carColor= carColor;
            this.carEnginePower= carEnginePower;
            this.carModel=carModel;
            this.carCity=carCity;
            this.dateTo=dateTo;
            this.dateFrom=dateFrom;
            this.finalPrice=finalPrice;
            this.discountPer=discountPer;
            this.discountPrice=discountPrice;


        }
    }


    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }
    public String printDifference(Date startDate, Date endDate) {

        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);

       /* if (elapsedDays > 0)
            return elapsedDays + "";
        else {
            elapsedDays = 90;
            return elapsedDays + "";
        }*/
       return elapsedDays+"" ;
    }


}

