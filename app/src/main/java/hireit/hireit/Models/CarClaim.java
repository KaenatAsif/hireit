
package hireit.hireit.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CarClaim {

    @SerializedName("CarName")
    @Expose
    private String carName;
    @SerializedName("RegistrationNumber")
    @Expose
    private String registrationNumber;
    @SerializedName("TaxiNumber")
    @Expose
    private String taxiNumber;
    @SerializedName("carid")
    @Expose
    private String carid;

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getTaxiNumber() {
        return taxiNumber;
    }

    public void setTaxiNumber(String taxiNumber) {
        this.taxiNumber = taxiNumber;
    }

    public String getCarid() {
        return carid;
    }

    public void setCarid(String carid) {
        this.carid = carid;
    }

}
