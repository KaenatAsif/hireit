package hireit.hireit.Models;//
//	HireitCustomerLedgerResult.java
//
//	Create by Hassan on 12/7/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import org.json.*;
import java.util.*;


public class HireitCustomerLedgerResult{

	private ArrayList<HireitCustomerLedgerNew> customerLedger;
	private String result;
	private String totalBalance;
	private List<HireitCustomerLedgerNew> customerLedgerNew;

	public void setCustomerLedger(ArrayList<HireitCustomerLedgerNew> customerLedger){
		this.customerLedger = customerLedger;
	}
	public ArrayList<HireitCustomerLedgerNew> getCustomerLedger(){
		return this.customerLedger;
	}
	public void setResult(String result){
		this.result = result;
	}
	public String getResult(){
		return this.result;
	}
	public void setTotalBalance(String totalBalance){
		this.totalBalance = totalBalance;
	}
	public String getTotalBalance(){
		return this.totalBalance;
	}

	/**
	 * Instantiate the instance using the passed jsonObject to set the properties values
	 */
	public HireitCustomerLedgerResult(JSONObject jsonObject){
		if(jsonObject == null){
			return;
		}
		JSONArray customerLedgerJsonArray = jsonObject.optJSONArray("CustomerLedger");
		if(customerLedgerJsonArray != null){
			ArrayList<HireitCustomerLedgerNew> customerLedgerArrayList = new ArrayList<>();
			for (int i = 0; i < customerLedgerJsonArray.length(); i++) {

				JSONObject ledgerObj = customerLedgerJsonArray.optJSONObject(i);
				HireitCustomerLedgerNew ledgerNew = new HireitCustomerLedgerNew(ledgerObj);
				ledgerNew.setCarName(ledgerObj.opt("CarName").toString());
				ledgerNew.setId(ledgerObj.opt("Id").toString());
				ledgerNew.setRemaining(Integer.parseInt(ledgerObj.opt("Remaining").toString()));
				ledgerNew.setTotalPrice(ledgerObj.opt("TotalPrice").toString());
				ledgerNew.setCarId(Integer.parseInt(ledgerObj.opt("carId").toString()));

				customerLedgerArrayList.add(ledgerNew);

			}
			setCustomerLedger(customerLedgerArrayList);
			//customerLedger = (HireitCustomerLedgerResult) customerLedgerJsonArray.toString();
		}		result =  jsonObject.opt("Result").toString();
		totalBalance = jsonObject.opt("TotalBalance").toString();
	}

	/**
	 * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
	 */
	/*public JSONObject toJsonObject()
	{
		JSONObject jsonObject = new JSONObject();
		try {
			if(customerLedger != null && customerLedger.length > 0){
				JSONArray customerLedgerJsonArray = new JSONArray();
				for(HireitCustomerLedger customerLedgerElement : customerLedger){
					customerLedgerJsonArray.put(customerLedgerElement.toJsonObject());
				}
				jsonObject.put("CustomerLedger", customerLedgerJsonArray);
			}
			jsonObject.put("Result", result);
			jsonObject.put("TotalBalance", totalBalance);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject;
	}
*/
	public static class HireitCustomerLedgerNew
	{
		private String carName;
		private String id;
		private int remaining;
		private String totalPrice;
		private int carId;

		public HireitCustomerLedgerNew(JSONObject customerLedgerObject) {

		}

		public void setCarName(String carName){
			this.carName = carName;
		}
		public String getCarName(){
			return this.carName;
		}
		public void setId(String id){
			this.id = id;
		}
		public String getId(){
			return this.id;
		}
		public void setRemaining(int remaining){
			this.remaining = remaining;
		}
		public int getRemaining(){
			return this.remaining;
		}
		public void setTotalPrice(String totalPrice){
			this.totalPrice = totalPrice;
		}
		public String getTotalPrice(){
			return this.totalPrice;
		}
		public void setCarId(int carId){
			this.carId = carId;
		}
		public int getCarId(){
			return this.carId;
		}

	}


}