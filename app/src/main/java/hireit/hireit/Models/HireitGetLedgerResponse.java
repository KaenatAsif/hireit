package hireit.hireit.Models;//
//	HireitGetLedgerResponse.java
//
//	Create by Hassan on 12/7/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import org.json.JSONObject;


public class HireitGetLedgerResponse{

	private HireitCustomerLedgerResult customerLedgerResult;

	public void setCustomerLedgerResult(HireitCustomerLedgerResult customerLedgerResult){
		this.customerLedgerResult = customerLedgerResult;
	}
	public HireitCustomerLedgerResult getCustomerLedgerResult(){
		return this.customerLedgerResult;
	}

	/**
	 * Instantiate the instance using the passed jsonObject to set the properties values
	 */
	public HireitGetLedgerResponse(JSONObject jsonObject){
		if(jsonObject == null){
			return;
		}
		customerLedgerResult = new HireitCustomerLedgerResult(jsonObject.optJSONObject("CustomerLedgerResult"));
	}

	/**
	 * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
	 */
	/*public JSONObject toJsonObject()
	{
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("CustomerLedgerResult", customerLedgerResult.toJsonObject());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject;
	}
*/
}