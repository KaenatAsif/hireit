package hireit.hireit.Models;//
//	HireitUserProfileResponse.java
//
//	Create by Hassan on 22/6/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import org.json.JSONException;
import org.json.JSONObject;


public class HireitUserProfileResponse{

	private HireitCustomerProfile customerProfile;

	public void setCustomerProfile(HireitCustomerProfile customerProfile){
		this.customerProfile = customerProfile;
	}
	public HireitCustomerProfile getCustomerProfile(){
		return this.customerProfile;
	}

	/**
	 * Instantiate the instance using the passed jsonObject to set the properties values
	 */
	public HireitUserProfileResponse(JSONObject jsonObject){
		if(jsonObject == null){
			return;
		}
		customerProfile = new HireitCustomerProfile(jsonObject.optJSONObject("CustomerProfile"));
	}

	/**
	 * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
	 */
	public JSONObject toJsonObject()
	{
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("CustomerProfile", customerProfile.toJsonObject());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject;
	}

}