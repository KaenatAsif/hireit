package hireit.hireit.Models;//
//	HireitCarPriceContract.java
//
//	Create by Hassan on 3/7/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import org.json.JSONException;
import org.json.JSONObject;


public class HireitCarPriceContract{

	private String amountOfDiscunt;
	private int carPriceid;
	private String dateFrom;
	private String dateTo;
	private String discountPercentage;
	private String finalPrice;
	private String initialPrice;

	public void setAmountOfDiscunt(String amountOfDiscunt){
		this.amountOfDiscunt = amountOfDiscunt;
	}
	public String getAmountOfDiscunt(){
		return this.amountOfDiscunt;
	}
	public void setCarPriceid(int carPriceid){
		this.carPriceid = carPriceid;
	}
	public int getCarPriceid(){
		return this.carPriceid;
	}
	public void setDateFrom(String dateFrom){
		this.dateFrom = dateFrom;
	}
	public String getDateFrom(){
		return this.dateFrom;
	}
	public void setDateTo(String dateTo){
		this.dateTo = dateTo;
	}
	public String getDateTo(){
		return this.dateTo;
	}
	public void setDiscountPercentage(String discountPercentage){
		this.discountPercentage = discountPercentage;
	}
	public String getDiscountPercentage(){
		return this.discountPercentage;
	}
	public void setFinalPrice(String finalPrice){
		this.finalPrice = finalPrice;
	}
	public String getFinalPrice(){
		return this.finalPrice;
	}
	public void setInitialPrice(String initialPrice){
		this.initialPrice = initialPrice;
	}
	public String getInitialPrice(){
		return this.initialPrice;
	}

	/**
	 * Instantiate the instance using the passed jsonObject to set the properties values
	 */
	public HireitCarPriceContract(JSONObject jsonObject){
		if(jsonObject == null){
			return;
		}
		amountOfDiscunt = jsonObject.opt("AmountOfDiscunt").toString();
		carPriceid = jsonObject.optInt("CarPriceid");
		dateFrom = jsonObject.opt("DateFrom").toString();
		dateTo = jsonObject.opt("DateTo").toString();
		discountPercentage = jsonObject.opt("DiscountPercentage").toString();
		finalPrice = jsonObject.opt("FinalPrice").toString();
		initialPrice = jsonObject.opt("InitialPrice").toString();
	}

	/**
	 * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
	 */
	public JSONObject toJsonObject()
	{
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("AmountOfDiscunt", amountOfDiscunt);
			jsonObject.put("CarPriceid", carPriceid);
			jsonObject.put("DateFrom", dateFrom);
			jsonObject.put("DateTo", dateTo);
			jsonObject.put("DiscountPercentage", discountPercentage);
			jsonObject.put("FinalPrice", finalPrice);
			jsonObject.put("InitialPrice", initialPrice);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject;
	}

}