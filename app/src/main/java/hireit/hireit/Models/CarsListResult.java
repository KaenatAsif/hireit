
package hireit.hireit.Models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CarsListResult {

    @SerializedName("Result")
    @Expose
    private String result;
    @SerializedName("carClaims")
    @Expose
    private List<CarClaim> carClaims = null;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public List<CarClaim> getCarClaims() {
        return carClaims;
    }

    public void setCarClaims(List<CarClaim> carClaims) {
        this.carClaims = carClaims;
    }

}
