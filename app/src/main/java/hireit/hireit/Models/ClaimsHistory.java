
package hireit.hireit.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClaimsHistory {

    @SerializedName("GetClaimsHistoryResult")
    @Expose
    private GetClaimsHistoryResult getClaimsHistoryResult;

    public GetClaimsHistoryResult getGetClaimsHistoryResult() {
        return getClaimsHistoryResult;
    }

    public void setGetClaimsHistoryResult(GetClaimsHistoryResult getClaimsHistoryResult) {
        this.getClaimsHistoryResult = getClaimsHistoryResult;
    }

}
