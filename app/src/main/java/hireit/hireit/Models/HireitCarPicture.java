package hireit.hireit.Models;//
//	HireitCarPicture.java
//
//	Create by Hassan on 3/7/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import org.json.JSONException;
import org.json.JSONObject;


public class HireitCarPicture{

	private String id;
	private String piclink;

	public void setId(String id){
		this.id = id;
	}
	public String getId(){
		return this.id;
	}
	public void setPiclink(String piclink){
		this.piclink = piclink;
	}
	public String getPiclink(){
		return this.piclink;
	}

	/**
	 * Instantiate the instance using the passed jsonObject to set the properties values
	 */
	public HireitCarPicture(JSONObject jsonObject){
		if(jsonObject == null){
			return;
		}
		id = jsonObject.opt("Id").toString();
		piclink = jsonObject.opt("piclink").toString();
	}

	/**
	 * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
	 */
	public JSONObject toJsonObject()
	{
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("Id", id);
			jsonObject.put("piclink", piclink);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject;
	}

}