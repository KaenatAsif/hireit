package hireit.hireit.Models;//
//	HireitCarsResult.java
//
//	Create by Hassan on 3/7/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import org.json.JSONObject;


public class HireitCarsResult{

	private String result;
	private HireitCarsNew carsNew;

	public void setResult(String result){
		this.result = result;
	}
	public String getResult(){
		return this.result;
	}
	public void setCarsNew(HireitCarsNew carsNew){
		this.carsNew = carsNew;
	}
	public HireitCarsNew getCarsNew(){
		return this.carsNew;
	}

	/**
	 * Instantiate the instance using the passed jsonObject to set the properties values
	 */
	public HireitCarsResult(JSONObject jsonObject){
		if(jsonObject == null){
			return;
		}
		result = jsonObject.opt("Result").toString();
		carsNew = new HireitCarsNew(jsonObject.optJSONObject("carsNew"));
	}

	/**
	 * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
	 */
/*	public JSONObject toJsonObject()
	{
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("Result", result);
			jsonObject.put("carsNew", carsNew.toJsonObject());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject;
	}*/

}