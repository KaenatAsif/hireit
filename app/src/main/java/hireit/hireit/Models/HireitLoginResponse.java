package hireit.hireit.Models;//
//	Hire-itLoginResponse.java
//
//	Create by Hassan on 20/6/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import org.json.JSONException;
import org.json.JSONObject;
public class HireitLoginResponse{

	private String result;

	public void setResult(String result){
		this.result = result;
	}
	public String getResult(){
		return this.result;
	}

	/**
	 * Instantiate the instance using the passed jsonObject to set the properties values
	 */
	public HireitLoginResponse(JSONObject jsonObject){
		if(jsonObject == null){
			return;
		}
		result = (String) jsonObject.opt("Result");
	}

	/**
	 * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
	 */
	public JSONObject toJsonObject()
	{
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("Result", result);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject;
	}

}