package hireit.hireit.Models;//
//	HireitCarsResponse.java
//
//	Create by Hassan on 3/7/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import org.json.*;
import java.util.*;


public class HireitCarsResponse{

	private HireitCarsResult[] carsResult;

	public void setCarsResult(HireitCarsResult[] carsResult){
		this.carsResult = carsResult;
	}
	public HireitCarsResult[] getCarsResult(){
		return this.carsResult;
	}

	/**
	 * Instantiate the instance using the passed jsonObject to set the properties values
	 */
	public HireitCarsResponse(JSONObject jsonObject){
		if(jsonObject == null){
			return;
		}
		JSONArray carsResultJsonArray = jsonObject.optJSONArray("CarsResult");
		if(carsResultJsonArray != null){
			ArrayList<HireitCarsResult> carsResultArrayList = new ArrayList<>();
			for (int i = 0; i < carsResultJsonArray.length(); i++) {
				JSONObject carsResultObject = carsResultJsonArray.optJSONObject(i);
				carsResultArrayList.add(new HireitCarsResult(carsResultObject));
			}
			carsResult = (HireitCarsResult[]) carsResultArrayList.toArray();
		}	}

	/**
	 * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
	 */
	/*public JSONObject toJsonObject()
	{
		JSONObject jsonObject = new JSONObject();
		try {
			if(carsResult != null && carsResult.length > 0){
				JSONArray carsResultJsonArray = new JSONArray();
				for(HireitCarsResult carsResultElement : carsResult){
					carsResultJsonArray.put(carsResultElement.toJsonObject());
				}
				jsonObject.put("CarsResult", carsResultJsonArray);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject;
	}*/

}