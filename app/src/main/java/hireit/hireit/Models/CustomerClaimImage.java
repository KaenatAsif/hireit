
package hireit.hireit.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CustomerClaimImage {

    @SerializedName("ClaimNumber")
    @Expose
    private String claimNumber;
    @SerializedName("Status")
    @Expose
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @SerializedName("piclink")
    @Expose


    private List<String> piclink = null;

    public String getClaimNumber() {
        return claimNumber;
    }

    public void setClaimNumber(String claimNumber) {
        this.claimNumber = claimNumber;
    }

    public List<String> getPiclink() {
        return piclink;
    }

    public void setPiclink(List<String> piclink) {
        this.piclink = piclink;
    }

}
