
package hireit.hireit.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetClaimsHistoryResult {

    @SerializedName("customerClaimImage")
    @Expose
    private List<CustomerClaimImage> customerClaimImage = null;

    public List<CustomerClaimImage> getCustomerClaimImage() {
        return customerClaimImage;
    }

    public void setCustomerClaimImage(List<CustomerClaimImage> customerClaimImage) {
        this.customerClaimImage = customerClaimImage;
    }

}
