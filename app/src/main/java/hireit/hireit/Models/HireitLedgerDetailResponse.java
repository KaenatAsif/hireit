package hireit.hireit.Models;//
//	HireitLedgerDetailResponse.java
//
//	Create by Hassan on 12/7/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import org.json.*;
import java.util.*;


public class HireitLedgerDetailResponse
{

	//private HireitCustomerLedgerDetialsResult[] customerLedgerDetialsResult;

	private ArrayList<HireitCustomerLedgerResultNew> customerLedgerResultNew;
	private List<HireitCustomerLedgerResultNew> customerLedgerResultNews;

	public void setCustomerLedgerDetialsResult(ArrayList<HireitCustomerLedgerResultNew> customerLedgerResultNew){
		this.customerLedgerResultNew = customerLedgerResultNew;
	}
	public ArrayList<HireitCustomerLedgerResultNew> getCustomerLedgerDetialsResult(){
		return this.customerLedgerResultNew;
	}

	/**
	 * Instantiate the instance using the passed jsonObject to set the properties values
	 */
	public HireitLedgerDetailResponse(JSONObject jsonObject){
		if(jsonObject == null){
			return;
		}
		JSONArray customerLedgerDetialsResultJsonArray = jsonObject.optJSONArray("CustomerLedgerDetialsResult");

		if(customerLedgerDetialsResultJsonArray != null)
		{

			ArrayList<HireitCustomerLedgerResultNew> customerLedgerDetialsResultArrayList = new ArrayList<>();
			for (int i = 0; i < customerLedgerDetialsResultJsonArray.length(); i++)
			{
				JSONObject customerLedgerDetialsResultObject = customerLedgerDetialsResultJsonArray.optJSONObject(i);
				HireitCustomerLedgerResultNew abc = new HireitCustomerLedgerResultNew(customerLedgerDetialsResultObject);
				//customerLedgerDetialsResultArrayList.add(new HireitCustomerLedgerDetialsResult(customerLedgerDetialsResultObject));
				abc.setCarName(customerLedgerDetialsResultObject.opt("CarName").toString());
				abc.setId(customerLedgerDetialsResultObject.opt("Id").toString());
				abc.setNote(customerLedgerDetialsResultObject.opt("Note").toString());
				abc.setPaid(customerLedgerDetialsResultObject.opt("Paid").toString());
				abc.setPaymentDate(customerLedgerDetialsResultObject.opt("PaymentDate").toString());
				abc.setRemaining(customerLedgerDetialsResultObject.optInt("Remaining"));
				abc.setCarId(customerLedgerDetialsResultObject.optInt("carId"));
				customerLedgerDetialsResultArrayList.add(abc);
			}

			setCustomerLedgerDetialsResult(customerLedgerDetialsResultArrayList);

		}
	}

	/**
	 * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
	 */
/*
	public JSONObject toJsonObject()
	{
		JSONObject jsonObject = new JSONObject();
		try {
			if(customerLedgerDetialsResult != null && customerLedgerDetialsResult.length > 0){
				JSONArray customerLedgerDetialsResultJsonArray = new JSONArray();
				for(HireitCustomerLedgerDetialsResult customerLedgerDetialsResultElement : customerLedgerDetialsResult){
					customerLedgerDetialsResultJsonArray.put(customerLedgerDetialsResultElement.toJsonObject());
				}
				jsonObject.put("CustomerLedgerDetialsResult", customerLedgerDetialsResultJsonArray);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject;
	}
*/
public class HireitCustomerLedgerResultNew{
		private String carName;
		private String id;
		private String note;
		private String paid;
		private String paymentDate;
		private int remaining;
		private int carId;

		public HireitCustomerLedgerResultNew(JSONObject customerLedgerDetialsResultObject) {

		}

		public void setCarName(String carName){
			this.carName = carName;
		}
		public String getCarName(){
			return this.carName;
		}
		public void setId(String id){
			this.id = id;
		}
		public String getId(){
			return this.id;
		}
		public void setNote(String note){
			this.note = note;
		}
		public String getNote(){
			return this.note;
		}
		public void setPaid(String paid){
			this.paid = paid;
		}
		public String getPaid(){
			return this.paid;
		}
		public void setPaymentDate(String paymentDate){
			this.paymentDate = paymentDate;
		}
		public String getPaymentDate(){
			return this.paymentDate;
		}
		public void setRemaining(int remaining){
			this.remaining = remaining;
		}
		public int getRemaining(){
			return this.remaining;
		}
		public void setCarId(int carId){
			this.carId = carId;
		}
		public int getCarId(){
			return this.carId;
		}








	}

}