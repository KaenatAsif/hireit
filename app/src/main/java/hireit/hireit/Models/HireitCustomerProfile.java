package hireit.hireit.Models;//
//	HireitCustomerProfile.java
//
//	Create by Hassan on 22/6/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import org.json.JSONException;
import org.json.JSONObject;


public class HireitCustomerProfile{

	private String applicationUserId;
	private String dateOfBirth;
	private String firstName;
	private String gender;
	private String lastName;
	private String mobileNumber;
	private String refferalCode;
	private String resultMessage;
	private String walletAmount;

	public void setApplicationUserId(String applicationUserId){
		this.applicationUserId = applicationUserId;
	}
	public String getApplicationUserId(){
		return this.applicationUserId;
	}
	public void setDateOfBirth(String dateOfBirth){
		this.dateOfBirth = dateOfBirth;
	}
	public String getDateOfBirth(){
		return this.dateOfBirth;
	}
	public void setFirstName(String firstName){
		this.firstName = firstName;
	}
	public String getFirstName(){
		return this.firstName;
	}
	public void setGender(String gender){
		this.gender = gender;
	}
	public String getGender(){
		return this.gender;
	}
	public void setLastName(String lastName){
		this.lastName = lastName;
	}
	public String getLastName(){
		return this.lastName;
	}
	public void setMobileNumber(String mobileNumber){
		this.mobileNumber = mobileNumber;
	}
	public String getMobileNumber(){
		return this.mobileNumber;
	}
	public void setResultMessage(String resultMessage){
		this.resultMessage = resultMessage;
	}
	public String getResultMessage(){
		return this.resultMessage;
	}

	public String getRefferalCode() {
		return refferalCode;
	}

	public void setRefferalCode(String refferalCode) {
		this.refferalCode = refferalCode;
	}

	public String getWalletAmount() {
		return walletAmount;
	}

	public void setWalletAmount(String walletAmount) {
		this.walletAmount = walletAmount;
	}

	/**
	 * Instantiate the instance using the passed jsonObject to set the properties values
	 */
	public HireitCustomerProfile(JSONObject jsonObject){
		if(jsonObject != null) {


			if(jsonObject.opt("ApplicationUserId") != null)
			{
				applicationUserId = jsonObject.opt("ApplicationUserId").toString();
			}

			if(jsonObject.opt("DateOfBirth") != null)
			{
				dateOfBirth =jsonObject.opt("DateOfBirth").toString();
			}
			if(jsonObject.opt("FirstName") != null)
			{
				firstName = jsonObject.opt("FirstName").toString();
			}
			if(jsonObject.opt("Gender") != null)
			{
				gender =  jsonObject.opt("Gender").toString();
			}
			if(jsonObject.opt("LastName") != null)
			{
				lastName =jsonObject.opt("LastName").toString();
			}
			if(jsonObject.opt("MobileNumber") != null)
			{
				mobileNumber =jsonObject.opt("MobileNumber").toString();
			}
			if(jsonObject.opt("RefferalCode") != null)
			{
				refferalCode =jsonObject.opt("RefferalCode").toString();
			}
			if(jsonObject.opt("resultMessage") != null)
			{
				resultMessage = jsonObject.opt("resultMessage").toString();
			}
			if(jsonObject.opt("walletAmount") != null)
			{
				walletAmount =jsonObject.opt("walletAmount").toString();
			}

			System.out.println("\n hhh "+jsonObject.opt("ApplicationUserId"));
			System.out.println("\n hhh "+jsonObject.opt("FirstName"));
			System.out.println("\n hhh "+jsonObject.opt("Gender"));
			System.out.println("\n hhh "+jsonObject.opt("MobileNumber"));
			System.out.println("\n hhh "+jsonObject.opt("RefferalCode"));
			System.out.println("\n hhh "+jsonObject.opt("resultMessage"));
			System.out.println("\n hhh "+jsonObject.opt("walletAmount"));

		}
	}

	/**
	 * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
	 */
	public JSONObject toJsonObject()
	{
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("ApplicationUserId", applicationUserId);
			jsonObject.put("DateOfBirth", dateOfBirth);
			jsonObject.put("FirstName", firstName);
			jsonObject.put("Gender", gender);
			jsonObject.put("LastName", lastName);
			jsonObject.put("MobileNumber", mobileNumber);
			jsonObject.put("RefferalCode", refferalCode);
			jsonObject.put("resultMessage", resultMessage);
			jsonObject.put("walletAmount", walletAmount);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject;
	}

}