package hireit.hireit.Models;//
//	HireitCustomerLedgerDetialsResult.java
//
//	Create by Hassan on 12/7/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import org.json.JSONException;
import org.json.JSONObject;


public class HireitCustomerLedgerDetialsResult{

	private String carName;
	private String id;
	private String note;
	private String paid;
	private String paymentDate;
	private int remaining;
	private int carId;

	public void setCarName(String carName){
		this.carName = carName;
	}
	public String getCarName(){
		return this.carName;
	}
	public void setId(String id){
		this.id = id;
	}
	public String getId(){
		return this.id;
	}
	public void setNote(String note){
		this.note = note;
	}
	public String getNote(){
		return this.note;
	}
	public void setPaid(String paid){
		this.paid = paid;
	}
	public String getPaid(){
		return this.paid;
	}
	public void setPaymentDate(String paymentDate){
		this.paymentDate = paymentDate;
	}
	public String getPaymentDate(){
		return this.paymentDate;
	}
	public void setRemaining(int remaining){
		this.remaining = remaining;
	}
	public int getRemaining(){
		return this.remaining;
	}
	public void setCarId(int carId){
		this.carId = carId;
	}
	public int getCarId(){
		return this.carId;
	}

	/**
	 * Instantiate the instance using the passed jsonObject to set the properties values
	 */
	public HireitCustomerLedgerDetialsResult(JSONObject jsonObject){
		if(jsonObject == null){
			return;
		}
		carName = jsonObject.opt("CarName").toString();
		id = jsonObject.opt("Id").toString();
		note = jsonObject.opt("Note").toString();
		paid = jsonObject.opt("Paid").toString();
		paymentDate = jsonObject.opt("PaymentDate").toString();
		remaining = jsonObject.optInt("Remaining");
		carId = jsonObject.optInt("carId");
	}

	/**
	 * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
	 */
	public JSONObject toJsonObject()
	{
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("CarName", carName);
			jsonObject.put("Id", id);
			jsonObject.put("Note", note);
			jsonObject.put("Paid", paid);
			jsonObject.put("PaymentDate", paymentDate);
			jsonObject.put("Remaining", remaining);
			jsonObject.put("carId", carId);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject;
	}

}