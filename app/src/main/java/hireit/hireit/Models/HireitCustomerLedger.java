package hireit.hireit.Models;//
//	HireitCustomerLedger.java
//
//	Create by Hassan on 12/7/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import org.json.JSONException;
import org.json.JSONObject;


public class HireitCustomerLedger{

	private String carName;
	private String id;
	private int remaining;
	private String totalPrice;
	private int carId;

	public void setCarName(String carName){
		this.carName = carName;
	}
	public String getCarName(){
		return this.carName;
	}
	public void setId(String id){
		this.id = id;
	}
	public String getId(){
		return this.id;
	}
	public void setRemaining(int remaining){
		this.remaining = remaining;
	}
	public int getRemaining(){
		return this.remaining;
	}
	public void setTotalPrice(String totalPrice){
		this.totalPrice = totalPrice;
	}
	public String getTotalPrice(){
		return this.totalPrice;
	}
	public void setCarId(int carId){
		this.carId = carId;
	}
	public int getCarId(){
		return this.carId;
	}

	/**
	 * Instantiate the instance using the passed jsonObject to set the properties values
	 */
	public HireitCustomerLedger(JSONObject jsonObject){
		if(jsonObject == null){
			return;
		}
		carName = jsonObject.opt("CarName").toString();
		id = jsonObject.opt("Id").toString();
		remaining = jsonObject.optInt("Remaining");
		totalPrice = jsonObject.opt("TotalPrice").toString();
		carId = jsonObject.optInt("carId");
	}

	/**
	 * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
	 */
	public JSONObject toJsonObject()
	{
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("CarName", carName);
			jsonObject.put("Id", id);
			jsonObject.put("Remaining", remaining);
			jsonObject.put("TotalPrice", totalPrice);
			jsonObject.put("carId", carId);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject;
	}

}