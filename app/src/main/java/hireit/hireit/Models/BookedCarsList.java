
package hireit.hireit.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BookedCarsList {

    @SerializedName("CarsListResult")
    @Expose
    private CarsListResult carsListResult;

    public CarsListResult getCarsListResult() {
        return carsListResult;
    }

    public void setCarsListResult(CarsListResult carsListResult) {
        this.carsListResult = carsListResult;
    }

}
