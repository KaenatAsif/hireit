package hireit.hireit.Models;//
//	HireitCarsNew.java
//
//	Create by Hassan on 3/7/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import org.json.*;
import java.util.*;


public class HireitCarsNew{

	private String carCity;
	private String carName;
	private String color;
	private String enginePower;
	private String model;
	private String status;
	private String registrationNumber;
	private String bookedDate;
	private String chasisNumber;
	private String engineNumber;
	private String expiryDate;
	private String weeklyRent;
	private String weeksBooked;
	private String taxiNumber;
	private String totalAmount;

	public String getBookedDate() {
		return bookedDate;
	}

	public void setBookedDate(String bookedDate) {
		this.bookedDate = bookedDate;
	}

	public String getChasisNumber() {
		return chasisNumber;
	}

	public void setChasisNumber(String chasisNumber) {
		this.chasisNumber = chasisNumber;
	}

	public String getEngineNumber() {
		return engineNumber;
	}

	public void setEngineNumber(String engineNumber) {
		this.engineNumber = engineNumber;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getWeeklyRent() {
		return weeklyRent;
	}

	public void setWeeklyRent(String weeklyRent) {
		this.weeklyRent = weeklyRent;
	}

	public String getWeeksBooked() {
		return weeksBooked;
	}

	public void setWeeksBooked(String weeksBooked) {
		this.weeksBooked = weeksBooked;
	}

	public String getTaxiNumber() {
		return taxiNumber;
	}

	public void setTaxiNumber(String taxiNumber) {
		this.taxiNumber = taxiNumber;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	private List<HireitCarPictureNew> carPictureList;
	private HireitCarPriceContract carPriceContract;
	private int id;

	public List<HireitCarPictureNew> getCarPictureList() {
		return carPictureList;
	}

	public void setCarPictureList(List<HireitCarPictureNew> carPictureList) {
		this.carPictureList = carPictureList;
	}

	public void setCarCity(String carCity){
		this.carCity = carCity;
	}
	public String getCarCity(){
		return this.carCity;
	}
	public void setCarName(String carName){
		this.carName = carName;
	}
	public String getCarName(){
		return this.carName;
	}
	public void setColor(String color){
		this.color = color;
	}
	public String getColor(){
		return this.color;
	}
	public void setEnginePower(String enginePower){
		this.enginePower = enginePower;
	}
	public String getEnginePower(){
		return this.enginePower;
	}
	public void setModel(String model){
		this.model = model;
	}
	public String getModel(){
		return this.model;
	}
	public void setStatus(String status){
		this.status = status;
	}
	public String getStatus(){
		return this.status;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	//	public void setCarPicture(HireitCarPicture[] carPicture){
//		this.carPicture = carPicture;
//	}
//	public HireitCarPicture[] getCarPicture(){
//		return this.carPicture;
//	}
	public void setCarPriceContract(HireitCarPriceContract carPriceContract){
		this.carPriceContract = carPriceContract;
	}
	public HireitCarPriceContract getCarPriceContract(){
		return this.carPriceContract;
	}
	public void setId(int id){
		this.id = id;
	}
	public int getId(){
		return this.id;
	}

	/**
	 * Instantiate the instance using the passed jsonObject to set the properties values
	 */
	public HireitCarsNew(JSONObject jsonObject){
		if(jsonObject == null){
			return;
		}
		carCity = jsonObject.opt("CarCity").toString();
		carName = jsonObject.opt("CarName").toString();
		color = jsonObject.opt("Color").toString();
		enginePower = jsonObject.opt("EnginePower").toString();
		model = jsonObject.opt("Model").toString();
		status = jsonObject.opt("Status").toString();

        registrationNumber = jsonObject.opt("RegistrationNumber").toString();

		if (jsonObject.opt("TotalAmount") != null)
		{
			totalAmount=jsonObject.opt("TotalAmount").toString();
		}

		if (jsonObject.opt("BookedDate")!=null){
		bookedDate=jsonObject.opt("BookedDate").toString();}
		if (jsonObject.opt("EngineNumber")!=null){
		engineNumber=jsonObject.opt("EngineNumber").toString();}
		if (jsonObject.opt("TaxiNumber")!=null){
		taxiNumber=jsonObject.opt("TaxiNumber").toString();}
		if (jsonObject.opt("WeeklyRent")!=null){
		weeklyRent=jsonObject.opt("WeeklyRent").toString();}
		if (jsonObject.opt("WeeksBooked")!=null){
		weeksBooked=jsonObject.opt("WeeksBooked").toString();}
		if (jsonObject.opt("ExpiryDate")!=null){
		expiryDate=jsonObject.opt("ExpiryDate").toString();}
		if (jsonObject.opt("ChasisNumber")!=null){
		chasisNumber=jsonObject.opt("ChasisNumber").toString();}



		JSONArray carPictureJsonArray = jsonObject.optJSONArray("carPicture");
		if(carPictureJsonArray != null){
			ArrayList<HireitCarPictureNew> carPictureArrayList = new ArrayList<>();
			for (int i = 0; i < carPictureJsonArray.length(); i++) {
				JSONObject carPictureObject = carPictureJsonArray.optJSONObject(i);
				HireitCarPictureNew hireitCarPictureNew = new HireitCarPictureNew();
				hireitCarPictureNew.setId(carPictureObject.opt("Id").toString());
				hireitCarPictureNew.setPictureLink(carPictureObject.opt("piclink").toString());
				carPictureArrayList.add(hireitCarPictureNew);

				//carPictureArrayList.add(new HireitCarPicture(carPictureObject));
			}
			setCarPictureList(carPictureArrayList);
			//carPicture = (HireitCarPicture[]) carPictureArrayList.toArray();
		}	carPriceContract = new HireitCarPriceContract(jsonObject.optJSONObject("carPriceContract"));
		id = jsonObject.optInt("id");
	}

	/**
	 * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
	 */
//	public JSONObject toJsonObject()
//	{
//		JSONObject jsonObject = new JSONObject();
//		try {
//			jsonObject.put("CarCity", carCity);
//			jsonObject.put("CarName", carName);
//			jsonObject.put("Color", color);
//			jsonObject.put("EnginePower", enginePower);
//			jsonObject.put("Model", model);
//			jsonObject.put("Status", status);
//			if(carPicture != null && carPicture.length > 0){
//				JSONArray carPictureJsonArray = new JSONArray();
//				for(HireitCarPicture carPictureElement : carPicture){
//					carPictureJsonArray.put(carPictureElement.toJsonObject());
//				}
//				jsonObject.put("carPicture", carPictureJsonArray);
//			}
//			jsonObject.put("carPriceContract", carPriceContract.toJsonObject());
//			jsonObject.put("id", id);
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return jsonObject;
//	}



	public static class HireitCarPictureNew
	{
		private String id;
		private String pictureLink;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getPictureLink() {
			return pictureLink;
		}

		public void setPictureLink(String pictureLink) {
			this.pictureLink = pictureLink;
		}
	}

}