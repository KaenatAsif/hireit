package hireit.hireit.Models;//
//	HireitDocument.java
//
//	Create by Hassan on 11/7/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import org.json.JSONException;
import org.json.JSONObject;


public class HireitDocument{

	private String dVLABack;
	private String dVLAFront;
	private String result;
	private String taxiBadgeBack;
	private String taxiBadgeFront;
	private String UtilityBillOrTaxiLetter;

	public String getUtilityBillOrTaxiLetter() {
		return UtilityBillOrTaxiLetter;
	}

	public void setUtilityBillOrTaxiLetter(String utilityBillOrTaxiLetter) {
		UtilityBillOrTaxiLetter = utilityBillOrTaxiLetter;
	}

	public void setDVLABack(String dVLABack){
		this.dVLABack = dVLABack;
	}
	public String getDVLABack(){
		return this.dVLABack;
	}
	public void setDVLAFront(String dVLAFront){
		this.dVLAFront = dVLAFront;
	}
	public String getDVLAFront(){
		return this.dVLAFront;
	}
	public void setResult(String result){
		this.result = result;
	}
	public String getResult(){
		return this.result;
	}
	public void setTaxiBadgeBack(String taxiBadgeBack){
		this.taxiBadgeBack = taxiBadgeBack;
	}
	public String getTaxiBadgeBack(){
		return this.taxiBadgeBack;
	}
	public void setTaxiBadgeFront(String taxiBadgeFront){
		this.taxiBadgeFront = taxiBadgeFront;
	}
	public String getTaxiBadgeFront(){
		return this.taxiBadgeFront;
	}

	/**
	 * Instantiate the instance using the passed jsonObject to set the properties values
	 */
	public HireitDocument(JSONObject jsonObject){
		if(jsonObject == null){
			return;
		}
		dVLABack =  jsonObject.opt("DVLABack").toString();
		dVLAFront = jsonObject.opt("DVLAFront").toString();
		result =  jsonObject.opt("Result").toString();
		taxiBadgeBack =  jsonObject.opt("TaxiBadgeBack").toString();
		taxiBadgeFront =  jsonObject.opt("TaxiBadgeFront").toString();
		UtilityBillOrTaxiLetter =  jsonObject.opt("UtilityBillOrTaxiLetter").toString();
	}

	/**
	 * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
	 */
	public JSONObject toJsonObject()
	{
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("DVLABack", dVLABack);
			jsonObject.put("DVLAFront", dVLAFront);
			jsonObject.put("Result", result);
			jsonObject.put("TaxiBadgeBack", taxiBadgeBack);
			jsonObject.put("TaxiBadgeFront", taxiBadgeFront);
			jsonObject.put("UtilityBillOrTaxiLetter", UtilityBillOrTaxiLetter);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject;
	}

}