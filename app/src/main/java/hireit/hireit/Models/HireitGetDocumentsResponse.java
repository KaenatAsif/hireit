package hireit.hireit.Models;//
//	HireitGetDocumentsResponse.java
//
//	Create by Hassan on 11/7/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import org.json.JSONException;
import org.json.JSONObject;


public class HireitGetDocumentsResponse{

	private HireitDocument documents;

	public void setDocuments(HireitDocument documents){
		this.documents = documents;
	}
	public HireitDocument getDocuments(){
		return this.documents;
	}

	/**
	 * Instantiate the instance using the passed jsonObject to set the properties values
	 */
	public HireitGetDocumentsResponse(JSONObject jsonObject){
		if(jsonObject == null){
			return;
		}
		documents = new HireitDocument(jsonObject.optJSONObject("Documents"));
	}

	/**
	 * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
	 */
	public JSONObject toJsonObject()
	{
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("Documents", documents.toJsonObject());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject;
	}

}