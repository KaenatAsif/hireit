package hireit.hireit;

import org.json.JSONException;

/**
 * Created by faheem on 03/08/2017.
 */

public interface AsyncResponse {
    void processFinish(Object result) throws JSONException;
}